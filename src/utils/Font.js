export const FONT_FAMILY = {
    "Roboto": "Roboto-Regular",
    "RobotoSemiBold": "Roboto-Medium",
    "RobotoBold": "Roboto-Bold",
    "Poppins":"Poppins-Regular",
    "PoppinsBold":"Poppins-Bold",
    "PoppinsSemiBold":"Poppins-SemiBold"
};
