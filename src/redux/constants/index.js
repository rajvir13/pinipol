// ----------------------------------------
// ----------------------------------------
// Base Url
// ----------------------------------------
import {Platform} from 'react-native';

module.exports.BASE_URL = 'http://np.seasiafinishingschool.com:7094';
// module.exports.BASE_URL = 'http://stgn.appsndevs.com:7094/';

// ----------------------------------------
// ----------------------------------------
// Device Type
// ----------------------------------------
export const DeviceType = Platform.OS === 'ios' ? 'ios' : 'android';

// ----------------------------------------
// ----------------------------------------
// End Points
// ----------------------------------------
export const API = {

    LOGIN: '/api/login',
    SOCIAL_LOGIN: '/api/getuserbysocialid',
    FORGOT_PASSWORD: '/api/forgetpassword',
    ACCOUNT_VERIFY: '/api/accountverifyotp',
    RESET_PASSWORD: '/api/resetpassword',
    CHECK_USER_AVAILABILITY: '/api/checkuseravailablity',
    CHECK_EMAIL: '/api/verifyemail',
    PERSONAL_REGISTER: '/api/register',
    ORGANIZATION_TYPE: '/api/organisation/predictive/search',
    SECTOR_TYPE: '/api/sector/predictive/search',
    COUNTRY: '/api/countrypredictivesearch',
    CITY: '/api/citypredictivesearch',
    BUSINESS_REGISTER: '/api/registerbusinessaccount',
    SUBSCRIPTION_PLAN: '/api/getsubscriptionplan',
    LOGOUT: '/api/logout',
    RESEND_PIN: '/api/resendpin',

    //----------------------------
    BUSINESS_PROFILE: '/api/profilescreen',
    CHOOSE_INTEREST: '/api/interest',
    UPDATE_PROFILE: '/api/updateprofilepersonalaccount',
    UPDATE_PROFILE_BUSINESS: '/api/updateprofilebusinessaccount',
    //-----------------------------
    CREATE_POLL: '/api/createpoll',
    CATEGORIES: '/api/getpollcategories',
    USER_POLLS: '/api/getpollbyuser',
    GET_ALL_POLLS: '/api/getpoll',
    MOST_POPULAR_POLL: '/api/mostpopularpoll',
    OCCUPATION: '/api/occuptionspredictivesearch',
    RELIGION: '/api/religionspredictivesearch',
    VOTE_ON_POLL:'/api/voteonpoll'
};
