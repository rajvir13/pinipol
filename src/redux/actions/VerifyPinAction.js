/**
 * Created by Nisha-React on 9/23/20.
 */

import {API, BASE_URL} from '../constants';

export const VerifyOtp = (payload, accountVerifyResponse) => {
    let formData = new FormData();
    formData.append('username', payload.username);
    formData.append('otp', payload.otp);
    fetch(BASE_URL + API.ACCOUNT_VERIFY, {
        method: 'POST',
        body: formData,
    })
        .then((response) => response.json())
        .then((responseData) => {
            accountVerifyResponse(responseData);
        })
        .catch((error) => {
            accountVerifyResponse(error);
        }).done();

};
