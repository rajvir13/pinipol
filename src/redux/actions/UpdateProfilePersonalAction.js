/**
 * Created by Nisha-React on 9/23/20.
 */

import {API, BASE_URL} from '../constants';
import axios from 'axios';
import {Platform} from 'react-native';

export const UpdateProfilePersonalAction = (payload, updateProfileResponse) => {
    let formData = new FormData();
    formData.append('email', payload.email);
    formData.append('phone_no', payload.phone_no);
    formData.append('firstname', payload.firstname);
    formData.append('lastname', payload.lastname);
    formData.append('dob', payload.dob);
    formData.append('city', payload.city);
    formData.append('country', payload.country);
    formData.append('gender', payload.gender);
    formData.append('privacy', payload.privacy);
    formData.append('interest', payload.interest);
    formData.append('relationship_status', payload.relationship_status);
    formData.append('occupation', payload.occupation);
    formData.append('religion', payload.religion);
    formData.append('profile_image', payload.profile_image);
    formData.append('user_type', payload.user_type);
    formData.append('account_type', payload.account_type);

    // let data = JSON.stringify({
    //     'email': payload.email,
    //     'phone_no': payload.phone_no,
    //     'firstname': payload.firstname,
    //     'lastname': payload.lastname,
    //     'dob': payload.dob,
    //     'city': payload.city,
    //     'country': payload.country,
    //     'gender': payload.gender,
    //     'privacy': payload.privacy,
    //     'interest': payload.interest,
    //     'relationship_status': payload.relationship_status,
    //     'occupation': payload.occupation,
    //     'religion': payload.religion,
    //     'profile_image': payload.profile_image,
    //     'user_type': payload.user_type,
    //     'account_type': payload.account_type,
    // });

    axios.post(BASE_URL + API.UPDATE_PROFILE, formData, {
        headers:
            {
                //'Content-Type': 'application/json',
                'Authorization': `${'Bearer ' + payload.authToken}`,
                'Content-type': 'multipart/form-data',
            },
    })
        .then((response) => {
            updateProfileResponse(response.data);
        })
        .catch((error) => {
            updateProfileResponse(error);

        }).done();
};


export const UpdateProfileBusinessAction = (payload, updateProfileResponse) => {
    let formData = new FormData();
    formData.append('organisation_name', payload.organisation_name);
    formData.append('legalfirstname', payload.legalfirstname);
    formData.append('legallastname', payload.legallastname);
    formData.append('position', payload.position);
    formData.append('sector', payload.sector);
    formData.append('street_address', payload.street_address);
    formData.append('city', payload.city);
    formData.append('apt_street', payload.apt_street);
    formData.append('zipcode', payload.zipcode);
    formData.append('state', payload.state);
    formData.append('country_code', payload.country_code);
    formData.append('phone_no', payload.phone_no);
    formData.append('organisation_type', payload.organisation_type);
    formData.append('subscription_plan', payload.subscription_plan);
    formData.append('email', payload.email);
    formData.append('profile_image', payload.profile_image);

    axios.post(BASE_URL + API.UPDATE_PROFILE_BUSINESS, formData, {
        headers:
            {
                //'Content-Type': 'application/json',
                'Authorization': `${'Bearer ' + payload.authToken}`,
                'Content-type': 'multipart/form-data',
            },
    })
        .then((response) => {
            updateProfileResponse(response.data);
        })
        .catch((error) => {
            //updateProfileResponse(error.response.data)
            // if (Platform.OS === 'ios') {
            //     updateProfileResponse(error.response.data);
            // } else {
            updateProfileResponse(error);
            // }

        }).done();


};
