import axios from 'axios';
import {API, BASE_URL} from '../constants';

export const VoteOnPollAction = (payload, voteOnPollResponse) => {
    let formData = new FormData();
    formData.append('poll_id', payload.poll_id);
    formData.append('option_id', payload.option_id);
    formData.append('anonymously', payload.anonymously);

    axios.post(BASE_URL + API.VOTE_ON_POLL, formData, {
        headers: {
            'Authorization': `${'Bearer' + ' ' + payload.accessToken}`,
        },
    })
        .then((response) => {
            voteOnPollResponse(response.data);
        })
        .catch((error) => {
            voteOnPollResponse(error);

        }).done();
};
