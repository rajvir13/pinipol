import * as types from '../events/index';
import {API, BASE_URL, DeviceType} from '../constants';
import axios from 'axios';

export const GetBusinessProfile = (payload, profileResponse) => {
    let formData = new FormData();
    formData.append('account_type', payload.account_type);

    axios.post(BASE_URL + API.BUSINESS_PROFILE, formData, {
        headers: {
            'Authorization': `${'Bearer' + ' ' + payload.accessToken}`,
        },
    })
        .then((response) => {
            profileResponse(response.data);
        })
        .catch((error) => {
            profileResponse(error);

        }).done();

};
