/**
 * Created by Nisha-React on 9/23/20.
 */

import {API, BASE_URL} from '../constants';
import axios from 'axios';

export const PersonalRegisterAction = (payload, personalRegisterResponse) => {
    let formData = new FormData();
    formData.append('username', payload.username);
    formData.append('firstname', payload.firstname);
    formData.append('lastname', payload.lastname);
    formData.append('email', payload.email);
    formData.append('password', payload.password);
    formData.append('dob', payload.dob);
    formData.append('social_id', payload.socialId);
    formData.append('social_platform', payload.socialPlatform);
    // fetch(BASE_URL + API.PERSONAL_REGISTER, {
    //     method: 'POST',
    //     headers: {
    //         'Accept': 'application/json',
    //         'Content-Type': 'application/json',
    //     },
    //     body: formData,
    // })
    //     .then((response) => response.json())
    //     .then((responseData) => {
    //         personalRegisterResponse(responseData);
    //     })
    //     .catch((error) => {
    //         personalRegisterResponse(error);
    //     }).done();


    axios.post(BASE_URL + API.PERSONAL_REGISTER, formData, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    })
        .then((response) => {
            personalRegisterResponse(response.data);
        })
        .catch((error) => {
            personalRegisterResponse(error);

        }).done();
};


export const BusinessRegisterAction = (payload, businessRegisterResponse) => {
    let formData = new FormData();
    formData.append('organisation_name', payload.orgName);
    formData.append('legalfirstname', payload.firstName);
    formData.append('legallastname', payload.lastName);
    formData.append('email', payload.email);
    formData.append('password', payload.password);
    formData.append('phone_no', payload.phone);
    formData.append('user_role', 3);
    formData.append('position', payload.position);
    formData.append('sector', payload.sector);
    formData.append('street_address', payload.street);
    formData.append('apt_street', payload.dob);
    formData.append('country', payload.country);
    formData.append('city', payload.city);
    formData.append('zipcode', payload.zipCode);
    formData.append('country_code', payload.countryCode);
    formData.append('organisation_type', payload.organizationType);
    formData.append('subscription_plan', payload.id);
    formData.append('website', payload.website);
    formData.append('social_id', payload.socialId);
    formData.append('social_platform', payload.socialProfile);
    formData.append('username', payload.userName);

    axios.post(BASE_URL + API.BUSINESS_REGISTER, formData, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    })
        .then((response) => {
            businessRegisterResponse(response.data);
        })
        .catch((error) => {
            businessRegisterResponse(error);

        }).done();

    // fetch(BASE_URL + API.BUSINESS_REGISTER, {
    //     method: 'POST',
    //     headers: {
    //         'Accept': 'application/json',
    //         'Content-Type': 'application/json',
    //     },
    //     body: formData,
    // })
    //     .then((response) => response.json())
    //     .then((responseData) => {
    //         businessRegisterResponse(responseData);
    //     })
    //     .catch((error) => {
    //         businessRegisterResponse(error);
    //     }).done();

};

