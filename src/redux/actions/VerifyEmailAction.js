/**
 * Created by Nisha-React on 9/23/20.
 */

import {API, BASE_URL} from '../constants';

export const VerifyEmailAction = (payload, verifyEmailResponse) => {
    let formData = new FormData();
    formData.append('email', payload.email);
    fetch(BASE_URL + API.CHECK_EMAIL, {
        method: 'POST',
        body: formData,
    })
        .then((response) => response.json())
        .then((responseData) => {
            verifyEmailResponse(responseData);
        })
        .catch((error) => {
            verifyEmailResponse(error);
        }).done();

};
