/**
 * Created by Nisha-React on 9/23/20.
 */

import {API, BASE_URL} from '../constants';

export const ResetPasswordAction = (payload, resetPasswordResponse) => {

    let formData = new FormData();
    formData.append('otp', payload.otp);
    formData.append('new_password', payload.password);
    fetch(BASE_URL + API.RESET_PASSWORD, {
        method: 'POST',
        body: formData,
    })
        .then((response) => response.json())
        .then((responseData) => {
            resetPasswordResponse(responseData);
        })
        .catch((error) => {
            resetPasswordResponse(error);
        }).done();

};
