import axios from 'axios';
import {API, BASE_URL} from '../constants';

export const CategoriesAction = (payload, categoriesResponse) => {
    let formData = new FormData();
    formData.append("user_type","3");

    axios.post(BASE_URL + API.CATEGORIES, formData, {
        headers: {
            // 'Content-Type': 'application/json',
            'Authorization': `${'Bearer' + ' ' + payload.accessToken}`,
        },
    })
        .then((response) => {
            categoriesResponse(response.data);
        })
        .catch((error) => {
            categoriesResponse(error);

        }).done();
};
