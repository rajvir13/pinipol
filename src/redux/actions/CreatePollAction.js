/**
 * Created by Nisha-React on 9/23/20.
 */

import {API, BASE_URL} from '../constants';
import axios from 'axios';
import {Platform} from 'react-native';

export const CreatePollAction = (payload, createPollResponse) => {
    debugger;

    let formData = new FormData();
    formData.append('poll_cat', payload.poll_cat);
    formData.append('poll_title', payload.poll_title);
    formData.append('poll_description', payload.poll_description);
    formData.append('poll_type', payload.poll_type);
    formData.append('make_poll_as', payload.make_poll_as);
    formData.append('created_by', payload.created_by);
    formData.append('allow_votes', payload.allow_votes);
    formData.append('start_date', payload.start_date);
    formData.append('end_date', payload.end_date);
    formData.append('thus_votes', payload.thus_votes);
    formData.append('poll_result', payload.poll_result);
    formData.append('poll_result_validation', payload.poll_result_validation);
    formData.append('option', payload.options);

    let data = JSON.stringify({
        'poll_cat': payload.poll_cat,
        'poll_title': payload.poll_title,
        'poll_description': payload.poll_description,
        'poll_type': payload.poll_type,
        'make_poll_as': payload.make_poll_as,
        'created_by': payload.created_by,
        'allow_votes': payload.allow_votes,
        'start_date': payload.start_date,
        'end_date': payload.end_date,
        'thus_votes': payload.thus_votes,
        'poll_result': payload.poll_result,
        'poll_result_validation': payload.poll_result_validation,
        'option': payload.options,

    });

    axios.post(BASE_URL + API.CREATE_POLL, data, {
        headers: {
            'Authorization': `${'Bearer' + ' ' + payload.accessToken}`,
            // 'Content-type': 'application/x-www-form-urlencoded'
            'Content-type': 'application/json',
        },
    })
        .then((response) => {
            createPollResponse(response.data);
        })
        .catch((error) => {
            if (Platform.OS === 'ios') {
                createPollResponse(error.response.data);
            } else {
                createPollResponse(error);
            }
        }).done();
};



