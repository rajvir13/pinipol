import {combineReducers} from 'redux';
import LoginReducer from './LoginReducer';
import LogoutReducer from './LogoutReducer';

const rootReducer = (state, action) => {
    if (action.type === 'LOGOUT_SUCCESS') {
        state = undefined;
    }
    return appReducer(state, action);
};

const appReducer = combineReducers({
    LoginReducer,
    LogoutReducer
});
export default rootReducer;
