import React from 'react';
import {
    View, Image, Text, TouchableOpacity, Alert,
} from 'react-native';
import {DrawerContentScrollView, DrawerItem} from '@react-navigation/drawer';
import {connect} from 'react-redux';
import {CommonActions} from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';

import {Spacer} from '../../customComponents/Spacer';
import {styles} from './styles';
import BaseClass from '../../utils/BaseClass';
import OrientationLoadingOverlay from '../../customComponents/CustomLoader';
import {LogoutAction} from '../../redux/actions/LogoutAction';
import STRINGS from '../../utils/Strings';
import * as _ from 'lodash';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLORS from '../../utils/Colors';
import {ICONS} from '../../utils/ImagePaths';
import {FONT_FAMILY} from '../../utils/Font';
import {FONT} from '../../utils/FontSizes';
import RightIcon from 'react-native-vector-icons/Entypo';

class CustomDrawerContent extends BaseClass {
    constructor(props) {
        super(props);
        this.setState({
            isLogOut: false,
            authToken: '',
            name: 'Grand Plaza',
            profile: 'https://www.cornwallbusinessawards.co.uk/wp-content/uploads/2017/11/dummy450x450-300x300.jpg',
            ownerId: '',
            address: '',
            phone_number: '',
            userName: '',
            email: '',
            account_type: '',
            account_type_id: 0,
            loginData: {},
        });
    }

    componentDidMount() {
        const {navigation} = this.props;
        this._unsubscribe = navigation.addListener('focus', () => {
            this.onFocusFunction();
        });
    }

    onFocusFunction = () => {
        AsyncStorage.getItem(STRINGS.accessToken, async (error, result) => {
            if (result !== undefined && result !== null) {
                let token = JSON.parse(result);
                await this.setState({
                    authToken: token,
                });
                AsyncStorage.getItem(STRINGS.userPersonalInfo, async (error, result) => {
                    if (result !== undefined && result !== null) {
                        let loginData = JSON.parse(result);
                        await this.setState({
                            loginData: loginData,
                            profile: loginData.profile_image !== null ? loginData.profile_image : 'https://www.cornwallbusinessawards.co.uk/wp-content/uploads/2017/11/dummy450x450-300x300.jpg',
                            userName: loginData.account_type === '1'?`${loginData.firstname + ' ' + loginData.lastname}`:loginData.org_name,
                            account_type: loginData.account_type === '1' ? 'Individual Account' : loginData.account_type === '2' ? 'Business Account' : '',
                            account_type_id: loginData.account_type,

                        });
                    }
                });
            }
        });
    };

    componentWillUnmount() {
        this._unsubscribe();
    }


    componentWillReceiveProps(nextProps, nextContext) {
        const {isLogOut} = this.state;
        const {navigation} = this.props;
        const {logoutResponse} = nextProps.logoutState;
        if (logoutResponse !== undefined && isLogOut) {
            if (logoutResponse === 'Network request failed') {
                this.hideDialog();
                this.showToastAlert(STRINGS.CHECK_INTERNET);
            } else {
                const {message, code} = logoutResponse;
                if (code === '200') {
                    this.hideDialog();
                    this.setState({
                        isLogOut: !isLogOut,
                    });
                    AsyncStorage.removeItem(STRINGS.accessToken);
                    AsyncStorage.removeItem(STRINGS.loginData);
                    AsyncStorage.removeItem(STRINGS.userPersonalInfo);
                    navigation.dispatch(
                        CommonActions.reset({
                            index: 0,
                            routes: [
                                {name: 'login'},
                            ],
                        }),
                    );
                } else if (code === '111') {
                    this.hideDialog();
                    console.warn(message);
                } else {
                    this.showToastAlert('Session expired');
                    this.hideDialog();
                }
            }
        }
        // this.updateUserData();
    }

    // ----------------------------------------
    // ----------------------------------------
    // METHODS
    // ----------------------------------------

    // _goOwnerAddressProfile = () => {
    //     const {ownerName, ownerLastName, email, phone_number, name, address, hotelImages, hotelID, ownerId, lat, long} = this.state;
    //     this.props.navigation.navigate('ownerAddressProfile', {
    //         owner_Name: ownerName,
    //         owner_Last_Name: ownerLastName,
    //         owner_email: email,
    //         owner_mobileNo: phone_number,
    //         hotelName: name,
    //         hotelAddr: address,
    //         hotels_image: hotelImages,
    //         hotelID: hotelID,
    //         ownerId: ownerId,
    //         lat: lat,
    //         long: long,
    //     });
    // };

    logoutUser = () => {
        const {isLogOut, authToken} = this.state;
        Alert.alert(
            STRINGS.APP_NAME,
            STRINGS.EXIT_ALERT,
            [
                {
                    text: 'Cancel', style: 'cancel',
                },
                {
                    text: 'OK', onPress: () => {
                        this.setState({isLogOut: !isLogOut});
                        this.props.logout({accessToken: authToken});
                        this.showDialog();
                    },
                },

            ],
            {cancelable: false});
    };


    // updateUserData() {
    //     const {loginData} = this.state;
    //
    //     AsyncStorage.getItem(STRINGS.loginData).then((result) => {
    //         let userData = JSON.parse(result);
    //         if (loginData !== result) {
    //             if (userData !== undefined && userData !== null) {
    //                 this.setState({
    //                     // loginData: result,
    //                     // name: userData.name,
    //                     // profilePic: userData.profilepic,
    //                     authToken: userData.access_token,
    //                 })
    //
    //             }
    //         }
    //
    //     }).catch((error) => {
    //         console.log(error)
    //     })
    //         .done();
    // }


    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message="Loading.."/>
        );
    };


    render() {
        const {name, profile, userName, account_type, account_type_id} = this.state;
        return (
            <View style={{flex: 1}}>
                <DrawerContentScrollView>
                    <View style={{
                        paddingVertical: wp(5),
                        paddingLeft: wp(7),
                        borderBottomWidth: wp(0.1),
                        borderBottomColor: COLORS.black_color,
                    }}>
                        <View style={{width: wp(50), alignItems: 'center'}}>
                            <Image source={{uri: profile}}
                                   style={{
                                       alignItems: 'center',
                                       borderRadius: wp(25) / 2,
                                       width: wp(25),
                                       height: wp(25),
                                       borderWidth: 0.4,
                                       borderColor: COLORS.black_color,
                                   }}
                                   resizeMode={'cover'}
                            />
                            <Spacer space={2}/>
                            <View style={{width: wp(50), alignItems: 'center', justifyContent: 'center'}}>
                                <Text style={{
                                    fontFamily: FONT_FAMILY.PoppinsBold,
                                    fontSize: FONT.TextSmall_2,
                                    color: COLORS.black_color,
                                }}>{userName}</Text>
                                <Spacer space={0.5}/>
                                <Text style={{
                                    fontFamily: FONT_FAMILY.Roboto,
                                    fontSize: FONT.TextSmall_2,
                                    color: COLORS.black_color,
                                }}>{account_type}</Text>
                            </View>
                        </View>

                    </View>
                    <Spacer space={1}/>
                    <View style={{alignItems: 'center', flexDirection: 'row'}}>
                        <DrawerItem
                            style={{width: wp(85)}}
                            labelStyle={styles.labelTextStyle}
                            label="Home"
                            onPress={() => this.props.navigation.navigate('pHome')}
                        />
                        <View onTouchEnd={() => this.props.navigation.navigate('pHome')}
                              style={{position: 'absolute', zIndex: 1, right: wp(2)}}>
                            <RightIcon name={'chevron-right'} size={wp(6)} color={COLORS.off_black}/>
                        </View>
                    </View>
                    <View style={{alignItems: 'center', flexDirection: 'row'}}>
                        <DrawerItem
                            style={{width: wp(85)}}
                            labelStyle={styles.labelTextStyle}
                            label="My Profile"
                            onPress={() => account_type_id === '1' ? this.props.navigation.navigate('personalProfileScreen') : this.props.navigation.navigate('businessProfileScreen')}
                        />
                        <View onTouchEnd={() => this.props.navigation.navigate('personalProfileScreen')}
                              style={{position: 'absolute', zIndex: 1, right: wp(2)}}>
                            <RightIcon name={'chevron-right'} size={wp(6)} color={COLORS.off_black}/>
                        </View>
                    </View>

                    <View style={{alignItems: 'center', flexDirection: 'row'}}>
                        <DrawerItem
                            style={{width: wp(85)}}
                            labelStyle={styles.labelTextStyle}
                            label="Create a Poll"
                            onPress={() => account_type_id === '1' ? this.props.navigation.navigate('pCreatePoll') : this.props.navigation.navigate('orgCreatePoll')}
                        />
                        <View
                            onTouchEnd={() => account_type_id === '1' ? this.props.navigation.navigate('pCreatePoll') : this.props.navigation.navigate('orgCreatePoll')}
                            style={{position: 'absolute', zIndex: 1, right: wp(2)}}>
                            <RightIcon name={'chevron-right'} size={wp(6)} color={COLORS.off_black}/>
                        </View>
                    </View>

                    <View style={{alignItems: 'center', flexDirection: 'row'}}>
                        <DrawerItem
                            style={{width: wp(85)}}
                            labelStyle={styles.labelTextStyle}
                            label="My Polls"
                            onPress={() => this.showToastAlert(STRINGS.COMING_SOON)}
                        />
                        <View onTouchEnd={() => this.showToastAlert(STRINGS.COMING_SOON)}
                              style={{position: 'absolute', zIndex: 1, right: wp(2)}}>
                            <RightIcon name={'chevron-right'} size={wp(6)} color={COLORS.off_black}/>
                        </View>
                    </View>

                    <View style={{alignItems: 'center', flexDirection: 'row'}}>
                        <DrawerItem
                            style={{width: wp(85)}}
                            labelStyle={styles.labelTextStyle}
                            label="My Votes"
                            onPress={() => this.showToastAlert(STRINGS.COMING_SOON)}
                        />
                        <View onTouchEnd={() => this.showToastAlert(STRINGS.COMING_SOON)}
                              style={{position: 'absolute', zIndex: 1, right: wp(2)}}>
                            <RightIcon name={'chevron-right'} size={wp(6)} color={COLORS.off_black}/>
                        </View>
                    </View>

                    <View style={{alignItems: 'center', flexDirection: 'row'}}>
                        <DrawerItem
                            style={{width: wp(85)}}
                            labelStyle={styles.labelTextStyle}
                            label="Popular Polls"
                            onPress={() => this.props.navigation.navigate('popularHome')}
                        />
                        <View onTouchEnd={() => this.props.navigation.navigate('popularHome')}
                              style={{position: 'absolute', zIndex: 1, right: wp(2)}}>
                            <RightIcon name={'chevron-right'} size={wp(6)} color={COLORS.off_black}/>
                        </View>
                    </View>

                    <View style={{alignItems: 'center', flexDirection: 'row'}}>
                        <DrawerItem
                            style={{width: wp(85)}}
                            labelStyle={styles.labelTextStyle}
                            label="My Connections"
                            onPress={() => this.showToastAlert(STRINGS.COMING_SOON)}
                        />
                        <View onTouchEnd={() => this.showToastAlert(STRINGS.COMING_SOON)}
                              style={{position: 'absolute', zIndex: 1, right: wp(2)}}>
                            <RightIcon name={'chevron-right'} size={wp(6)} color={COLORS.off_black}/>
                        </View>
                    </View>

                    <View style={{alignItems: 'center', flexDirection: 'row'}}>
                        <DrawerItem
                            style={{width: wp(85)}}
                            labelStyle={styles.labelTextStyle}
                            label="Notifications"
                            onPress={() => this.showToastAlert(STRINGS.COMING_SOON)}
                        />
                        <View onTouchEnd={() => this.showToastAlert(STRINGS.COMING_SOON)}
                              style={{position: 'absolute', zIndex: 1, right: wp(2)}}>
                            <RightIcon name={'chevron-right'} size={wp(6)} color={COLORS.off_black}/>
                        </View>
                    </View>

                    <View style={{alignItems: 'center', flexDirection: 'row'}}>
                        <DrawerItem
                            style={{width: wp(85)}}
                            labelStyle={styles.labelTextStyle}
                            label="Settings"
                            onPress={() => this.showToastAlert(STRINGS.COMING_SOON)}
                        />
                        <View onTouchEnd={() => this.showToastAlert(STRINGS.COMING_SOON)}
                              style={{position: 'absolute', zIndex: 1, right: wp(2)}}>
                            <RightIcon name={'chevron-right'} size={wp(6)} color={COLORS.off_black}/>
                        </View>
                    </View>

                    <DrawerItem
                        labelStyle={styles.labelTextStyle}
                        label="Logout"
                        // icon={() => <OwnerLogoutComponent/>}
                        onPress={() => this.logoutUser()}
                    />
                </DrawerContentScrollView>
                {/*{this._renderCustomLoader()}*/}
            </View>
        );
    }
}

// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state) => ({
    logoutState: state.LogoutReducer,
});

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
    return {
        logout: (payload) => dispatch(LogoutAction(payload)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomDrawerContent);

// export default CustomDrawerContent
