import React from 'react';
import {
    Image,
    KeyboardAvoidingView,
    Keyboard,
    Text,
    TouchableWithoutFeedback,
    View, TouchableOpacity, ImageBackground,
} from 'react-native';
import Menu, { MenuItem } from 'react-native-material-menu';
import styles from './styles';

import { Input } from 'react-native-elements';
import BaseClass from '../../../../utils/BaseClass';
import { MainContainer, SafeAreaViewContainer, ScrollContainer } from '../../../../utils/BaseStyle';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { ICONS } from '../../../../utils/ImagePaths';
import EditIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import STRINGS from '../../../../utils/Strings';
import COLORS from '../../../../utils/Colors';
import { Spacer } from "../../../../customComponents/Spacer";
import { validateEmail, validatePassword, validatePhnNumber } from '../../../../utils/Validations';
import moment from 'moment';
import CheckSVG from '../../../../customComponents/imagesComponents/Check';
import UnCheckSVG from '../../../../customComponents/imagesComponents/Uncheck';
import { PrimaryButton } from '../../../../customComponents/ButtonComponent';
import TopHeader from '../../../../customComponents/Header';
import OrientationLoadingOverlay from '../../../../customComponents/CustomLoader';
import { FONT_FAMILY } from "../../../../utils/Font";
import { FONT } from '../../../../utils/FontSizes';
// import {navigate} from "../../../../../RootNavigation";
import CountryPicker from "react-native-country-picker-modal";
import EntypoIcon from "react-native-vector-icons/Entypo";

class BusinessAddMoreDetails extends BaseClass {
    constructor(props) {
        let userLocaleCountryCode = 'IN';
        super(props);
        const { data } = this.props.route.params;
        console.warn(data)
        this.state = {

            validAdminName: '',
            labelAdminName: 'Admin First Name',
            adminName: data.firstname === 'undefined' ? '' :  data.firstname,


            validAdminLName: '',
            labelAdminLName: 'Admin Last Name',
            adminLastName:  data.lastname === 'undefined' ? '' :  data.lastname,


            validEmail: '',
            labelEmail: 'Email Address',
            useremail:  data.email === 'undefined' ? '' :  data.email,

            validNumber: '',
            labelNumber: 'Contact No.',
            usernumber: '140 325 XXXX',

            areAllValuesValid: false,
            isLoading: false,



            cca2: userLocaleCountryCode,
            callingCode: '+91',
            isDropvisible: false,

            phoneNoText: data. phone_no === 'undefined' ? '' :  data.phone_no,


        };
    }

    _onSaveButton = () => {
        // this.showDialog();
        const { navigation, route } = this.props;
        // alert("Coming Soon")
        navigation.goBack();
    };


    onChange = async (text, type) => {
        let arr = [1];
        await arr.map(item => {
            switch (type) {
                case 'adminName':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelAdminName: '',
                        });
                    } else if (text.trim().length < 3) {
                        this.setState({
                            validAdminName: STRINGS.name_minimum_three,
                            labelAdminName: 'Admin First Name',
                        });
                    } else {
                        this.setState({
                            validAdminName: '',
                        });
                    }
                    this.setState({ adminName: text });
                    break;
                case 'adminLastName':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelAdminLName: '',
                        });
                    } else if (text.trim().length < 3) {
                        this.setState({
                            validAdminLName: STRINGS.name_minimum_three,
                            labelAdminLName: 'Admin Last Name',
                        });
                    } else {
                        this.setState({
                            validAdminLName: '',
                        });
                    }
                    this.setState({ adminLastName: text });
                    break;

                case 'useremail':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelEmail: '',
                        });
                    } else if (!validateEmail(text)) {
                        this.setState({
                            validEmail: STRINGS.valid_email,
                            labelEmail: 'Email ID',
                        });
                    } else {
                        this.setState({
                            validEmail: '',
                        });
                    }
                    this.setState({ useremail: text });
                    break;
                case 'usernumber':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelNumber: '',
                        });
                    } else if (!validatePhnNumber(text)) {
                        this.setState({
                            validNumber: STRINGS.valid_Phone,
                            labelNumber: 'Phone Number',
                        });
                    } else {
                        this.setState({
                            validNumber: '',
                        });
                    }
                    this.setState({ usernumber: text });
                    break;

                default:
                    break;

            }
        });
        this.checkAllValues();
    };

    checkAllValues = () => {
        const {
            validAdminName, adminName, validAdminLName, adminLastName, useremail, validEmail, validNumber, usernumber
        } = this.state;
        if (validAdminName !== '' && adminName.length === 0) {
            this.setState({ areAllValuesValid: false });
        } else if (validAdminLName !== '' && adminLastName.length === 0) {
            this.setState({ areAllValuesValid: false });
        } else if (validEmail !== '' && useremail.length === 0) {
            this.setState({ areAllValuesValid: false });
        } else if (validNumber !== '' && usernumber.length === 0) {
            this.setState({ areAllValuesValid: false });
        }
        else {
            this.setState({ areAllValuesValid: true });
        }
    };


    //===========================================================
    //===========================country code====================
    //===========================================================

    onCountrySelect = (value) => {
        this.setState({
            cca2: value.cca2,
            callingCode: `+${value.callingCode[0]}`,
        });
    };

    DropDownView = () => {
        const { isDropvisible } = this.state;
        return (
            <TouchableOpacity onPress={() => this.setState({ isDropvisible: !isDropvisible })}>
                <EntypoIcon name={'chevron-down'} size={wp(5)} />
            </TouchableOpacity>
        );
    };


    _renderCountryPicker = () => {
        return (
            <CountryPicker
                renderFlagButton={this.DropDownView}
                countryList={this.state.countryData}
                onSelect={(value) => {
                    this.onCountrySelect(value);
                }}
                hideAlphabetFilter={true}
                showCallingCode={true}
                filterPlaceholder={STRINGS.SEARCH_TEXT}
                closeable={true}
                filterable={true}
                cca2={this.state.cca2}
                translation="eng"
                autoFocusFilter={false}
                countryCode={this.state.cca2}
                withFilter={true}
                visible={this.state.isDropvisible}
                onClose={() => this.setState({ isDropvisible: false })}
            />
        );
    };





    _renderInputs = () => {


        const { phoneNoText, callingCode,
            validAdminName, labelAdminName, adminName, validAdminLName, labelAdminLName, adminLastName, useremail,
            validEmail, labelEmail, validNumber, labelNumber, usernumber
        } = this.state;
        return (
            <View style={{ alignItems: 'center' }}>
                <Input
                    placeholder='Admin First Name'
                    ref={'adminName'}
                    label={labelAdminName}
                    labelStyle={styles.input_label_style}
                    errorStyle={styles.input_error_style}
                    errorMessage={validAdminName}
                    inputStyle={styles.input_input_container}
                    inputContainerStyle={{ width: wp(90) }}
                    value={adminName}
                    onChangeText={(text) => this.onChange(text, 'adminName')}
                    onSubmitEditing={() => this.refs.AdminlastName.focus()}
                    returnKeyType={'next'}

                />
                <Spacer space={1} />
                <Input
                    ref={'AdminlastName'}
                    placeholder='Admin Last Name'
                    label={labelAdminLName}
                    labelStyle={styles.input_label_style}
                    errorStyle={styles.input_error_style}
                    errorMessage={validAdminLName}
                    inputStyle={styles.input_input_container}
                    inputContainerStyle={{ width: wp(90) }}
                    value={adminLastName}
                    onChangeText={(text) => this.onChange(text, 'adminLastName')}
                    onSubmitEditing={() => this.refs.UserEmail.focus()}
                    returnKeyType={'next'}
                />
                <Spacer space={1} />
                <Input
                    ref={'UserEmail'}
                    placeholder='Email ID'
                    label={labelEmail}
                    labelStyle={styles.input_label_style}
                    errorStyle={styles.input_error_style}
                    inputStyle={styles.input_input_container}
                    errorMessage={validEmail}
                    inputContainerStyle={{ width: wp(90) }}
                    value={useremail}
                    onChangeText={(text) => this.onChange(text, 'useremail')}
                    onSubmitEditing={() => this.refs.PhoneNumber.focus()}
                    returnKeyType={'next'}
                />
                <Spacer space={1} />
                <View>
                    <Text style={{
                        marginLeft: wp(1),
                        fontFamily: FONT_FAMILY.RobotoBold,
                        fontSize: FONT.textSmall1,
                        color: COLORS.off_black,
                    }}>Phone No.</Text>

                    <View style={{
                        flexDirection: 'row'
                    }}>

                        <Input
                            inputContainerStyle={{}}
                            editable={false}
                            pointerEvents={'none'}
                            placeholder={`+${91}`}
                            // leftIcon={this._renderCountryPicker}
                            leftIconContainerStyle={{ marginLeft: 0, alignItems: 'center' }}
                            value={callingCode}
                            containerStyle={{
                                width: wp('35%'),
                            }}
                            rightIcon={
                                this._renderCountryPicker
                            }
                            inputStyle={{
                                fontSize: FONT.TextMedium_2,
                                marginHorizontal: wp(1),
                                // marginTop:wp(2)
                            }}
                        />
                        <Spacer row={3} />
                        <Input
                            inputContainerStyle={{
                                width: wp('54%'),
                            }}
                            placeholder={STRINGS.MOBILE_NUMBER}
                            placeholderTextColor={COLORS.off_black}
                            ref="Mobile"
                            value={phoneNoText}
                            onChangeText={(text) =>
                                this.setState({
                                    phoneNoText: text.trim() && text.replace(/^0+/, ''),
                                })
                            }
                            maxLength={10}
                            keyboardType="phone-pad"
                            returnKeyType={'next'}
                            inputStyle={{
                                paddingLeft: wp(0.5),
                                fontSize: FONT.TextMedium_2,
                            }}
                            containerStyle={{
                                paddingBottom: 0,
                                alignItems: 'center',
                                width: wp('50%'),
                            }}
                            rightIcon={<EntypoIcon size={wp(6)} />}
                        />
                    </View>
                </View>
            </View>
        );
    };


    // =============================================================================================
    // Render method for Custom Loader
    // =============================================================================================

    _renderCustomLoader = () => {
        const { isLoading } = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={STRINGS.LOADING_TEXT} />
        );
    };

    render() {
        const { areAllValuesValid } = this.state;
        const { navigation } = this.props;
        const { navigate } = navigation;
        return (
            <SafeAreaViewContainer>
                <TopHeader
                    rightComponentNotVisible={true}
                    text={"Add More Details"}
                    onPressBackArrow={() => navigation.goBack()} />
                <KeyboardAvoidingView
                    style={styles.keyboard_style}
                    behavior={(Platform.OS === 'ios') ? 'padding' : null}>
                    <TouchableWithoutFeedback
                        onPress={() => {
                            Keyboard.dismiss();
                        }}>
                        <ScrollContainer>
                            <MainContainer>
                                <Spacer space={5} />
                                {this._renderInputs()}
                                <Spacer space={4} />
                                <PrimaryButton
                                    onPress={() => this._onSaveButton()}
                                    //isDisabled={!areAllValuesValid}
                                    // bgColor={areAllValuesValid ? COLORS.app_theme_color : COLORS.app_theme_color_disabled}
                                    bgColor={COLORS.app_theme_color}
                                    text={STRINGS.SAVE_TEXT} />
                                <Spacer space={4} />
                            </MainContainer>
                            {this._renderCustomLoader()}
                        </ScrollContainer>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </SafeAreaViewContainer>
        );
    }
}


export default BusinessAddMoreDetails;
