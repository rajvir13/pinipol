import React from 'react';
import styles from './styles';

import {CreateText, InnerText, NormalText, NormalInnerText} from './styles';

import {
    Image,
    View,
    SafeAreaView,
    Text,
    TextInput,
    Alert,
    KeyboardAvoidingView,
} from 'react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ICONS} from '../../../../utils/ImagePaths';
import BaseClass from '../../../../utils/BaseClass';
import {FONT_FAMILY} from '../../../../utils/Font';
import {FONT} from '../../../../utils/FontSizes';
import COLORS from '../../../../utils/Colors';
import STRINGS from '../../../../utils/Strings';
import AsyncStorage from '@react-native-community/async-storage';
import {
    MainContainer,
    SafeAreaViewContainer,
    ScrollContainer,
} from '../../../../utils/BaseStyle';
import {Spacer} from '../../../../customComponents/Spacer';
import TopHeader from '../../../../customComponents/Header';
import {LogoutAction} from '../../../../redux/actions/LogoutAction';
import connect from 'react-redux/lib/connect/connect';
import {CommonActions} from '@react-navigation/native';
import OrientationLoadingOverlay from '../../../../customComponents/CustomLoader';
// import { RandomAvatarAction } from "../../../../redux/actions/RandomAvatarAction";
import {DrawerContentScrollView} from '@react-navigation/drawer';
import {PrimaryButton} from '../../../../customComponents/ButtonComponent';
import {GetBusinessProfile} from '../../../../redux/actions/GetBusinessProfileAction';

class BusinessProfileScreen extends BaseClass {
    constructor(props) {
        super(props);
        const {route} = this.props;
        this.state = {
            authToken: undefined,
            data: [],
            userInfo: {},
            image: 'https://www.cornwallbusinessawards.co.uk/wp-content/uploads/2017/11/dummy450x450-300x300.jpg',
            organizationName: '',
            sectorName: '',
            firstname: '',
            lastname: '',
            street: '',
            city: '',
            country: '',
            phone_number: '',
        };
    }

    componentDidMount() {
        const {navigation} = this.props;
        this._unsubscribe = navigation.addListener('focus', () => {
            this.onFocusFunction();
        });
    }

    onFocusFunction = () => {

        AsyncStorage.getItem(STRINGS.accessToken, async (error, result) => {
            if (result !== undefined && result !== null) {
                let token = JSON.parse(result);
                console.log('result is', result);
                await this.setState({
                    authToken: token,
                });
                AsyncStorage.getItem(STRINGS.loginData, async (error, result) => {
                    if (result !== undefined && result !== null) {
                        let loginData = JSON.parse(result);
                        await this.setState({
                            loginData: loginData,
                            profile: loginData.profile_image !== null ? loginData.profile_image : 'https://www.cornwallbusinessawards.co.uk/wp-content/uploads/2017/11/dummy450x450-300x300.jpg',
                            userName: loginData.username,
                            account_type: loginData.account_type === '1' ? 'Personal Account' : loginData.account_type === '2' ? 'Business Account' : '',
                            account_type_id: loginData.account_type,
                        });
                        this.showDialog();
                        GetBusinessProfile(
                            {
                                accessToken: token,
                                account_type: loginData.account_type,
                            },
                            (data) => this.getProfileResponse(data),
                        );
                    }
                });

            }
        });
    };


    componentWillUnmount() {
        this._unsubscribe();
    }


    getProfileResponse = (response) => {

        const {image} = this.state;
        if (response === 'Network request failed') {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET);
        } else {
            const {message, code, data, user_meta} = response;
            console.log('data is----', data);
            const {navigation} = this.props
            if (code === '200') {
                this.hideDialog();
                this.setState({
                    data: data,
                    userInfo: user_meta,
                    image: user_meta !== null && user_meta.profile_image !== null ? user_meta.profile_image : image,
                    organizationName: user_meta !== null && user_meta.organization_name !== null ? user_meta.organization_name : '',
                    sectorName: user_meta !== null && user_meta.sector_name !== null ? user_meta.sector_name : '',
                    firstname: data !== null && data.firstname !== null ? data.firstname : '',
                    lastname: data !== null && data.lastname !== null ? data.lastname : '',
                    street: user_meta !== null && user_meta.address !== null ? user_meta.address : '',
                    city: user_meta !== null && user_meta.city_name !== null ? user_meta.city_name : '',
                    country: user_meta !== null && user_meta.country !== null ? user_meta.country : '',
                    phone_number: data !== null && data.phone_no !== null ? data.phone_no : '',
                });


                AsyncStorage.setItem(STRINGS.userPersonalInfo, JSON.stringify({
                    'profile_image': user_meta.profile_image,
                    'firstname': data.firstname,
                    'lastname': data.lastname,
                    'account_type': data.account_type,
                    "org_name": data.name,
                }));
            } else if (code === '111') {
                this.hideDialog();
                console.warn(message);
            } else if (code === '201') {
                this.hideDialog();
                this.showToastAlert('User Unauthorized');
            } else if (code === '401') {
                this.hideDialog();
                AsyncStorage.removeItem(STRINGS.accessToken);
                AsyncStorage.removeItem(STRINGS.loginData);
                AsyncStorage.removeItem(STRINGS.userPersonalInfo);
                navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [
                            {name: 'login'},
                        ],
                    }),
                );
                this.showToastAlert(message);
            }
            else {
                this.showToastAlert('Session expired');
                this.hideDialog();
            }
        }

    };

    //-------------response profile end------------------------
    // componentWillUnmount() {
    //     this._unsubscribe();
    // }

    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message="Loading.."/>
        );
    };

    _openDrawer = () => {
        const {navigation} = this.props;
        navigation.openDrawer();
    };
    editProfile = () => {
        const {data, userInfo} = this.state;
        // alert("Coming Soon")
        const {navigate} = this.props.navigation;
        navigate('businessEditProfile', {
            Alldata: data,
            userData: userInfo,

        });
    };

    onEmailEditingEnd = () => {
        // this.showDialog();
        alert('Coming Soon');
    };

    render() {
        const {navigation} = this.props;
        const {
            image, data, userInfo, organizationName, sectorName, city, street,
            country, firstname, lastname, phone_number,
        } = this.state;
        return (
            <SafeAreaViewContainer>
                {console.log('business addres', userInfo)}
                <TopHeader
                    text={'Profile'}
                    //  isEdit={true} openPressIcon={() => this.editProfile()}
                    onPressBackArrow={() => navigation.goBack()}
                    onRightArrow={() => this.editProfile()}
                    isRight={true}
                />
                <ScrollContainer>
                    <MainContainer>
                        <Spacer space={2}/>
                        <Image
                            style={{height: wp(30), width: wp(30), borderRadius: wp(30) / 2}}
                            source={{uri: image}}
                            resizeMode={'cover'}
                        />
                        <Spacer space={5}/>
                        <View
                            style={{
                                width: wp(90),
                                flexDirection: 'column',
                            }}
                        >
                            <CreateText>{data.name}</CreateText>
                            <Spacer space={0.3}/>
                            {data.account_type === '2' ? <InnerText>{'Business Account'}</InnerText> :
                                <InnerText>{'Personal Account'}</InnerText>}

                            <Spacer space={1}/>
                            <Text
                                style={{
                                    fontFamily: FONT_FAMILY.Roboto,
                                    fontSize: FONT.textSmall1,
                                    fontStyle: 'normal',
                                    color: COLORS.black_color,
                                }}
                            >
                                {data.email}
                            </Text>
                            <Spacer space={1.5}/>
                            <View style={{flexDirection: 'row', width: wp(90)}}>
                                <NormalText>{'Legal Name'}</NormalText>
                                <Text>:</Text>
                                <Spacer row={.5}/>
                                <NormalInnerText>
                                    {firstname + ' ' + lastname}</NormalInnerText>
                            </View>
                            <Spacer space={1.5}/>
                            <View style={{flexDirection: 'row'}}>
                                <NormalText>{'Organization Type'}</NormalText>
                                <Text>:</Text>
                                <Spacer row={.5}/>
                                <NormalInnerText>{organizationName}</NormalInnerText>
                            </View>
                            <Spacer space={1.5}/>
                            <View style={{flexDirection: 'row'}}>
                                <NormalText>{'Sector'}</NormalText>
                                <Text>:</Text>
                                <Spacer row={.5}/>
                                <NormalInnerText>{sectorName}</NormalInnerText>
                            </View>
                            <Spacer space={1.5}/>
                            <View style={{flexDirection: 'row'}}>
                                <NormalText>{'Phone No.'}</NormalText>
                                <Text>:</Text>
                                <Spacer row={.5}/>
                                <NormalInnerText>{phone_number}</NormalInnerText>
                            </View>
                            <Spacer space={1.5}/>
                            <View style={{flexDirection: 'row'}}>
                                <NormalText>{'Business Address'}</NormalText>
                                <Text>:</Text>
                                <Spacer row={.5}/>
                                <Text
                                    style={{
                                        color: COLORS.black_color,
                                        fontFamily: FONT_FAMILY.Roboto,
                                        fontSize: FONT.textSmall1,
                                        width: wp(30),
                                    }}
                                    numberOfLines={3}
                                >
                                    {street + ',\n' + city + ',\n' + country}
                                </Text>
                            </View>
                            <Spacer space={1}/>
                        </View>
                        <Spacer space={4}/>
                        <View
                            style={{
                                width: wp(100),
                                height: hp(0.1),
                                backgroundColor: COLORS.placeHolder_color,
                            }}
                        />
                        <Spacer space={4}/>
                        <View
                            style={{
                                flexDirection: 'column',
                                justifyContent: 'flex-start',
                                width: wp(90),
                            }}
                        >
                            {/* <Text
                style={{
                  fontFamily: FONT_FAMILY.PoppinsBold,
                  fontSize: FONT.textSmall1,
                  fontStyle: "normal",
                  color: COLORS.black_color,
                }}
              >
                {"Subscription Plan"}
              </Text>
              <Spacer space={2} />
              <Text
                style={{
                  fontFamily: FONT_FAMILY.Roboto,
                  fontSize: FONT.textSmall1,
                  fontWeight: "400",
                  fontStyle: "normal",
                  color: COLORS.black_color,
                }}
              >
                {"Gold Plan (15 Polls/month)"}
              </Text> */}
                        </View>

                        <Spacer space={4}/>
                        {/* <PrimaryButton
              onPress={() => this.onEmailEditingEnd()}
              //isDisabled={!areAllValuesValid}
              //bgColor={areAllValuesValid ? COLORS.app_theme_color : COLORS.app_theme_color_disabled}
              bgColor={COLORS.app_theme_color}
              text={STRINGS.UPGRADE_TEXT}
            /> */}
                    </MainContainer>
                    {this._renderCustomLoader()}
                </ScrollContainer>
            </SafeAreaViewContainer>
        );
    }
}

export default BusinessProfileScreen;
