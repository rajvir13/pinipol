import React from 'react';
import {
    Image,
    KeyboardAvoidingView,
    Keyboard,
    Text,
    TouchableWithoutFeedback,
    View, TouchableOpacity, ImageBackground, Platform, Alert,
} from 'react-native';
import {
    CreateBusinessText,
    StartText,
    styles,
    terms_conditions,
} from './styles';
import Menu, { MenuItem } from 'react-native-material-menu';


import DateTimePickerModal from 'react-native-modal-datetime-picker';
import { Input } from 'react-native-elements';
import BaseClass from '../../../../utils/BaseClass';
import { MainContainer, SafeAreaViewContainer, ScrollContainer } from '../../../../utils/BaseStyle';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { ICONS } from '../../../../utils/ImagePaths';
import EditIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import STRINGS from '../../../../utils/Strings';
import COLORS from '../../../../utils/Colors';
import { Spacer } from '../../../../customComponents/Spacer';
import { validateEmail, validatePassword, validateWebsite } from '../../../../utils/Validations';
import moment from 'moment';
import CheckSVG from '../../../../customComponents/imagesComponents/Check';
import UnCheckSVG from '../../../../customComponents/imagesComponents/Uncheck';
import { PrimaryButton } from '../../../../customComponents/ButtonComponent';
import TopHeader from '../../../../customComponents/Header';
import OrientationLoadingOverlay from '../../../../customComponents/CustomLoader';
import { FONT_FAMILY } from '../../../../utils/Font';
import { FONT } from '../../../../utils/FontSizes';
import Autocomplete from '../../../../customComponents/ModalDropdown/AutoComplete';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import CountryPicker from 'react-native-country-picker-modal';
import {
    CityAction,
    CountryAction,
    OrganizationTypeAction,
    SectorTypeAction,
} from '../../../../redux/actions/DropDownACtions';
import { TransformCityData, TransformCountryData, TransformDropdownData } from '../../../../utils/DataTransformation';
import ImagePicker from 'react-native-image-crop-picker';
import { UpdateProfileBusinessAction } from '../../../../redux/actions/UpdateProfilePersonalAction';
import AsyncStorage from '@react-native-community/async-storage';
import { CommonActions } from "@react-navigation/native"

class BusinessEditProfile extends BaseClass {
    constructor(props) {
        super(props);
        let userLocaleCountryCode = 'IN';
        const { Alldata, userData } = this.props.route.params;
        this.state = {
            data: this.props.route.params.Alldata,
            user_metaData: this.props.route.params.userData,

            validName: '',
            labelName: 'Name',
            userName: Alldata.name !== null ? Alldata.name : '',

            validEmail: '',
            labelEmail: 'Email ID',
            userEmail: Alldata.email !== null ? Alldata.email : '',


            validfirstName: '',
            labelfirstName: 'Legal First Name',
            lFirstName: Alldata.firstname !== null ? Alldata.firstname : '',

            validLastName: '',
            labelLastName: 'Legal Last Name',
            lLastName: Alldata.lastname !== null ? Alldata.lastname : '',

            validPositionName: '',
            labelPositionName: 'Position',
            postionName: userData !== null && userData.position !== null ? userData.position : '',

            isOrganizationType: true,
            labelOrganization: 'Organisation Type',
            organizationResponse: [],
            orgId: userData !== null && userData.organisation_type !== null ? parseInt(userData.organisation_type) : '',
            organisationType: userData !== null && userData.organization_name !== null ? userData.organization_name : '',
            orgError: '',

            sector: userData !== null && userData.sector_name !== null ? userData.sector_name : '',
            isSectorType: true,
            labelSector: 'Sector',
            sectorResponse: [],
            sectorId: userData !== null && userData.sector !== null ? parseInt(userData.sector) : '',
            sectorError: '',

            phoneNoText: Alldata !== null && Alldata.phone_no !== null ? Alldata.phone_no : '',

            streetAddress: userData !== null && userData.address !== null ? userData.address : '',
            validStreetAddress: '',
            labelStreetAddress: 'Street Address',

            countryName: userData !== null && userData.country !== null ? userData.country : '',
            isCountryName: true,
            countryId: 0,
            labelCountry: 'Country',
            countryResponse: [],
            countryError: '',

            cityResponse: [],
            cityName: userData !== null && userData.city_name !== null ? userData.city_name : '',
            isCityName: true,
            labelCity: 'City',
            cityId: userData !== null && userData.city !== null ? parseInt(userData.city) : '',
            cityError: '',

            zipCode: userData !== null && userData.zipcode !== null ? userData.zipcode : '',
            validZipCode: '',
            labelZipCode: 'Zip Code',

            website: userData !== null && userData.website !== null ? userData.website : '',
            validWebsite: '',
            labelWebsite: 'Website',

            isChecked: false,
            areAllValuesValid: false,

            cca2: userLocaleCountryCode,
            callingCode: '+91',
            isDropvisible: false,
            image: userData !== null && userData.profile_image !== null ? userData.profile_image : 'https://www.cornwallbusinessawards.co.uk/wp-content/uploads/2017/11/dummy450x450-300x300.jpg',
            authToken: '',


            /*   orgEmail: email,
               orgName: orgName,
               password: password,
               cPassword: password,
               userName: userName,
               socialId: socialId,
               socialPlatform: socialPlatform,
               socialEmail: socialEmail,
               id: id,*/

            isLoading: false,
            profileBase64: '',
        };
    }

    componentDidMount() {
        const { navigation } = this.props;
        AsyncStorage.getItem(STRINGS.accessToken, async (error, result) => {
            if (result !== undefined && result !== null) {
                let loginData = JSON.parse(result);
                console.log('result is', result);
                await this.setState({
                    authToken: loginData,
                });
            }
        });
    }

    _renderBusinessContact = () => {
        const {
            validfirstName, labelfirstName, lFirstName,
            validLastName, labelLastName, lLastName,
            validPositionName, labelPositionName, postionName,
            data, user_metaData,
            validName, labelName, userName,
            validEmail, labelEmail, userEmail,

        } = this.state;
        return (
            <View style={{ alignItems: 'center' }}>
                <Input
                    placeholder={'Name'}
                    ref={'Name'}
                    label={labelName}
                    labelStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}
                    errorStyle={{
                        color: COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}
                    errorMessage={validName}
                    inputStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                    }}
                    inputContainerStyle={{ width: wp(90) }}
                    value={userName}
                    onChangeText={(text) => this.onChange(text, 'name')}
                    onSubmitEditing={() => this.refs.Email.focus()}
                    returnKeyType={'next'}
                />

                <Input
                    editable={false}
                    placeholder={'User Name'}
                    ref={'UserName'}
                    label={'User Name'}
                    labelStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}
                    errorStyle={{
                        color: COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}
                    // errorMessage={validName}
                    inputStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                    }}
                    inputContainerStyle={{ width: wp(90) }}
                    value={data.username}
                    // onChangeText={(text) => this.onChange(text, 'name')}
                    onSubmitEditing={() => this.refs.Email.focus()}
                    returnKeyType={'next'}
                />

                <Input
                    editable={false}
                    placeholder={'Email ID'}
                    ref={'Email'}
                    label={labelEmail}
                    labelStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}
                    errorStyle={{
                        color: COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}
                    errorMessage={validEmail}
                    inputStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                    }}
                    inputContainerStyle={{ width: wp(90) }}
                    value={userEmail}
                    onChangeText={(text) => this.onChange(text, 'email')}
                    onSubmitEditing={() => this.refs.FirstName.focus()}
                    returnKeyType={'next'}
                />
                <Input
                    placeholder={STRINGS.ABOUT_BUSINESS_LEGAL_FIRST_PLACEHOLDER}
                    ref={'FirstName'}
                    label={labelfirstName}
                    labelStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}
                    errorStyle={{
                        color: COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}
                    errorMessage={validfirstName}
                    inputStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                    }}
                    inputContainerStyle={{ width: wp(90) }}
                    value={lFirstName}
                    onChangeText={(text) => this.onChange(text, 'firstName')}
                    onSubmitEditing={() => this.refs.LastName.focus()}
                    returnKeyType={'next'}
                />
                <Input
                    placeholder={STRINGS.ABOUT_BUSINESS_LEGAL_LAST_PLACEHOLDER}
                    ref={'LastName'}
                    label={labelLastName}
                    labelStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}
                    errorStyle={{
                        color: COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}
                    errorMessage={validLastName}
                    inputStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                    }}
                    inputContainerStyle={{ width: wp(90) }}
                    value={lLastName}
                    onChangeText={(text) => this.onChange(text, 'lastname')}
                    onSubmitEditing={() => this.refs.Position.focus()}
                    returnKeyType={'next'}
                />

                <Input
                    placeholder={STRINGS.ABOUT_BUSINESS_POSITION_PLACEHOLDER}
                    ref={'Position'}
                    label={labelPositionName}
                    labelStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}
                    errorStyle={{
                        color: validPositionName === 'Position Available' ? COLORS.valid_Color : COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}
                    errorMessage={validPositionName}
                    inputStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                    }}
                    inputContainerStyle={{ width: wp(90) }}
                    value={postionName}
                    onChangeText={(text) => this.onChange(text, 'position')}
                    onSubmitEditing={() => this.refs.OrgType.focus()}
                    returnKeyType={'done'}
                />
            </View>
        );
    };
    _renderBusinessDetail = () => {
        const {
            organisationType, sector, isOrganizationType, organizationResponse, orgError,
            labelOrganization, isSectorType, sectorResponse, labelSector, sectorError,
        } = this.state;
        return (
            <>
                <View style={{
                    flex: 1,
                    // position:'absolute',
                    zIndex: 10,
                    width: wp(90),
                }}>
                    {labelOrganization.length !== 0 && <Text style={{
                        fontFamily: FONT_FAMILY.RobotoBold,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}>{STRINGS.ABOUT_BUSINESS_ORGANISATION_TYPE_PLACEHOLDER}</Text>}
                    <Autocomplete
                        placeholder={STRINGS.ABOUT_BUSINESS_ORGANISATION_TYPE_PLACEHOLDER}
                        placeholderTextColor={COLORS.placeHolder_color}
                        ref={'OrgType'}
                        data={organizationResponse}
                        style={{
                            paddingVertical: wp(1),
                            fontFamily: FONT_FAMILY.Roboto,
                            fontSize: FONT.TextMedium_2,
                            color: COLORS.black_color,
                        }}
                        inputContainerStyle={{
                            paddingVertical: wp(2),
                            backgroundColor: COLORS.backGround_color,
                            width: wp(90),
                            borderWidth: 0,
                            borderBottomWidth: wp(0.35),
                            borderBottomColor: 'rgb(163,171,180)',
                        }}
                        listContainerStyle={{
                            width: wp(90),
                        }}
                        listStyle={{
                            height: wp(45),
                            width: wp(90),
                            marginLeft: 0,
                        }}

                        hideResults={isOrganizationType}
                        defaultValue={organisationType}
                        onChangeText={(text) => this.onChange(text, 'org')}
                        renderItem={({ item, i }) => (
                            <TouchableOpacity
                                onPress={() =>
                                    this.setState({
                                        organisationType: item.label,
                                        isOrganizationType: true,
                                        orgId: parseInt(item.value),
                                    })
                                }>
                                <Text style={{
                                    padding: wp(2), fontFamily: FONT_FAMILY.Roboto,
                                    fontSize: FONT.TextMedium_2,
                                    color: COLORS.black_color,
                                }}>{item.label}</Text>
                                <View style={{ height: wp(0.2), backgroundColor: 'rgb(163,171,180)', width: wp(90) }} />
                                <Spacer space={0.5} />
                            </TouchableOpacity>
                        )}
                    />
                    {orgError !== '' && <Text style={{
                        paddingTop: wp(1),
                        color: COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}>{orgError}</Text>}
                </View>
                <Spacer space={2} />
                <View style={{
                    flex: 1,
                    zIndex: 8,
                    width: wp(90),
                }}>
                    {labelSector.length !== 0 && <Text style={{
                        fontFamily: FONT_FAMILY.RobotoBold,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}>{STRINGS.ABOUT_BUSINESS_SECTOR_PLACEHOLDER}</Text>}
                    <Autocomplete
                        placeholder={STRINGS.ABOUT_BUSINESS_SECTOR_PLACEHOLDER}
                        placeholderTextColor={COLORS.placeHolder_color}
                        data={sectorResponse}
                        style={{
                            paddingVertical: wp(1),
                            fontFamily: FONT_FAMILY.Roboto,
                            fontSize: FONT.TextMedium_2,
                            color: COLORS.black_color,
                        }}
                        inputContainerStyle={{
                            paddingVertical: wp(2),
                            backgroundColor: COLORS.backGround_color,
                            width: wp(90),
                            borderWidth: 0,
                            borderBottomWidth: wp(0.35),
                            borderBottomColor: 'rgb(163,171,180)',
                        }}
                        listContainerStyle={{
                            width: wp(90),
                        }}
                        listStyle={{
                            height: wp(45),
                            width: wp(90),
                            marginLeft: 0,
                        }}

                        hideResults={isSectorType}
                        defaultValue={sector}
                        onChangeText={text => this.onChange(text, 'sector')}
                        renderItem={({ item, i }) => (
                            <TouchableOpacity onPress={() => this.setState({
                                sector: item.label,
                                isSectorType: true,
                                sectorId: parseInt(item.value),
                            })}>
                                <Text style={{
                                    padding: wp(2), fontFamily: FONT_FAMILY.Roboto,
                                    fontSize: FONT.TextMedium_2,
                                    color: COLORS.black_color,
                                }}>{item.label}</Text>
                                <View style={{ height: wp(0.2), backgroundColor: 'rgb(163,171,180)', width: wp(90) }} />
                                <Spacer space={0.5} />
                            </TouchableOpacity>
                        )}
                    />
                    {sectorError !== '' && <Text style={{
                        paddingTop: wp(1),
                        color: COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}>{sectorError}</Text>}
                </View>
            </>

        );
    };


    //===========================================================
    //===========================country code====================
    //===========================================================

    onCountrySelect = (value) => {
        this.setState({
            cca2: value.cca2,
            callingCode: `+${value.callingCode[0]}`,
        });
    };

    DropDownView = () => {
        const { isDropvisible } = this.state;
        return (
            <TouchableOpacity onPress={() => this.setState({ isDropvisible: !isDropvisible })}>
                <EntypoIcon name={'chevron-down'} size={wp(5)} />
            </TouchableOpacity>
        );
    };

    _renderCountryPicker = () => {
        return (
            <CountryPicker
                renderFlagButton={this.DropDownView}
                countryList={this.state.countryData}
                onSelect={(value) => {
                    this.onCountrySelect(value);
                }}
                hideAlphabetFilter={true}
                showCallingCode={true}
                filterPlaceholder={STRINGS.SEARCH_TEXT}
                closeable={true}
                filterable={true}
                cca2={this.state.cca2}
                translation="eng"
                autoFocusFilter={false}
                countryCode={this.state.cca2}
                withFilter={true}
                visible={this.state.isDropvisible}
                onClose={() => this.setState({ isDropvisible: false })}
            />
        );
    };


    moreDetails() {
        const { data} = this.state;
        const {navigate} = this.props.navigation;
        navigate('businessAddMoreDetails', {data});
    }

    _renderBusinessPhone = () => {
        const { phoneNoText, callingCode, data, user_metaData } = this.state;
        return (
            <View>
                <Text style={{
                    paddingVertical: wp(1),
                    fontFamily: FONT_FAMILY.RobotoBold,
                    fontSize: FONT.textSmall1,
                    color: COLORS.black_color,
                }}>Phone No.</Text>


                <View style={{
                    flexDirection: 'row',
                }}>

                    <Input
                        inputContainerStyle={{}}
                        editable={false}
                        pointerEvents={'none'}
                        placeholder={`+${91}`}
                        // leftIcon={this._renderCountryPicker}
                        leftIconContainerStyle={{ marginLeft: 0, alignItems: 'center' }}
                        value={callingCode}
                        containerStyle={{
                            width: wp('35%'),
                        }}
                        rightIcon={
                            this._renderCountryPicker
                        }
                        inputStyle={{
                            fontSize: FONT.TextMedium_2,
                            marginHorizontal: wp(1),
                            // marginTop:wp(2)
                        }}
                    />
                    <Spacer row={3} />
                    <Input
                        inputContainerStyle={{
                            width: wp('54%'),
                        }}
                        placeholder={STRINGS.MOBILE_NUMBER}
                        placeholderTextColor={COLORS.off_black}
                        ref="Mobile"
                        value={phoneNoText}
                        onChangeText={(text) =>
                            this.setState({
                                phoneNoText: text.trim() && text.replace(/^0+/, ''),
                            })
                        }
                        maxLength={10}
                        keyboardType="phone-pad"
                        returnKeyType={'next'}
                        inputStyle={{
                            paddingLeft: wp(0.5),
                            fontSize: FONT.TextMedium_2,
                        }}
                        containerStyle={{
                            paddingBottom: 0,
                            alignItems: 'center',
                            width: wp('50%'),
                        }}
                        rightIcon={<EntypoIcon size={wp(6)} />}
                    />
                </View>
            </View>
        );
    };

    _renderCountryAndCity = () => {
        const {
            streetAddress, validStreetAddress, labelStreetAddress, countryName, cityName, countryError,
            isCountryName, isCityName, labelCountry, user_metaData, labelCity, countryResponse, cityResponse, cityError,
        } = this.state;
        return (
            <>
                <View style={{ alignItems: 'center' }}>
                    <Input
                        inputContainerStyle={{ width: wp(90) }}
                        placeholder={STRINGS.ABOUT_BUSINESS_STREET_ADDRESS_PLACEHOLDER}
                        label={labelStreetAddress}
                        labelStyle={{
                            fontFamily: FONT_FAMILY.Roboto,
                            fontSize: FONT.TextSmall,
                            color: COLORS.black_color,
                        }}
                        errorStyle={{
                            color: COLORS.invalid_color,
                            fontFamily: FONT_FAMILY.Roboto,
                            fontSize: FONT.TextSmall_2,
                        }}
                        errorMessage={validStreetAddress}
                        inputStyle={{
                            fontFamily: FONT_FAMILY.Roboto,
                            fontSize: FONT.TextMedium_2,
                            color: COLORS.black_color,
                        }}
                        value={streetAddress}
                        onChangeText={(text) => {
                            this.onChange(text, 'street');
                        }}
                    />
                </View>
                <View style={{
                    flex: 1,
                    // position:'absolute',
                    zIndex: 10,
                    width: wp(90),
                }}>
                    {labelCountry.length !== 0 && <Text style={{
                        fontFamily: FONT_FAMILY.RobotoBold,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}>{STRINGS.ABOUT_BUSINESS_COUNTRY_PLACEHOLDER}</Text>}
                    <Autocomplete
                        placeholder={STRINGS.ABOUT_BUSINESS_COUNTRY_PLACEHOLDER}
                        placeholderTextColor={COLORS.placeHolder_color}
                        data={countryResponse}
                        style={{
                            paddingVertical: wp(1),
                            fontFamily: FONT_FAMILY.Roboto,
                            fontSize: FONT.TextMedium_2,
                            color: COLORS.black_color,
                        }}
                        inputContainerStyle={{
                            paddingVertical: wp(2),
                            backgroundColor: COLORS.backGround_color,
                            width: wp(90),
                            borderWidth: 0,
                            borderBottomWidth: wp(0.35),
                            borderBottomColor: 'rgb(163,171,180)',
                        }}
                        listContainerStyle={{
                            width: wp(90),
                        }}
                        listStyle={{
                            height: wp(45),
                            width: wp(90),
                            marginLeft: 0,
                        }}

                        hideResults={isCountryName}
                        defaultValue={countryName}
                        //   defaultValue={user_metaData.country}
                        onChangeText={(text) => this.onChange(text, 'country')}
                        renderItem={({ item, i }) => (
                            <TouchableOpacity
                                onPress={() => this.setState({
                                    countryName: item.label,
                                    isCountryName: true,
                                    countryId: parseInt(item.value),
                                })}>
                                <Text style={{
                                    padding: wp(2), fontFamily: FONT_FAMILY.Roboto,
                                    fontSize: FONT.TextMedium_2,
                                    color: COLORS.black_color,
                                }}>{item.label}</Text>
                                <View style={{ height: wp(0.2), backgroundColor: 'rgb(163,171,180)', width: wp(90) }} />
                                <Spacer space={0.5} />
                            </TouchableOpacity>
                        )}
                    />
                    {countryError !== '' && <Text style={{
                        paddingTop: wp(1),
                        color: COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}>{countryError}</Text>}
                </View>
                <Spacer space={2} />
                <View style={{
                    flex: 1,
                    zIndex: 8,
                    width: wp(90),
                }}>
                    {labelCity.length !== 0 && <Text style={{
                        fontFamily: FONT_FAMILY.RobotoBold,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}>{STRINGS.ABOUT_BUSINESS_CITY_PLACEHOLDER}</Text>}
                    <Autocomplete
                        placeholder={STRINGS.ABOUT_BUSINESS_CITY_PLACEHOLDER}
                        placeholderTextColor={COLORS.placeHolder_color}
                        data={cityResponse}
                        style={{
                            paddingVertical: wp(1),
                            fontFamily: FONT_FAMILY.Roboto,
                            fontSize: FONT.TextMedium_2,
                            color: COLORS.black_color,
                        }}
                        inputContainerStyle={{
                            paddingVertical: wp(2),
                            backgroundColor: COLORS.backGround_color,
                            width: wp(90),
                            borderWidth: 0,
                            borderBottomWidth: wp(0.35),
                            borderBottomColor: 'rgb(163,171,180)',
                        }}
                        listContainerStyle={{
                            width: wp(90),
                        }}
                        listStyle={{
                            height: wp(45),
                            width: wp(90),
                            marginLeft: 0,
                        }}

                        hideResults={isCityName}
                        defaultValue={cityName}
                        //defaultValue={user_metaData.city}
                        onChangeText={text => this.onChange(text, 'city')}
                        renderItem={({ item, i }) => (
                            <TouchableOpacity onPress={() => this.setState({
                                cityName: item.label,
                                isCityName: true,
                                cityId: parseInt(item.value),
                            })}>
                                <Text style={{
                                    padding: wp(2), fontFamily: FONT_FAMILY.Roboto,
                                    fontSize: FONT.TextMedium_2,
                                    color: COLORS.black_color,
                                }}>{item.label}</Text>
                                <View style={{ height: wp(0.2), backgroundColor: 'rgb(163,171,180)', width: wp(90) }} />
                                <Spacer space={0.5} />
                            </TouchableOpacity>
                        )}
                    />
                    {cityError !== '' && <Text style={{
                        paddingTop: wp(1),
                        color: COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}>{cityError}</Text>}
                </View>
            </>
        );
    };


    _renderBusinessAddress = () => {
        const { validZipCode, labelZipCode, user_metaData, zipCode, validWebsite, labelWebsite, website } = this.state;
        return (
            <View style={{ alignItems: 'center' }}>
                <Input
                    inputContainerStyle={{ width: wp(90) }}
                    placeholder={STRINGS.ABOUT_BUSINESS_ZIP_PLACEHOLDER}
                    label={labelZipCode}
                    labelStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}
                    errorStyle={{
                        color: COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}
                    errorMessage={validZipCode}
                    inputStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                    }}
                    value={zipCode}
                    maxLength={6}
                    keyboardType="phone-pad"
                    onChangeText={(text) => {
                        this.onChange(text, 'zip');
                    }}
                />
                <Input
                    inputContainerStyle={{ width: wp(90) }}
                    placeholder={`${STRINGS.ABOUT_BUSINESS_WEBSITE_PLACEHOLDER} (Optional)`}
                    label={labelWebsite}
                    labelStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}
                    errorStyle={{
                        color: COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}
                    errorMessage={validWebsite}
                    inputStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                    }}
                    value={website}
                    onChangeText={(text) => {
                        this.onChange(text, 'website');
                    }}
                />




            </View>
        );
    };
    _renderCheckBox = () => {
        const { isChecked } = this.state;
        const { navigation } = this.props;
        const { navigate } = navigation;
        return (
            <TouchableOpacity
                style={{
                    width: wp(90),
                    flexDirection: 'row',
                }}
                onPress={() => this.onChange('', 'terms')}>
                {isChecked ? <CheckSVG /> : <UnCheckSVG />}
                <Spacer row={2} />
                <Text style={{
                    fontSize: FONT.TextSmall,
                    color: COLORS.black_color,
                    fontFamily: FONT_FAMILY.Roboto,
                }}>I accept the
                    <Text style={{ color: COLORS.app_theme_color }} onPress={() => navigate('termsScreen')}> terms and
                        conditions</Text></Text>
            </TouchableOpacity>
        );
    };

    onSelectValue = (item) => {
        console.warn('item', item);
    };

    onChange = async (text, type) => {
        const { isChecked, countryName } = this.state;
        let arr = [1];
        await arr.map(item => {
            switch (type) {
                case 'name':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelName: '',
                        });
                    } else if (text.trim().length < 3) {
                        this.setState({
                            validName: STRINGS.name_minimum_three,
                            labelName: 'Name',
                        });
                    } else {
                        this.setState({
                            validName: '',
                        });
                    }
                    this.setState({ userName: text });
                    break;
                case 'email':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelEmail: '',
                        });
                    } else if (!validateEmail(text)) {
                        this.setState({
                            validEmail: STRINGS.valid_email,
                            labelEmail: 'Email ID',
                        });
                    } else {
                        this.setState({
                            validEmail: '',
                        });
                    }
                    this.setState({ userEmail: text });
                    break;
                case 'firstName':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelfirstName: '',
                        });
                    } else if (text.trim().length < 3) {
                        this.setState({
                            validfirstName: STRINGS.name_minimum_three,
                            labelfirstName: STRINGS.ABOUT_BUSINESS_LEGAL_FIRST_PLACEHOLDER,
                        });
                    } else {
                        this.setState({
                            validfirstName: '',
                        });
                    }
                    this.setState({ lFirstName: text });
                    break;
                case 'lastname':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelLastName: '',
                        });
                    } else if (text.trim().length < 3) {
                        this.setState({
                            validLastName: STRINGS.name_minimum_three,
                            labelLastName: STRINGS.ABOUT_BUSINESS_LEGAL_LAST_PLACEHOLDER,
                        });
                    } else {
                        this.setState({
                            validLastName: '',
                        });
                    }
                    this.setState({ lLastName: text });
                    break;
                case 'position':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelPositionName: '',
                        });
                    } else if (text.trim().length < 3) {
                        this.setState({
                            validPositionName: 'Position can not be less then 3 letters.',
                            labelPositionName: 'Position',
                        });
                    } else {
                        this.setState({
                            validPositionName: '',
                        });
                    }
                    this.setState({ postionName: text });
                    break;
                case 'street':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelStreetAddress: '',
                            validStreetAddress: 'Street Address can not be empty.',
                        });
                    } else if (text.trim().length < 3) {
                        this.setState({
                            labelStreetAddress: STRINGS.ABOUT_BUSINESS_STREET_ADDRESS_PLACEHOLDER,
                        });
                    } else {
                        this.setState({
                            validStreetAddress: '',
                        });
                    }
                    this.setState({ streetAddress: text });
                    break;
                case 'zip':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelZipCode: '',
                            validZipCode: 'ZipCode can not be empty.',
                        });
                    } else if (text.trim().length < 6) {
                        this.setState({
                            labelZipCode: STRINGS.ABOUT_BUSINESS_ZIP_PLACEHOLDER,
                            validZipCode: '',
                        });
                    } else {
                        this.setState({
                            validZipCode: '',
                        });
                    }
                    this.setState({ zipCode: text.trim() });
                    break;
                case 'website':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelWebsite: '',
                            // validWebsite: 'Website can not be empty.',
                        });
                    } else if (!validateWebsite(text)) {
                        this.setState({
                            labelWebsite: `${STRINGS.ABOUT_BUSINESS_WEBSITE_PLACEHOLDER} (Optional)`,
                            validWebsite: 'Enter valid website.',
                        });
                    } else {
                        this.setState({
                            validWebsite: '',
                        });
                    }
                    this.setState({ website: text.trim() });
                    break;
                case 'org':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelOrganization: '',
                            orgError: '',
                        });
                    } else {
                        this.setState({ labelOrganization: STRINGS.BS_ORGANISATION_NAME_PLACEHOLDER });
                    }
                    if (text.trim().length > 0) {
                        OrganizationTypeAction({
                            search_text: text,
                        }, data => this.organizatonTypeResponse(data, 'org'));
                    }
                    this.setState({
                        isOrganizationType: text.trim().length < 1,
                    });
                    break;
                case 'sector':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelSector: '',
                            sectorError: '',
                        });
                    } else {
                        this.setState({ labelSector: STRINGS.BS_ORGANISATION_NAME_PLACEHOLDER });
                    }
                    if (text.trim().length > 0) {
                        SectorTypeAction({
                            search_text: text,
                        }, data => this.organizatonTypeResponse(data, 'sector'));
                    }
                    this.setState({
                        isSectorType: text.trim().length < 1,
                    });
                    break;
                case 'country':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelCountry: '',
                            countryError: '',
                        });
                    } else {
                        this.setState({ labelCountry: STRINGS.BS_ORGANISATION_NAME_PLACEHOLDER });
                    }
                    if (text.trim().length > 0) {
                        CountryAction({
                            search_text: text,
                        }, data => this.organizatonTypeResponse(data, 'country'));
                    }
                    this.setState({
                        isCountryName: text.trim().length < 1,
                    });
                    break;
                case 'city':
                    if (countryName.length !== 0) {
                        if (text.trim().length === 0) {
                            this.setState({
                                labelCity: '',
                                cityError: '',
                            });
                        } else {
                            this.setState({ labelCity: STRINGS.BS_ORGANISATION_NAME_PLACEHOLDER });
                        }
                        if (text.trim().length > 0) {
                            CityAction({
                                country_name: countryName,
                                search_text: text,
                            }, data => this.organizatonTypeResponse(data, 'city'));
                        }
                        this.setState({
                            isCityName: text.trim().length < 1,
                        });
                    } else {
                        // this.showToastAlert('Please choose county first.');
                        this.setState({
                            cityError: 'Please choose county first',
                        });
                    }
                    break;
                case 'terms':
                    this.setState({ isChecked: !isChecked });
                    break;
                default:
                    break;

            }
        });
        // this.checkAllValues();
    };


    organizatonTypeResponse = (response, type) => {
        if (response !== 'Network request failed') {
            this.hideDialog();
            const { code, message, data, technical_message } = response;
            const { navigation } = this.props
            if (code === '200') {
                this.hideDialog();
                switch (type) {
                    case 'org':
                        this.setState({
                            organizationResponse: TransformDropdownData(data),
                            orgError: '',
                        });
                        break;
                    case 'sector':
                        this.setState({
                            sectorResponse: TransformDropdownData(data),
                            sectorError: '',
                        });
                        break;
                    case 'country':
                        this.setState({
                            countryResponse: TransformCountryData(data),
                            countryError: '',
                        });
                        break;
                    case 'city':
                        this.setState({
                            cityResponse: TransformCityData(data),
                            cityError: '',
                        });
                        break;
                }

            } else if (code === '201') {
                this.hideDialog();
                // this.showToastAlert(message);
                switch (type) {
                    case 'org':
                        this.setState({
                            orgError: message,
                            organizationResponse: [],
                        });
                        break;
                    case 'sector':
                        this.setState({
                            sectorError: message,
                            sectorResponse: [],
                        });
                        break;
                    case 'country':
                        this.setState({
                            countryError: message,
                            countryResponse: [],
                        });
                        break;
                    case 'city':
                        this.setState({
                            cityError: message,
                            cityResponse: [],
                        });
                        break;
                }
            } else if (code === '422') {
                this.hideDialog();
                this.showToastAlert(technical_message.social_id[0]);
            }
            else if (code === '401') {
                this.hideDialog();
                AsyncStorage.removeItem(STRINGS.accessToken);
                AsyncStorage.removeItem(STRINGS.loginData);
                AsyncStorage.removeItem(STRINGS.userPersonalInfo);
                navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [
                            { name: 'login' },
                        ],
                    }),
                );
                this.showToastAlert(message);
            }
        } else {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET);
        }
    };

    checkAllValues = () => {
        const {
            validName, userName, validEmail, userEmail,
            validfirstName, lFirstName, validLastName, lLastName, validPositionName, postionName, organisationType, sector, phoneNoText,
            streetAddress, validStreetAddress, countryName, cityName, zipCode, validZipCode, website, validWebsite, isChecked, socialId,
        } = this.state;
        if (validName !== '' || userName.length === 0) {
            this.setState({ areAllValuesValid: false });
        } else if (validEmail !== '' || userEmail.length === 0) {
            this.setState({ areAllValuesValid: false });
        } else if (validfirstName !== '' || lFirstName.length === 0) {
            this.setState({ areAllValuesValid: false });
        } else if (validLastName !== '' || lLastName.length === 0) {
            this.setState({ areAllValuesValid: false });
        } else if (validPositionName !== '' || postionName.length === 0) {
            this.setState({ areAllValuesValid: false });
        } else if (organisationType.length === 0) {
            this.setState({ areAllValuesValid: false });
        } else if (sector.length === 0) {
            this.setState({ areAllValuesValid: false });
        } else if (countryName.length === 0) {
            this.setState({ areAllValuesValid: false });
        } else if (cityName.length === 0) {
            this.setState({ areAllValuesValid: false });
        } else if (phoneNoText.length === 0 || phoneNoText.length < 10) {
            this.setState({ areAllValuesValid: false });
        } else if (validStreetAddress !== '' || streetAddress.length === 0) {
            this.setState({ areAllValuesValid: false });
        } else if (validZipCode !== '' || zipCode.length === 0) {
            this.setState({ areAllValuesValid: false });
            // } else if (validWebsite !== '' || website.length === 0) {
            //     this.setState({areAllValuesValid: false});
        } else if (!isChecked) {
            this.setState({ areAllValuesValid: false });
        } else {
            this.setState({ areAllValuesValid: true });
        }
    };

    /*_onNextPress() {
        const {navigation} = this.props;
        const {navigate} = navigation;
        const {
            lFirstName, lLastName, postionName, orgId, sectorId, phoneNoText, streetAddress,
            countryId, cityId, zipCode, website, isChecked, socialId, callingCode, socialPlatform,
            userName, orgName, password, orgEmail, id,countryName
        } = this.state;
        BusinessRegisterAction({
            'firstName': lFirstName,
            'lastName': s,
            'position': postionName,
            'organizationType': orgId,
            'sector': sectorId,
            'phone': phoneNoText,
            'street': streetAddress,
            'country':countryName,
            /!* 'country': countryId,*!/
            'city': cityId,
            'zipCode': zipCode,
            'website': website,
            'socialId': socialId,
            'socialProfile': socialPlatform,
            'email': orgEmail,
            'userName': socialId !== '' ? socialId : userName,
            'password': socialId !== '' ? 'Mind@123' : password,
            'countryCode': callingCode,
            'orgName': orgName,
            'id': id,
        }, data => this.registerApiResponse(data));
        this.showDialog();
    }*/

    onPressBack = () => {
        const { navigation } = this.props;
        navigation.goBack();
    };

    _renderCustomLoader = () => {
        const { isLoading } = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={STRINGS.LOADING_TEXT} />
        );
    };


    // ----------------------------------------
    // ----------------------------------------
    // Gallery
    // ----------------------------------------

    chooseFromGallery = (showAvatar) => {
        ImagePicker.openPicker({
            compressImageQuality: 0.2,
            includeBase64: true,
            cropping: true,
            mediaType: 'photo',
        }).then(image => {
            this.setState({
                image: image.path,
                profileBase64: `data:${image.mime};base64,${image.data}`,
            });
        }).catch(e => {
            console.warn(e);
        });
    };

    // ----------------------------------------
    // ----------------------------------------
    // camera
    // ----------------------------------------
    chooseFromCamera = (showAvatar) => {
        ImagePicker.openCamera({
            compressImageQuality: 0.2,
            includeBase64: true,
            cropping: true,
            mediaType: 'photo',
        }).then(image => {
            this.setState({
                image: image.path,
                profileBase64: `data:${image.mime};base64,${image.data}`,
            });
        }).catch(e => console.log(e));
    };

    updateUserDetail = () => {
        const {
            image, gender, privacy, parentemail, childFirstName, childLastName, datePicker, profileBase64, StaticArray, addmoreDetails,

            authToken, userName, lLastName, lFirstName, postionName, sectorId, cityId, zipCode, countryId, phoneNoText,
            orgId, userEmail, streetAddress,
        } = this.state;

        // if (image === 'https://www.cornwallbusinessawards.co.uk/wp-content/uploads/2017/11/dummy450x450-300x300.jpg') {
        //     this.showToastAlert('Profile Image can not be empty');
        // } else {
        UpdateProfileBusinessAction(
            {
                organisation_name: userName,
                legalfirstname: lFirstName,
                legallastname: lLastName,
                position: postionName,
                sector: sectorId,
                street_address: streetAddress,
                city: cityId,
                apt_street: '',
                zipcode: zipCode,
                state: '',
                country_code: countryId,
                phone_no: phoneNoText,
                organisation_type: orgId,
                subscription_plan: 2,
                email: userEmail,
                profile_image: image.startsWith('http://') ? '' : profileBase64,
                authToken: authToken,

            },
            data => this.updateProfileResponse(data));
        this.showDialog();
        // }
    };


    updateProfileResponse = (response) => {
        const { navigation } = this.props;
        const { navigate } = navigation;
        const { userName } = this.state;
        console.warn('CHKK RES-->', response);
        if (response !== 'Network request failed') {
            this.hideDialog();
            const { code, message, data } = response;
            if (code === '200') {
                this.hideDialog();
                this.showToastSucess(message);
                navigation.goBack();
            } else if (code === '403') {
                this.hideDialog();
                this.showToastAlert(message);
            } else if (code === '422') {
                this.hideDialog();
            } else if (code === '401') {
                this.hideDialog();
                AsyncStorage.removeItem(STRINGS.accessToken);
                AsyncStorage.removeItem(STRINGS.loginData);
                AsyncStorage.removeItem(STRINGS.userPersonalInfo);
                navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [
                            { name: 'login' },
                        ],
                    }),
                );
                this.showToastAlert(message);
            }
            else {
                this.showToastAlert('Profile Image can not be empty');
            }
        } else {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET);
        }
    };


    // ----------------------------------------
    // ----------------------------------------
    // This method is use to open camera and gallery
    // ----------------------------------------

    onEditPress = (showAvatar) => {
        Alert.alert(
            STRINGS.APP_NAME,
            STRINGS.SELECT_IMAGE,
            [
                {
                    text: STRINGS.CANCEL_TEXT, style: 'cancel',
                },
                {
                    text: STRINGS.GALLERY_TEXT, onPress: () => {
                        this.chooseFromGallery(showAvatar);
                    },
                },
                {
                    text: STRINGS.CAMERA_TEXT, onPress: () => {
                        this.chooseFromCamera(showAvatar);
                    },
                },
            ],
            { cancelable: false });
    };

    render() {

        const { areAllValuesValid, image } = this.state;
        const { navigation } = this.props;
        const { navigate } = navigation;
        return (
            <SafeAreaViewContainer>
                <TopHeader
                    rightComponentNotVisible={true}
                    text={'Edit Profile'}
                    onPressBackArrow={() => navigation.goBack()} />
                <KeyboardAvoidingView
                    style={{ flex: 1, backgroundColor: COLORS.backGround_color }}
                    behavior={(Platform.OS === 'ios') ? 'padding' : null}
                >
                    <TouchableWithoutFeedback
                        onPress={() => {
                            Keyboard.dismiss();
                        }}>
                        <ScrollContainer>
                            <MainContainer>
                                <Spacer space={2} />
                                <View style={{ flexDirection: 'column' }}>
                                    <Image style={{ height: wp(30), width: wp(30), borderRadius: wp(30) / 2 }}
                                        source={{ uri: image }} resizeMode={'cover'} />

                                    <EditIcon style={{ position: 'absolute', right: wp(1), bottom: -wp(3.5) }}
                                        size={40}
                                        color={COLORS.app_theme_color}
                                        // onPress={() => navigate('ChooseAvatar')}
                                        onPress={() => this.onEditPress()}
                                        name={'pencil-circle'} />
                                    {/* <Image
                                        source={ICONS.NEXT_PAGE_IMAGE} resizeMode={'contain'}/>*/}


                                </View>
                                <Spacer space={5} />
                                {this._renderBusinessContact()}
                                <StartText>{STRINGS.ABOUT_BUSINESS_DETAIL_TEXT}</StartText>
                                <Spacer space={2} />
                                {this._renderBusinessDetail(false)}
                                <Spacer space={2} />
                                {this._renderBusinessPhone()}
                                <StartText>{STRINGS.ABOUT_BUSINESS_ADDRESS_TEXT}</StartText>
                                <Spacer space={2} />
                                {this._renderCountryAndCity()}
                                <Spacer space={2} />
                                {this._renderBusinessAddress()}
                                <Spacer space={1} />
                                <TouchableOpacity onPress={() => this.moreDetails()}>
                                    <View style={{ width: wp("90%"), flexDirection: 'row', justifyContent: 'space-between' }}>

                                        <Text style={{
                                            alignSelf: 'center',
                                            fontFamily: FONT_FAMILY.PoppinsBold,
                                            fontSize: FONT.TextSmall,
                                            color: COLORS.off_black,
                                        }}>
                                            {STRINGS.ADD_MORE_DETAILS}
                                        </Text>
                                        <Image style={{}}
                                            source={ICONS.NEXT_PAGE_IMAGE} resizeMode={'contain'} />

                                    </View>
                                </TouchableOpacity>
                                <Spacer space={3} />
                                <PrimaryButton
                                    onPress={() => this.updateUserDetail()}
                                    text={'Save'}
                                    isNextIcon={false}
                                    textWidth={wp(85)}
                                    // isDisabled={!areAllValuesValid}
                                    bgColor={COLORS.app_theme_color}
                                />

                                <Spacer space={3} />
                            </MainContainer>
                            {this._renderCustomLoader()}
                        </ScrollContainer>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </SafeAreaViewContainer>
        );
    }
}


export default BusinessEditProfile;
