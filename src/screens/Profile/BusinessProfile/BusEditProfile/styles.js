import styled from "styled-components/native/dist/styled-components.native.esm";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import COLORS from "../../../../utils/Colors";
import { FONT } from "../../../../utils/FontSizes";
import { FONT_FAMILY } from "../../../../utils/Font";
import {StyleSheet} from 'react-native'


const CreateBusinessText = styled.Text`
color: ${COLORS.app_theme_color};
fontSize: ${FONT.TextNormal};
fontFamily: ${FONT_FAMILY.PoppinsBold}; 
width: ${wp("80%")};
lineHeight:${wp(10)};
alignItems: ${'center'};
textAlign: ${'center'};
fontStyle: ${'normal'};
`;

const StartText = styled.Text`
color: ${COLORS.black_color};
fontSize: ${FONT.TextMedium_2};
fontFamily: ${FONT_FAMILY.PoppinsBold}; 
alignItems: flex-start;
width: ${wp("90%")};
textAlign: left
fontStyle: normal
fontWeight: bold
`;

const terms_conditions= styled.Text `
 color: ${COLORS.app_theme_color}
 `


const styles = StyleSheet.create({
    autocompleteContainer: {
        // backgroundColor: COLORS.backGround_color,
        width:wp('90%')
    },
});
export {
    CreateBusinessText,
    StartText,
    styles,
    terms_conditions
}
