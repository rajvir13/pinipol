import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {StyleSheet} from 'react-native';

import {FONT_FAMILY} from '../../../../../utils/Font';
import {FONT} from '../../../../../utils/FontSizes';

const style = StyleSheet.create({
    childContainer: {
        height: wp(13),
        alignItems: 'center',
        paddingHorizontal: wp(1),
        marginTop:wp(2),
    },

    tittleText: {
        fontFamily: FONT_FAMILY.Roboto,
        color: 'white',
        fontSize: wp(3.3),
    },

    detailsText: {
        width: wp(70),
        color: 'white',
        fontFamily: FONT_FAMILY.Roboto,
        fontSize: wp(3.5),
    },
    underline: {
        width: wp(100), height: wp(.3), backgroundColor: '#707070',
    },
});

export default style;
