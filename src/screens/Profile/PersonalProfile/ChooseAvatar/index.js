import React from 'react';
import {
    Image,
    KeyboardAvoidingView,
    Keyboard,
    Text,
    TouchableWithoutFeedback,
    View, TouchableOpacity,
    FlatList,
} from 'react-native';
import Menu, {MenuItem} from 'react-native-material-menu';


import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {Input} from 'react-native-elements';
import BaseClass from '../../../../utils/BaseClass';
import {MainContainer, SafeAreaViewContainer, ScrollContainer} from '../../../../utils/BaseStyle';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {ICONS} from '../../../../utils/ImagePaths';
import STRINGS from '../../../../utils/Strings';
import {Spacer} from "../../../../customComponents/Spacer";
import {validateEmail, validatePassword} from '../../../../utils/Validations';
import moment from 'moment';
import CheckSVG from '../../../../customComponents/imagesComponents/Check';
import UnCheckSVG from '../../../../customComponents/imagesComponents/Uncheck';
import {PrimaryButton} from '../../../../customComponents/ButtonComponent';
import TopHeader from '../../../../customComponents/Header';
import OrientationLoadingOverlay from '../../../../customComponents/CustomLoader';
import {FONT_FAMILY} from "../../../../utils/Font";
import {FONT} from '../../../../utils/FontSizes';
import SelectIcon from '../../../Profile/PersonalProfile/component/SelectIcon';
import COLORS from "../../../../utils/Colors";
// import {ChooseAvatarAction} from "../../../../redux/actions/ChooseAvatarAction";
import FastImage from 'react-native-fast-image'


class ChooseAvatar extends BaseClass {
    constructor(props) {
                super(props);
        this.state = {
            isLoading: false,
            data: [],
            selectedIcon:''

        };
    }


    componentDidMount() {
        const {navigation} = this.props;
        // this._unsubscribe = navigation.addListener('focus', () => {
        //     this.onFocusFunction();
        // });


    }


    // onFocusFunction = () => {
    //     //    this.showDialog();
    //     ChooseAvatarAction({}, data => this.isChooseAvatarResponse(data));
    //       this.componentDidMount();
    // };



    // componentWillUnmount() {
    //     this._unsubscribe();
    // }

    isChooseAvatarResponse = (response) => {
        if (response !== 'Network request failed') {
               console.warn("tttt", response)
            this.hideDialog();
            const {code, message, data} = response;
            if (code === '200') {
                this.hideDialog();
                this.setState(
                    {
                        data: data,
                    }
                )
            } else if (code === '403') {
                this.hideDialog();
            }
        } else {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET);
        }
    };


// =============================================================================================
    // Render method for Custom Loader
    // =============================================================================================

    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={STRINGS.LOADING_TEXT}/>
        );
    };

    iconSelectClick = (item) => {
        this.setState({
            selectedIcon:item
        })
        //alert("Coming Soon")
    };


    onEmailEditingEnd = () => {
        const {navigation} = this.props;
        const {navigate} = navigation;
        const {selectedIcon}=this.state
        navigate('personalEditProfile', {selectedIcon: selectedIcon});
    };




    render() {
        const {navigation} = this.props;
        const {data} = this.state;
        return (
            <SafeAreaViewContainer>
                <TopHeader
                    rightComponentNotVisible={true}
                    text={"Choose Avatar"}
                    onPressBackArrow={() => navigation.goBack()}/>
                <View style={{flex:1,alignItems:'center',backgroundColor: COLORS.backGround_color}}>
                    <FlatList
                        numColumns={3}
                        data={data}
                        style={{width: wp(100), paddingTop: wp(3), }}
                        renderItem={({item, index}) =>


                           /* <FastImage
                                style={{width: wp(28), height: wp(28) }}
                                source={{
                                    uri: item.avatar_url,
                                }}
                                resizeMode={FastImage.resizeMode.contain}
                                onPress={() => alert("chkkkk")}
                            />*/


                           /* <Image

                                resizeMode='contain'
                                source={{uri: item.avatar_url}}
                                style={{width: wp(28), height: wp(28),}}
                                onPress={() => alert("chkkkk")}
                            />*/

                            <SelectIcon
                                data={item}
                                index={index}
                                 onPress={() => this.iconSelectClick(item)}
                              //  onPress={() => alert("Coming Soon")}
                            />
                        }
                    keyExtractor={(item, index) => index.toString()}
                    />

                    <PrimaryButton
                        onPress={() => this.onEmailEditingEnd()}
                        //isDisabled={!areAllValuesValid}
                        //bgColor={areAllValuesValid ? COLORS.app_theme_color : COLORS.app_theme_color_disabled}
                        bgColor={COLORS.app_theme_color}
                        text={STRINGS.SAVE_TEXT}/>
                </View>

                <Spacer space={2}/>

            </SafeAreaViewContainer>
        );
    }
}


export default ChooseAvatar;
