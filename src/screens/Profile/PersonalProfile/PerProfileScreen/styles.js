import styled from 'styled-components';
import { widthPercentageToDP as wp} from "react-native-responsive-screen";

import React from "react";

const InputTextsContainer = styled.View`
width: ${wp("90%")};
margin-vertical: ${wp("5%")};
justifyContent: space-evenly;
`;


export {

    InputTextsContainer,

}
