import React from 'react';

import {Image, View, Text} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {ICONS} from '../../../../utils/ImagePaths';
import BaseClass from '../../../../utils/BaseClass';
import {FONT_FAMILY} from '../../../../utils/Font';
import {FONT} from '../../../../utils/FontSizes';
import COLORS from '../../../../utils/Colors';
import STRINGS from '../../../../utils/Strings';
import AsyncStorage from '@react-native-community/async-storage';
import {MainContainer, SafeAreaViewContainer, ScrollContainer} from '../../../../utils/BaseStyle';
import {Spacer} from '../../../../customComponents/Spacer';
import {LogoutAction} from '../../../../redux/actions/LogoutAction';
import connect from 'react-redux/lib/connect/connect';
import OrientationLoadingOverlay from '../../../../customComponents/CustomLoader';
import {Chip} from 'react-native-paper';
import {PersonalProfileAction} from '../../../../redux/actions/GetUserProfileAction';
import {GetBusinessProfile} from '../../../../redux/actions/GetBusinessProfileAction';
import TopHeader from '../../../../customComponents/Header';
import {CommonActions} from "@react-navigation/native"

/*const StaticArray = ["Sports","Technology",  "Art",]*/


class PersonalProfileScreen extends BaseClass {
    constructor(props) {
        super(props);
        const {route} = this.props;
        this.state = {
            profileImage: 'https://www.cornwallbusinessawards.co.uk/wp-content/uploads/2017/11/dummy450x450-300x300.jpg',
            image: '',
            authToken: undefined,
            data: [],
            userInfo: undefined,
            StaticArray: [],
        };
    }


    componentDidMount() {
        const {navigation} = this.props;
        this._unsubscribe = navigation.addListener('focus', () => {
            this.onFocusFunction();
        });
    }

    onFocusFunction = () => {

        AsyncStorage.getItem(STRINGS.accessToken, async (error, result) => {
            if (result !== undefined && result !== null) {
                let token = JSON.parse(result);
                console.log('result is', result);
                await this.setState({
                    authToken: token,
                });
                AsyncStorage.getItem(STRINGS.loginData, async (error, result) => {
                    if (result !== undefined && result !== null) {
                        let loginData = JSON.parse(result);
                        await this.setState({
                            loginData: loginData,
                            profile: loginData.profile_image !== null ? loginData.profile_image : 'https://www.cornwallbusinessawards.co.uk/wp-content/uploads/2017/11/dummy450x450-300x300.jpg',
                            userName: loginData.username,
                            account_type: loginData.account_type === '1' ? 'Personal Account' : loginData.account_type === '2' ? 'Business Account' : '',
                            account_type_id: loginData.account_type,
                        });
                        this.showDialog();
                        GetBusinessProfile(
                            {
                                accessToken: token,
                                account_type: loginData.account_type,
                            },
                            (data) => this.getProfileResponse(data),
                        );
                    }
                });
            }
        });
    };

    componentWillUnmount() {
        this._unsubscribe();
    }

    getProfileResponse = (response) => {
        const {profileImage} = this.state;
        if (response === 'Network request failed') {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET);
        } else {
            const {message, code, data, user_meta, interest_data} = response;
            console.log('data is----', data);
            const {navigation} = this.props
            if (code === '200') {
                this.hideDialog();
                this.setState({
                    data: data,
                    userInfo: user_meta,
                    profileImage: user_meta !== null && user_meta.profile_image !== null ? user_meta.profile_image : profileImage,
                });

                if (interest_data !== undefined && interest_data.length !== 0) {
                    let arr = interest_data;
                    let array2 = arr.map((item, index) => {
                        return item;
                    });
                    this.setState({StaticArray: array2});
                } else {
                    this.setState({StaticArray: []});
                }

                AsyncStorage.setItem(STRINGS.userPersonalInfo, JSON.stringify({
                    'profile_image': user_meta.profile_image,
                    'firstname': data.firstname,
                    'lastname': data.lastname,
                    'account_type': data.account_type,
                }));

            } else if (code === '111') {
                this.hideDialog();
                console.warn(message);
            } else if (code === '201') {
                this.hideDialog();
                this.showToastAlert('User Unauthorized');
            } else if (code === '401') {
                this.hideDialog();
                AsyncStorage.removeItem(STRINGS.accessToken);
                AsyncStorage.removeItem(STRINGS.loginData);
                AsyncStorage.removeItem(STRINGS.userPersonalInfo);
                navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [
                            {name: 'login'},
                        ],
                    }),
                );
                this.showToastAlert(message);
            }
            else {
                this.showToastAlert('Session expired');
                this.hideDialog();
            }
        }

    };

    componentWillReceiveProps(nextProps) {
        const {route} = nextProps;
        if (route.params !== undefined && route.params.selectedIcon !== undefined) {
            this.setState({
                profileImage: route.params.selectedIcon,
            });
        }

    }

    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message="Loading.."/>
        );
    };

    _openDrawer = () => {
        const {navigation} = this.props;
        navigation.openDrawer();
    };
    editProfile = () => {
        // alert("Coming Soon")
        const {data, userInfo, StaticArray} = this.state;
        const {navigate} = this.props.navigation;
        navigate('personalEditProfile', {
            // alert("Coming Soon")
            Alldata: data,
            userData: userInfo,
            interestData: StaticArray,

        });
    };


    render() {
        const {navigation} = this.props;
        const {image, data, userInfo, profileImage, StaticArray} = this.state;

        return (
            <SafeAreaViewContainer>
                <TopHeader
                    text={'Profile'}
                    //  isEdit={true} openPressIcon={() => this.editProfile()}
                    onPressBackArrow={() => navigation.goBack()}
                    onRightArrow={() => this.editProfile()}
                    isRight={true}

                />
                <ScrollContainer>
                    <MainContainer>
                        <Spacer space={2}/>
                        <Image style={{height: wp(30), width: wp(30), borderRadius: wp(30) / 2}}
                               source={{uri: profileImage}}
                               resizeMode={'cover'}
                        />
                        <Spacer space={5}/>
                        <View style={{
                            width: wp(90),
                            flexDirection: 'column',
                        }}>
                            <Text style={{
                                fontFamily: FONT_FAMILY.PoppinsBold,
                                fontSize: FONT.textSmall1,
                                fontStyle: 'normal',
                                color: COLORS.black_color,
                            }}>{data.firstname + ' ' + data.lastname}</Text>
                            <Spacer space={0.3}/>

                            {data.account_type === '2' ?
                                <Text style={{
                                    fontFamily: FONT_FAMILY.Roboto,
                                    fontSize: FONT.textSmall1,
                                    fontStyle: 'normal',
                                    color: COLORS.placeHolder_color,
                                }}>{'Business Account'}</Text> :
                                <Text style={{
                                    fontFamily: FONT_FAMILY.Roboto,
                                    fontSize: FONT.textSmall1,
                                    fontStyle: 'normal',
                                    color: COLORS.placeHolder_color,
                                }}>{'Individual Account'}</Text>}

                            <Spacer space={1}/>
                            <Text style={{
                                fontFamily: FONT_FAMILY.Roboto,
                                fontSize: FONT.textSmall1,
                                fontStyle: 'normal',
                                color: COLORS.black_color,
                            }}>{data.email}</Text>
                            <Spacer space={1}/>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={{
                                    fontFamily: FONT_FAMILY.Roboto,
                                    fontSize: FONT.textSmall1,
                                    fontStyle: 'normal',
                                    color: COLORS.black_color,
                                }}>{'Voter Status'}</Text>
                                <Text style={{
                                    fontFamily: FONT_FAMILY.Roboto,
                                    fontSize: FONT.textSmall1,
                                    fontStyle: 'normal',
                                    color: COLORS.black_color,
                                }}>{''}</Text>
                            </View>
                        </View>
                        <Spacer space={4}/>
                        <View
                            style={{width: wp(100), height: hp(0.1), backgroundColor: COLORS.placeHolder_color}}/>
                        <Spacer space={4}/>

                        <View style={{
                            flexDirection: 'column',
                            justifyContent: 'flex-start',
                            width: wp(90),
                        }}>
                            <Text style={{
                                fontFamily: FONT_FAMILY.PoppinsBold,
                                fontSize: FONT.TextSmall,
                                fontStyle: 'normal',
                                color: COLORS.black_color,
                            }}>{'Interests'}</Text>
                            {/*   <View style={{*/}
                            {/*    width:wp(20),*/}
                            {/*    height:hp(3),*/}
                            {/*    borderRadius:wp(3),*/}
                            {/*    backgroundColor:COLORS.placeHolder_color,*/}
                            {/*    justifyContent:'center',*/}
                            {/*    alignItems:'center'}}>*/}
                            {/*    <Text>Sports</Text>*/}
                            {/*</View>*/}

                            <Spacer space={2}/>
                            {StaticArray.length !== 0 ?
                                <View style={{
                                    flexWrap: 'wrap',
                                    flexDirection: 'row',
                                    justifyContent: 'flex-start',
                                    alignItems: 'center',
                                    width: wp(85),


                                }}>
                                    {StaticArray.map((item, index) => {
                                            return (
                                                <Chip
                                                    // style={{
                                                    //     margin: wp(1), backgroundColor: COLORS.image_background_color,
                                                    // }}
                                                    style={{
                                                        margin: wp(1),
                                                        backgroundColor: item.isChecked ? COLORS.app_theme_color : COLORS.image_background_color,
                                                        paddingHorizontal: 2,
                                                    }}
                                                >{item.categoryName}</Chip>
                                            );
                                        },
                                    )}
                                </View>
                                :

                                <Text style={{
                                    fontFamily: FONT_FAMILY.Poppins,
                                    fontSize: FONT.TextSmall,
                                    fontStyle: 'normal',
                                    color: COLORS.placeHolder_color,
                                }}>{'No Interests added yet.'}</Text>

                            }

                        </View>
                        <Image style={{height: hp(30), width: wp(60)}}
                               source={{uri: image}} resizeMode={'contain'}/>
                        <Spacer space={4}/>
                    </MainContainer>
                    {this._renderCustomLoader()}
                </ScrollContainer>
            </SafeAreaViewContainer>

        );
    }
}

export default (PersonalProfileScreen);
