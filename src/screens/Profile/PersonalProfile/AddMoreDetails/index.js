import React from 'react';
import {
    Image,
    KeyboardAvoidingView,
    Keyboard,
    Text,
    TouchableWithoutFeedback,
    View, TouchableOpacity,
    FlatList, Platform,
} from 'react-native';
import Menu, { MenuItem } from 'react-native-material-menu';


import DateTimePickerModal from 'react-native-modal-datetime-picker';
import { Input } from 'react-native-elements';
import BaseClass from '../../../../utils/BaseClass';
import { MainContainer, SafeAreaViewContainer, ScrollContainer } from '../../../../utils/BaseStyle';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { ICONS } from '../../../../utils/ImagePaths';
import STRINGS from '../../../../utils/Strings';
import { Spacer } from '../../../../customComponents/Spacer';
import { validateEmail, validatePassword, validateWebsite } from '../../../../utils/Validations';
import moment from 'moment';
import CheckSVG from '../../../../customComponents/imagesComponents/Check';
import UnCheckSVG from '../../../../customComponents/imagesComponents/Uncheck';
import { PrimaryButton } from '../../../../customComponents/ButtonComponent';
import TopHeader from '../../../../customComponents/Header';
import OrientationLoadingOverlay from '../../../../customComponents/CustomLoader';
import { FONT_FAMILY } from '../../../../utils/Font';
import { FONT } from '../../../../utils/FontSizes';
import SelectIcon from '../../../Profile/PersonalProfile/component/SelectIcon';
import COLORS from '../../../../utils/Colors';
import Autocomplete from '../../../../customComponents/ModalDropdown/AutoComplete';

import {
    CityAction,
    CountryAction, OccupationAction,
    OrganizationTypeAction, ReligionAction,
    SectorTypeAction,
} from '../../../../redux/actions/DropDownACtions';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen/index';
import {
    TransformCityData,
    TransformCountryData,
    TransformDropdownData,
    TransformOccupationData, TransformReligionData,
} from '../../../../utils/DataTransformation';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import ModalDropdown from '../../../../customComponents/ModalDropdown';
import { CommonActions } from "@react-navigation/native"

const RELATION_SHIP = ['Single', 'Commited', 'Married'];

class AddMoreDetails extends BaseClass {
    constructor(props) {
        super(props);

        const { countryName, cityName, cityId, relationId, occupationId, religion, phone_no } = this.props.route.params;
        this.state = {

            isLoading: false,

            countryName: countryName === 'undefined' ? '' : countryName,
            isCountryName: true,
            countryId: 0,
            labelCountry: '',
            countryResponse: [],
            countryError: '',

            cityResponse: [],
            cityName: cityName === 'undefined' ? '' : cityName,
            isCityName: true,
            labelCity: '',
            cityId: cityId,
            cityError: '',

            occupationResponse: [],
            occupationName: '',
            isOccupationName: true,
            labelOccupation: '',
            occupationId: occupationId,
            occupationError: '',

            religionResponse: [],
            religionName: '',
            isReligionName: true,
            labelReligion: '',
            religionId: religion,
            religionError: '',

            phoneNoText: phone_no === 'undefined' ? '' : phone_no,
            relationLabel: 'Relationship Status',
            relationValue: 'Single',
            relationId: relationId,
            // relationId: 1,

        };
    }

    componentDidMount(): void {
        const { relationId, countryName, cityId, occupationId, religionId } = this.state;
        if (relationId === 1) {
            this.setState({ relationValue: 'Single' });
        } else if (relationId === 2) {
            this.setState({ relationValue: 'Commited' });
        } else if (relationId === 3) {
            this.setState({ relationValue: 'Married' });
        }

        // if (countryName !== '') {
        //     CountryAction({
        //         search_text: text,
        //     }, data => this.organizatonTypeResponse(data, 'country'));
        // } else if (cityId !== 0) {
        //     CityAction({
        //         country_name: countryName,
        //         search_text: text,
        //     }, data => this.organizatonTypeResponse(data, 'city'));
        // } else if (occupationId !== 0) {
        //     OccupationAction({
        //         search_text: text,
        //     }, data => this.organizatonTypeResponse(data, 'occupation'));
        // } else if (religionId !== 0) {
        //     ReligionAction({
        //         search_text: text,
        //     }, data => this.organizatonTypeResponse(data, 'religion'));
        // }

    }

    // =============================================================================================
    // Render method for Custom Loader
    // =============================================================================================

    _renderCustomLoader = () => {
        const { isLoading } = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={STRINGS.LOADING_TEXT} />
        );
    };
    onEmailEditingEnd = () => {
        const { navigation } = this.props;
        const { religionId, cityId, countryId, countryName, occupationId, relationId, phoneNoText } = this.state;
        const { navigate } = navigation;
        navigate('personalEditProfile', {
            addMoreDetails: {
                cityId: cityId,
                countryName: countryName,
                countryId: countryId,
                religionId: religionId,
                relationId: relationId,
                occupationId: occupationId,
                phoneNoText: phoneNoText,
            },
        });
    };


    checkAllValues = () => {
        const { countryName, cityName, occupationName, religionName, relationValue } = this.state;
        if (countryName.length === 0) {
            this.setState({ areAllValuesValid: false });
        } else if (cityName.length === 0) {
            this.setState({ areAllValuesValid: false });
        } else if (occupationName.length === 0) {
            this.setState({ areAllValuesValid: false });
        } else if (religionName.length === 0) {
            this.setState({ areAllValuesValid: false });
        } else if (relationValue.length === 0) {
            this.setState({ areAllValuesValid: false });
        } else {
            this.setState({ areAllValuesValid: true });
        }
    };


    organizatonTypeResponse = (response, type) => {
        if (response !== 'Network request failed') {
            this.hideDialog();
            const { code, message, data, technical_message } = response;
            const { navigation } = this.props
            if (code === '200') {
                this.hideDialog();
                switch (type) {

                    case 'country':
                        this.setState({
                            countryResponse: TransformCountryData(data),
                            countryError: '',
                        });
                        break;
                    case 'city':
                        this.setState({
                            cityResponse: TransformCityData(data),
                            cityError: '',
                        });
                        break;
                    case 'occupation':
                        this.setState({
                            occupationResponse: TransformOccupationData(data),
                            occupationError: '',
                        });
                        break;
                    case 'religion':
                        this.setState({
                            religionResponse: TransformReligionData(data),
                            religionError: '',
                        });
                        break;
                }

            } else if (code === '201') {
                this.hideDialog();
                // this.showToastAlert(message);
                switch (type) {

                    case 'country':
                        this.setState({
                            countryError: message,
                            countryResponse: [],
                        });
                        break;
                    case 'city':
                        this.setState({
                            cityError: message,
                            cityResponse: [],
                        });
                        break;
                    case 'occupation':
                        this.setState({
                            occupationError: message,
                            occupationResponse: [],
                        });
                        break;
                    case 'religion':
                        this.setState({
                            religionError: message,
                            religionResponse: [],
                        });
                        break;
                }
            } else if (code === '422') {
                this.hideDialog();
                this.showToastAlert(technical_message.social_id[0]);
            }
            else if (code === '401') {
                this.hideDialog();
                AsyncStorage.removeItem(STRINGS.accessToken);
                AsyncStorage.removeItem(STRINGS.loginData);
                AsyncStorage.removeItem(STRINGS.userPersonalInfo);
                navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [
                            { name: 'login' },
                        ],
                    }),
                );
                this.showToastAlert(message);
            }
        } else {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET);
        }
    };


    onChange = async (text, type, isSelect, item) => {
        const { isChecked, countryName } = this.state;
        let arr = [1];
        await arr.map(item1 => {
            switch (type) {
                case 'country':
                    if (isSelect) {
                        debugger                        
                        this.setState({
                            countryName: item.label,
                            isCountryName: true,
                            countryId: parseInt(item.value),
                        });
                        this.refs.textInput.value=item.label
                        console.log("country====",this.refs.textInput.value)
                    } else {
                        if (text.trim().length === 0) {
                            this.setState({
                                labelCountry: '',
                                countryError: '',
                            });
                        } else {
                            this.setState({ labelCountry: STRINGS.BS_ORGANISATION_NAME_PLACEHOLDER });
                        }
                        if (text.trim().length > 0) {
                            CountryAction({
                                search_text: text,
                            }, data => this.organizatonTypeResponse(data, 'country'));
                        }
                        this.setState({
                            isCountryName: text.trim().length < 1,
                        });
                    }
                    break;
                case 'city':
                    if (isSelect) {
                        this.setState({
                            cityName: item.label,
                            isCityName: true,
                            cityId: parseInt(item.value),
                        });
                    } else {
                        if (countryName.length !== 0) {
                            if (text.trim().length === 0) {
                                this.setState({
                                    labelCity: '',
                                    cityError: '',
                                });
                            } else {
                                this.setState({ labelCity: STRINGS.BS_ORGANISATION_NAME_PLACEHOLDER });
                            }
                            if (text.trim().length > 0) {
                                CityAction({
                                    country_name: countryName,
                                    search_text: text,
                                }, data => this.organizatonTypeResponse(data, 'city'));
                            }
                            this.setState({
                                isCityName: text.trim().length < 1,
                            });
                        } else {
                            // this.showToastAlert('Please choose county first.');
                            this.setState({
                                cityError: 'Please choose county first',
                            });
                        }
                    }
                    break;
                case 'occupation':
                    if (isSelect) {
                        this.setState({
                            occupationName: item.label,
                            isOccupationName: true,
                            occupationId: parseInt(item.value),
                        });
                    } else {
                        if (text.trim().length === 0) {
                            this.setState({
                                labelOccupation: '',
                                occupationError: '',
                            });
                        } else {
                            this.setState({ labelOccupation: STRINGS.OCCUPATION_TEXT });
                        }
                        if (text.trim().length > 0) {
                            OccupationAction({
                                search_text: text,
                            }, data => this.organizatonTypeResponse(data, 'occupation'));
                        }
                        this.setState({
                            isOccupationName: text.trim().length < 1,
                        });
                    }
                    break;
                case 'religion':
                    if (isSelect) {
                        this.setState({
                            religionName: item.label,
                            isReligionName: true,
                            religionId: parseInt(item.value),
                        });
                    } else {
                        if (text.trim().length === 0) {
                            this.setState({
                                labelReligion: '',
                                religionError: '',
                            });
                        } else {
                            this.setState({ labelReligion: STRINGS.RELIGION });
                        }
                        if (text.trim().length > 0) {
                            ReligionAction({
                                search_text: text,
                            }, data => this.organizatonTypeResponse(data, 'religion'));
                        }

                        this.setState({
                            isReligionName: text.trim().length < 1,
                        });
                    }
                    break;
                case 'relation':
                    if (text.length === 0) {
                        this.setState({
                            labelFrom: '',
                        });
                    } else {
                        this.setState({
                            labelFrom: 'Relationship Status',
                        });
                    }
                    if (item === '0') {
                        this.setState({
                            relationValue: text,
                            relationId: 1,
                        });
                    } else if (item === '1') {
                        this.setState({
                            relationValue: text,
                            relationId: 2,
                        });
                    } else if (item === '2') {
                        this.setState({
                            relationValue: text,
                            relationId: 3,
                        });
                    }

                    break;

                case 'terms':
                    this.setState({ isChecked: !isChecked });
                    break;
                default:
                    break;
            }
        });
        this.checkAllValues();
    };

    /*
       // =============================================================================================
       // religion menu methods
       // =============================================================================================
       */
    _menu = null;

    setMenuRef = ref => {
        this._menu = ref;
    };

    hideMenu = () => {
        this._menu.hide();
    };

    selectMenuItem = (selectedType) => {
        this.setState({
            relegion: selectedType,
        });
        this.hideMenu();
    };

    showMenu = () => {
        this._menu.show();
    };

    _renderBusinessDetail = () => {
        const {
            countryName, cityName, countryError, occupationResponse, occupationName, isOccupationName,
            labelOccupation, occupationId, occupationError, isCountryName, isCityName, labelCountry, labelCity,
            countryResponse, cityResponse, cityError, religionResponse, religionName, isReligionName,
            labelReligion, religionId, religionError, phoneNoText, relationLabel, relationValue,
        } = this.state;
        return (
            <>
                <View style={{
                    flex: 1,
                    // position:'absolute',
                    zIndex: 10,
                    width: wp(90),
                }}>
                    {labelCountry.length !== 0 && <Text style={{
                        fontFamily: FONT_FAMILY.RobotoBold,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}>{STRINGS.ABOUT_BUSINESS_COUNTRY_PLACEHOLDER}</Text>}
                    <Autocomplete
                        // ref={input => { this.textInput = input }}
                        // ref='countryInput'
                        placeholder={STRINGS.ABOUT_BUSINESS_COUNTRY_PLACEHOLDER}
                        placeholderTextColor={COLORS.placeHolder_color}
                        data={countryResponse}
                        style={{
                            paddingVertical: wp(1),
                            fontFamily: FONT_FAMILY.Roboto,
                            fontSize: FONT.TextMedium_2,
                            color: COLORS.black_color,
                        }}
                        inputContainerStyle={{
                            paddingVertical: wp(2),
                            backgroundColor: COLORS.backGround_color,
                            width: wp(90),
                            borderWidth: 0,
                            borderBottomWidth: wp(0.35),
                            borderBottomColor: 'rgb(163,171,180)',
                        }}
                        listContainerStyle={{
                            width: wp(90),
                        }}
                        listStyle={{
                            height: wp(45),
                            width: wp(90),
                            marginLeft: 0,
                        }}
                        // value={countryName}
                        hideResults={isCountryName}
                        defaultValue={countryName}
                        onChangeText={(text) => this.onChange(text, 'country', false)}
                        renderItem={({ item, i }) => (
                            <TouchableOpacity
                                onPress={(countryName) => this.onChange('', 'country', true, item)}>
                                <Text style={{
                                    padding: wp(2), fontFamily: FONT_FAMILY.Roboto,
                                    fontSize: FONT.TextMedium_2,
                                    color: COLORS.black_color,
                                }}>{item.label}</Text>
                                <View style={{ height: wp(0.2), backgroundColor: 'rgb(163,171,180)', width: wp(90) }} />
                                <Spacer space={0.5} />
                            </TouchableOpacity>
                        )}
                    />
                    {countryError !== '' && <Text style={{
                        paddingTop: wp(1),
                        color: COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}>{countryError}</Text>}
                </View>
                <Spacer space={2} />

                <View style={{
                    flex: 1,
                    zIndex: 8,
                    width: wp(90),
                }}>
                    {labelCity.length !== 0 && <Text style={{
                        fontFamily: FONT_FAMILY.RobotoBold,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}>{STRINGS.ABOUT_BUSINESS_CITY_PLACEHOLDER}</Text>}
                    <Autocomplete
                        placeholder={STRINGS.ABOUT_BUSINESS_CITY_PLACEHOLDER}
                        placeholderTextColor={COLORS.placeHolder_color}
                        data={cityResponse}
                        style={{
                            paddingVertical: wp(1),
                            fontFamily: FONT_FAMILY.Roboto,
                            fontSize: FONT.TextMedium_2,
                            color: COLORS.black_color,
                        }}
                        inputContainerStyle={{
                            paddingVertical: wp(2),
                            backgroundColor: COLORS.backGround_color,
                            width: wp(90),
                            borderWidth: 0,
                            borderBottomWidth: wp(0.35),
                            borderBottomColor: 'rgb(163,171,180)',
                        }}
                        listContainerStyle={{
                            width: wp(90),
                        }}
                        listStyle={{
                            height: wp(45),
                            width: wp(90),
                            marginLeft: 0,
                        }}

                        hideResults={isCityName}
                        defaultValue={cityName}
                        onChangeText={text => this.onChange(text, 'city', false)}
                        renderItem={({ item, i }) => (
                            <TouchableOpacity onPress={() =>
                                this.onChange('', 'city', true, item)
                            }>
                                <Text style={{
                                    padding: wp(2), fontFamily: FONT_FAMILY.Roboto,
                                    fontSize: FONT.TextMedium_2,
                                    color: COLORS.black_color,
                                }}>{item.label}</Text>
                                <View style={{ height: wp(0.2), backgroundColor: 'rgb(163,171,180)', width: wp(90) }} />
                                <Spacer space={0.5} />
                            </TouchableOpacity>
                        )}
                    />
                    {cityError !== '' && <Text style={{
                        paddingTop: wp(1),
                        color: COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}>{cityError}</Text>}
                </View>
                <Spacer space={2} />

                <Input
                    inputContainerStyle={{
                        width: wp(90),
                    }}
                    label={STRINGS.MOBILE_NUMBER}
                    labelStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                        width: wp(90),
                    }}
                    placeholder={STRINGS.MOBILE_NUMBER}
                    placeholderTextColor={COLORS.off_black}
                    // disabled={true}
                    ref="Mobile"
                    value={phoneNoText}
                    onChangeText={(text) =>
                        this.setState({
                            phoneNoText: text.trim() && text.replace(/^0+/, ''),
                        })
                    }
                    maxLength={10}
                    keyboardType="phone-pad"
                    returnKeyType={'next'}
                    inputStyle={{
                        paddingLeft: wp(0.5),
                        fontSize: FONT.TextMedium_2,
                    }}
                    containerStyle={{
                        paddingBottom: 0,
                        alignItems: 'center',
                        width: wp('50%'),
                    }}
                />

                <View style={{
                    flex: 1,
                    zIndex: 6,
                    width: wp(90),
                }}>
                    {labelOccupation.length !== 0 && <Text style={{
                        fontFamily: FONT_FAMILY.RobotoBold,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}>{STRINGS.OCCUPATION_TEXT}</Text>}
                    <Autocomplete
                        placeholder={STRINGS.OCCUPATION_TEXT}
                        placeholderTextColor={COLORS.placeHolder_color}
                        data={occupationResponse}
                        style={{
                            paddingVertical: wp(1),
                            fontFamily: FONT_FAMILY.Roboto,
                            fontSize: FONT.TextMedium_2,
                            color: COLORS.black_color,
                        }}
                        inputContainerStyle={{
                            paddingVertical: wp(2),
                            backgroundColor: COLORS.backGround_color,
                            width: wp(90),
                            borderWidth: 0,
                            borderBottomWidth: wp(0.35),
                            borderBottomColor: 'rgb(163,171,180)',
                        }}
                        listContainerStyle={{
                            width: wp(90),
                        }}
                        listStyle={{
                            height: wp(45),
                            width: wp(90),
                            marginLeft: 0,
                        }}

                        hideResults={isOccupationName}
                        defaultValue={occupationName}
                        onChangeText={(text) => this.onChange(text, 'occupation', false)}
                        renderItem={({ item, i }) => (
                            <TouchableOpacity
                                onPress={() =>
                                    this.onChange('', 'occupation', true, item)
                                }>
                                <Text style={{
                                    padding: wp(2), fontFamily: FONT_FAMILY.Roboto,
                                    fontSize: FONT.TextMedium_2,
                                    color: COLORS.black_color,
                                }}>{item.label}</Text>
                                <View style={{ height: wp(0.2), backgroundColor: 'rgb(163,171,180)', width: wp(90) }} />
                                <Spacer space={0.5} />
                            </TouchableOpacity>
                        )}
                    />
                    {occupationError !== '' && <Text style={{
                        paddingTop: wp(1),
                        color: COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}>{occupationError}</Text>}
                </View>
                <Spacer space={2} />

                <View style={{ width: wp(90) }}>
                    {relationLabel.length !== 0 && <Text style={{
                        fontFamily: FONT_FAMILY.RobotoSemiBold,
                        fontSize: FONT.TextSmall_2,
                        color: COLORS.off_black,
                        paddingBottom: wp(3),
                    }}>{'Allow votes from'}</Text>}
                    <ModalDropdown
                        defaultValue={relationValue}
                        style={{
                            width: wp(90),
                            borderBottomWidth: wp(0.3),
                            borderBottomColor: COLORS.arrow_color,
                        }}
                        textStyle={{
                            paddingBottom: wp(3),
                            fontFamily: FONT_FAMILY.Roboto,
                            fontSize: FONT.TextMedium_2,
                            color: COLORS.black_color,
                        }}
                        dropdownStyle={{ width: wp(90) }}
                        dropdownTextStyle={{
                            paddingVertical: wp(2),
                            fontFamily: FONT_FAMILY.Roboto,
                            fontSize: FONT.TextMedium_2,
                            color: COLORS.black_color,
                        }}
                        options={RELATION_SHIP}
                        onSelect={(label, value) => this.onChange(value, 'relation', false, label)}
                    />
                    <Spacer space={3} />
                </View>

                <View style={{
                    flex: 1,
                    zIndex: 4,
                    width: wp(90),
                }}>
                    {labelReligion.length !== 0 && <Text style={{
                        fontFamily: FONT_FAMILY.RobotoBold,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}>{STRINGS.RELIGION}</Text>}
                    <Autocomplete
                        placeholder={STRINGS.RELIGION}
                        placeholderTextColor={COLORS.placeHolder_color}
                        data={religionResponse}
                        style={{
                            paddingVertical: wp(1),
                            fontFamily: FONT_FAMILY.Roboto,
                            fontSize: FONT.TextMedium_2,
                            color: COLORS.black_color,
                        }}
                        inputContainerStyle={{
                            paddingVertical: wp(2),
                            backgroundColor: COLORS.backGround_color,
                            width: wp(90),
                            borderWidth: 0,
                            borderBottomWidth: wp(0.35),
                            borderBottomColor: 'rgb(163,171,180)',
                        }}
                        listContainerStyle={{
                            width: wp(90),
                        }}
                        listStyle={{
                            height: wp(45),
                            width: wp(90),
                            marginLeft: 0,
                        }}

                        hideResults={isReligionName}
                        defaultValue={religionName}
                        onChangeText={(text) => this.onChange(text, 'religion', false)}
                        renderItem={({ item, i }) => (
                            <TouchableOpacity
                                onPress={() => this.onChange('', 'religion', true, item)}>
                                <Text style={{
                                    padding: wp(2), fontFamily: FONT_FAMILY.Roboto,
                                    fontSize: FONT.TextMedium_2,
                                    color: COLORS.black_color,
                                }}>{item.label}</Text>
                                <View style={{ height: wp(0.2), backgroundColor: 'rgb(163,171,180)', width: wp(90) }} />
                                <Spacer space={0.5} />
                            </TouchableOpacity>
                        )}
                    />
                    {religionError !== '' && <Text style={{
                        paddingTop: wp(1),
                        color: COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}>{religionError}</Text>}
                </View>
            </>

        );
    };


    render() {
        const { navigation } = this.props;
        const { areAllValuesValid } = this.state;
        return (
            <SafeAreaViewContainer>
                <TopHeader
                    rightComponentNotVisible={true}
                    text={'Add More Details'}
                    onPressBackArrow={() => navigation.goBack()} />
                <KeyboardAvoidingView
                    style={{ flex: 1, backgroundColor: COLORS.backGround_color }}
                    behavior={(Platform.OS === 'ios') ? 'padding' : null}
                >
                    <TouchableWithoutFeedback
                        onPress={() => {
                            Keyboard.dismiss();
                        }}>
                        <ScrollContainer>
                            <MainContainer>
                                <View
                                    style={{ flex: 0.3, alignItems: 'center', backgroundColor: COLORS.backGround_color }}>
                                    <Spacer space={5} />
                                    {this._renderBusinessDetail(false)}
                                    {/*<Spacer space={1}/>*/}
                                    {/*{this._relegionSelection()}*/}
                                    <Spacer space={5} />
                                    <PrimaryButton
                                        onPress={() => this.onEmailEditingEnd()}
                                        // isDisabled={!areAllValuesValid}
                                        // bgColor={areAllValuesValid ? COLORS.app_theme_color : COLORS.app_theme_color_disabled}
                                        bgColor={COLORS.app_theme_color}
                                        text={STRINGS.SAVE_TEXT} />

                                    <Spacer space={4} />
                                </View>
                            </MainContainer>
                        </ScrollContainer>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </SafeAreaViewContainer>
        );
    }
}


export default AddMoreDetails;
