import React from 'react';
import {
    Image,
    KeyboardAvoidingView,
    Keyboard,
    Text,
    TouchableWithoutFeedback,
    View, TouchableOpacity, ImageBackground, Alert,
} from 'react-native';
import Menu, {MenuItem} from 'react-native-material-menu';
import styles from './styles';

import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {Input} from 'react-native-elements';
import BaseClass from '../../../../utils/BaseClass';
import {MainContainer, SafeAreaViewContainer, ScrollContainer} from '../../../../utils/BaseStyle';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {ICONS} from '../../../../utils/ImagePaths';
import EditIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import STRINGS from '../../../../utils/Strings';
import COLORS from '../../../../utils/Colors';
import {validateEmail, validatePassword} from '../../../../utils/Validations';
import moment from 'moment';
import OrientationLoadingOverlay from '../../../../customComponents/CustomLoader';
import {FONT_FAMILY} from '../../../../utils/Font';
import {FONT} from '../../../../utils/FontSizes';
// import {navigate} from "../../../../../RootNavigation";
import {Chip} from 'react-native-paper';
import {UpdateProfilePersonalAction} from '../../../../redux/actions/UpdateProfilePersonalAction';
import AsyncStorage from '@react-native-community/async-storage';
import {Spacer} from '../../../../customComponents/Spacer';
import {PrimaryButton} from '../../../../customComponents/ButtonComponent';
import TopHeader from '../../../../customComponents/Header';
import {UserAvailabillityAction} from '../../../../redux/actions/UserAvailabilityAction';
import RightArrow from 'react-native-vector-icons/Entypo';
import ImagePicker from 'react-native-image-crop-picker';
import {CommonActions} from "@react-navigation/native"

//const StaticArray = ["Sports","Technology",  "Art",]


class PersonalEditProfile extends BaseClass {
    constructor(props) {
        super(props);
        const {Alldata, userData, interestData} = this.props.route.params;
        this.state = {
            data: this.props.route.params.Alldata,
            user_metaData: this.props.route.params.userData,


            profileImage: userData !== null && userData.profile_image !== null ? userData.profile_image : 'https://www.cornwallbusinessawards.co.uk/wp-content/uploads/2017/11/dummy450x450-300x300.jpg',
            profileBase64: '',

            gender: '',
            genderId: userData !== null && userData.gender !== null ? parseInt(userData.gender) : 0,
            privacy: '',
            privacyId: userData !== null && userData.privacy !== null ? parseInt(userData.privacy) : 0,

            validChildFName: '',
            labelChildFName: 'First Name',
            childFirstName: Alldata.firstname !== null ? Alldata.firstname : '',

            validChildLName: '',
            labelChildLName: 'Last Name',
            childLastName: Alldata.lastname !== null ? Alldata.lastname : '',

            validUName: '',
            labelUName: 'Username',
            userName: Alldata.username !== null ? Alldata.username : '',

            validEmail: '',
            labelEmail: 'Email ID',
            parentemail: Alldata.email !== null ? Alldata.email : '',

            labelDate: 'Date of Birth',
            datePicker: Alldata.dob,

            checked: false,
            areAllValuesValid: false,

            socialId: '',
            socialPlatform: '',

            isLoading: false,
            StaticArray: interestData !== undefined ? interestData : [],
            addmoreDetails: {},
            authToken: '',

            countryName: userData !== null && userData.country !== null ? userData.country : '',
            cityName: userData !== null && userData.city_name !== null ? userData.city_name : '',
            cityId: userData !== null && userData.city !== null ? parseInt(userData.city) : 0,
            relationId: userData !== null && userData.relationship_status !== null ? parseInt(userData.relationship_status) : 0,
            occupationId: userData !== null && userData.occupation !== null ? parseInt(userData.occupation) : 0,
            religion: userData !== null && userData.religion !== null ? parseInt(userData.religion) : 0,
            phone_no: Alldata !== null && Alldata.phone_no !== null ? Alldata.phone_no : '',
        };
    }

    componentDidMount() {
        const {genderId, privacyId} = this.state;

        const {navigation} = this.props;
        AsyncStorage.getItem(STRINGS.accessToken, async (error, result) => {
            if (result !== undefined && result !== null) {
                let loginData = JSON.parse(result);
                console.log('result is', result);
                await this.setState({
                    authToken: loginData,
                });
                if (genderId === 1) {
                    this.setState({gender: STRINGS.MALE_TEXT});
                } else if (genderId === 2) {
                    this.setState({gender: STRINGS.FEMALE_TEXT});
                } else if (genderId === 3) {
                    this.setState({gender: STRINGS.OTHERS_TEXT});
                }

                if (privacyId === 1) {
                    this.setState({privacy: STRINGS.PUBLIC_TEXT});
                } else if (privacyId === 2) {
                    this.setState({privacy: STRINGS.PRIVATE_TEXT});
                }
            }
        });
        this.checkAllValues();
    }

    onUserEditingEnd = () => {
        UserAvailabillityAction({
            username: this.state.userName,
        }, data => this.isUserAvailableResponse(data));
    };

    onEmailEditingEnd = () => {
        const {authToken, profileImage, gender, genderId, privacy, privacyId, parentemail, userName, childFirstName, childLastName, datePicker, StaticArray, profileBase64, addmoreDetails} = this.state;
        // let addMore_Data = this.props.route.params.addMoreDetails;
        let arr = StaticArray;
        let array2 = arr.map((item, index) => {
            return item.id;
        });

        // if (profileImage === 'https://www.cornwallbusinessawards.co.uk/wp-content/uploads/2017/11/dummy450x450-300x300.jpg') {
        //     this.showToastAlert('Profile Image can not be empty');
        // } else {
        UpdateProfilePersonalAction(
            {
                email: parentemail,
                phone_no: addmoreDetails.phoneNoText,
                firstname: childFirstName,
                lastname: childLastName,
                dob: datePicker,
                city: addmoreDetails.cityId,
                country: addmoreDetails.countryName,
                religion: addmoreDetails.religionId,
                gender: genderId,
                privacy: privacyId,
                interest: JSON.stringify(array2),
                relationship_status: addmoreDetails.relationId,
                occupation: addmoreDetails.occupationId,
                // profile_image: profileImage,
                profile_image: profileImage.startsWith('http://') ? '' : profileBase64,
                authToken: authToken,
                user_type: 3,
                account_type: 1,
            },
            data => this.updateProfileResponse(data));
        this.showDialog();

        // }
    };

    updateProfileResponse = (response) => {
        const {navigation} = this.props;
        const {navigate} = navigation;
        console.warn('CHKK RES-->', response);
        if (response !== 'Network request failed') {
            this.hideDialog();
            const {code, message, data, technical_message} = response;
            if (code === '200') {
                this.hideDialog();
                this.showToastSucess(message);
                navigation.goBack();
            } else if (code === '403') {
                this.hideDialog();
                this.showToastAlert(message);
            } else if (code === '422') {
                this.hideDialog();
                this.showToastAlert(technical_message.profile_image);
            }else if (code === '401') {
                this.hideDialog();
                AsyncStorage.removeItem(STRINGS.accessToken);
                AsyncStorage.removeItem(STRINGS.loginData);
                AsyncStorage.removeItem(STRINGS.userPersonalInfo);
                navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [
                            {name: 'login'},
                        ],
                    }),
                );
                this.showToastAlert(message);
            }
             else {
                this.hideDialog();
                navigation.goBack();
                this.showToastSucess('Profile updated successfully.');
                // this.showToastAlert('Profile image can not be empty.');
            }
        } else {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET);
        }
    };

    componentWillReceiveProps(nextProps) {
        const {route} = nextProps;
        if (route.params !== undefined) {
            if (route.params.selectedinterest !== undefined) {

                // this.setState({
                //     StaticArray: route.params.selectedinterest,
                // });
                let res = route.params.selectedinterest;
                this.onChange(res, 'interest');
            }
            if (route.params.addMoreDetails !== undefined) {
                // this.setState({
                //     addmoreDetails: route.params.addMoreDetails,
                // });
                let res = route.params.addMoreDetails;
                this.onChange(res, 'addMore');
            }
        }
    }

    onChange = async (text, type, index) => {
        const {password, checked} = this.state;
        let arr = [1];
        await arr.map(item => {
            switch (type) {
                case 'childFirstName':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelChildFName: '',
                        });
                    } else if (text.trim().length < 3) {
                        this.setState({
                            validChildFName: STRINGS.name_minimum_three,
                            labelChildFName: 'First Name',
                        });
                    } else {
                        this.setState({
                            validChildFName: '',
                        });
                    }
                    this.setState({childFirstName: text});
                    break;
                case 'childLastName':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelChildLName: '',
                        });
                    } else if (text.trim().length < 3) {
                        this.setState({
                            validChildLName: STRINGS.name_minimum_three,
                            labelChildLName: 'Last Name',
                        });
                    } else {
                        this.setState({
                            validChildLName: '',
                        });
                    }
                    this.setState({childLastName: text});
                    break;
                case 'userName':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelUName: '',
                        });
                    } else if (text.trim().length < 5) {
                        this.setState({
                            //  validUName: 'Username Unavailable',
                            labelUName: 'Username',
                        });
                        /* } else {
                             this.setState({
                                 validUName: 'Username Available',
                             });*/
                    }
                    this.setState({userName: text});
                    break;
                case 'parentemail':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelEmail: '',
                        });
                    } else if (!validateEmail(text)) {
                        this.setState({
                            validEmail: STRINGS.valid_email,
                            labelEmail: 'Email ID',
                        });
                    } else {
                        this.setState({
                            validEmail: '',
                        });
                    }
                    this.setState({parentemail: text});
                    break;
                case 'dateOfBirth':
                    this.hideDatePicker();
                    if (text !== '') {
                        this.setState({
                            labelDate: 'Date of Birth',
                        });
                    } else {
                        this.setState({
                            labelDate: 'Date of Birth',
                        });
                    }
                    let date = moment(text).format('YYYY-MM-DD');
                    console.warn('text', date);
                    this.setState({
                        datePicker: date,
                        // datePicker1: text
                    });
                    break;
                case 'gender':
                    if (text === STRINGS.MALE_TEXT) {
                        this.setState({
                            genderId: 1,
                        });
                    } else if (text === STRINGS.FEMALE_TEXT) {
                        this.setState({
                            genderId: 2,
                        });
                    } else if (text === STRINGS.OTHERS) {
                        this.setState({
                            genderId: 3,
                        });
                    }
                    this.setState({
                        gender: text,
                    });
                    break;
                case 'privacy':
                    if (text === STRINGS.PUBLIC_TEXT) {
                        this.setState({
                            privacyId: 1,
                        });
                    } else if (text === STRINGS.PRIVATE_TEXT) {
                        this.setState({
                            privacyId: 2,
                        });
                    }
                    this.setState({
                        privacy: text,
                    });

                    break;
                case 'interest':
                    this.setState({
                        StaticArray: text,
                    });
                    break;
                case 'addMore':
                    this.setState({
                        addmoreDetails: text,
                        // countryName:text, cityName, cityId, relationId, occupationId, religion, phone_no
                    });
                    break;
                default:
                    break;
            }
        });
        this.checkAllValues();
    };

    checkAllValues = () => {
        const {
            validChildLName, validChildFName, validUName, validEmail, StaticArray, addmoreDetails,
            parentemail, childFirstName, childLastName, userName, gender, privacy,
        } = this.state;

        if (validChildFName !== '' && childFirstName.length === 0) {
            this.setState({areAllValuesValid: false});
        } else if (validChildLName !== '' && childLastName.length === 0) {
            this.setState({areAllValuesValid: false});
        } else if (validUName === 'Username Available' && userName.length === 0) {
            this.setState({areAllValuesValid: false});
        } else if (validEmail !== '' && parentemail.length === 0) {
            this.setState({areAllValuesValid: false});
            // } else if (gender === '') {
            //     this.setState({areAllValuesValid: false});
            // } else if (privacy === '') {
            //     this.setState({areAllValuesValid: false});
            // } else if (StaticArray.length === 0) {
            //     this.setState({areAllValuesValid: false});
            // } else if (Object.keys(addmoreDetails).length === 0) {
            //     this.setState({areAllValuesValid: false});
        } else {
            this.setState({areAllValuesValid: true});
        }
    };

    chooseInterests() {
        //alert("Coming Soon")
        const {StaticArray} = this.state;
        const {navigate} = this.props.navigation;
        navigate('ChooseInterests', {interestData: StaticArray});
    }

    moreDetails() {
        const {countryName, cityName, cityId, relationId, occupationId, religion, phone_no} = this.state;
        const {navigate} = this.props.navigation;
        navigate('AddMoreDetails', {countryName, cityName, cityId, relationId, occupationId, religion, phone_no});
    }


    // ----------------------------------------
    // ----------------------------------------
    // This method is use to open camera and gallery
    // ----------------------------------------

    onEditPress = (showAvatar) => {
        Alert.alert(
            STRINGS.APP_NAME,
            STRINGS.SELECT_IMAGE,
            [
                {
                    text: STRINGS.CANCEL_TEXT, style: 'cancel',
                },
                {
                    text: STRINGS.GALLERY_TEXT, onPress: () => {
                        this.chooseFromGallery(showAvatar);
                    },
                },
                {
                    text: STRINGS.CAMERA_TEXT, onPress: () => {
                        this.chooseFromCamera(showAvatar);
                    },
                },
            ],
            {cancelable: false});
    };

    // ----------------------------------------
    // ----------------------------------------
    // Gallery
    // ----------------------------------------

    chooseFromGallery = (showAvatar) => {
        ImagePicker.openPicker({
            cropping: true,
            mediaType: 'photo',
            compressImageQuality: 0.2,
            includeBase64: true,
        }).then(image => {
            this.setState({
                profileImage: image.path,
                profileBase64: `data:${image.mime};base64,${image.data}`,
            });
        }).catch(e => {
            console.warn(e);
        });
    };

    // ----------------------------------------
    // ----------------------------------------
    // camera
    // ----------------------------------------
    chooseFromCamera = (showAvatar) => {
        ImagePicker.openCamera({
            compressImageQuality: 0.2,
            includeBase64: true,
            cropping: true,
            mediaType: 'photo',
        }).then(image => {
            this.setState({
                profileImage: image.path,
                profileBase64: `data:${image.mime};base64,${image.data}`,
            });
        }).catch(e => console.log(e));
    };


    /*
       // =============================================================================================
       // Gender menu methods
       // =============================================================================================
       */
    _menuGender = null;

    setMenuRefGender = ref => {
        this._menuGender = ref;
    };

    hideMenuGender = () => {
        this._menuGender.hide();
    };

    selectMenuGenderItem = (selectedType) => {
        this.onChange(selectedType, 'gender');
        this.hideMenuGender();
    };

    showMenuGender = () => {
        this._menuGender.show();
    };


    /*
       // =============================================================================================
       // Privacy menu methods
       // =============================================================================================
       */
    _menu = null;

    setMenuRef = ref => {
        this._menu = ref;
    };

    hideMenu = () => {
        this._menu.hide();
    };

    selectMenuItem = (selectedType) => {
        this.onChange(selectedType, 'privacy');
        this.hideMenu();
    };

    showMenu = () => {
        this._menu.show();
    };


    /*  hideDatePicker = () => {
          this.isPickerShowing = false;
          this.setState({
              showPicker: false,
          });
      };*/

    onDelete(i) {
        this.setState({
            StaticArray: this.state.StaticArray.filter((item) => {
                if (item === i) {
                } else {
                    return item;
                }
            }),
        });

    };


    _renderInputs = () => {
        const {
            data, user_metaData,
            labelDate, gender, privacy,
            datePicker, validChildFName, labelChildFName, validChildLName, labelChildLName, validUName, labelUName,
            validEmail, labelEmail, validPassword, labelPassword, validCPassword, labelCPassword, checked,
            parentemail, childEmail, socialId, childFirstName, childLastName, userName, password, confirmPassword,
        } = this.state;
        return (
            <View style={{alignItems: 'center'}}>
                <Input
                    placeholder='First Name'
                    ref={'childFirstName'}
                    label={labelChildFName}
                    labelStyle={styles.input_label_style}
                    errorStyle={styles.input_error_style}
                    errorMessage={validChildFName}
                    inputStyle={styles.input_input_container}
                    inputContainerStyle={{width: wp(90)}}
                    value={childFirstName}
                    onChangeText={(text) => this.onChange(text, 'childFirstName')}
                    onSubmitEditing={() => this.refs.ChildLastName.focus()}
                    returnKeyType={'next'}

                />
                <Spacer space={1}/>
                <Input
                    ref={'ChildLastName'}
                    placeholder='Last Name'
                    label={labelChildLName}
                    labelStyle={styles.input_label_style}
                    errorStyle={styles.input_error_style}
                    errorMessage={validChildLName}
                    inputStyle={styles.input_input_container}
                    inputContainerStyle={{width: wp(90)}}
                    value={childLastName}
                    onChangeText={(text) => this.onChange(text, 'childLastName')}
                    onSubmitEditing={() => this.refs.ParentEmail.focus()}
                    returnKeyType={'next'}
                />
                <Spacer space={1}/>
                <Input
                    editable={false}
                    ref={'UserName'}
                    disabled={socialId !== ''}
                    placeholder='Username'
                    label={labelUName}
                    labelStyle={styles.input_label_style}
                    errorStyle={[styles.input_error_style,
                        {color: validUName === 'Username available' ? COLORS.valid_Color : COLORS.invalid_color}]}
                    errorMessage={validUName}
                    inputStyle={styles.input_input_container}
                    inputContainerStyle={{width: wp(90)}}
                    value={userName}
                    onChangeText={(text) => this.onChange(text, 'userName')}
                    onEndEditing={() => this.onUserEditingEnd()}
                    onSubmitEditing={() => this.refs.Password.focus()}
                    returnKeyType={'next'}
                />
                <Spacer space={1}/>
                <Input
                    ref={'ParentEmail'}
                    placeholder='Email ID'
                    label={labelEmail}
                    labelStyle={styles.input_label_style}
                    errorStyle={styles.input_error_style}
                    inputStyle={styles.input_input_container}
                    errorMessage={validEmail}
                    inputContainerStyle={{width: wp(90)}}
                    value={parentemail}
                    onChangeText={(text) => this.onChange(text, 'parentemail')}
                    returnKeyType={'done'}
                />
                <Spacer space={1}/>
                <Input
                    pointerEvents={'none'}
                    editable={false}
                    placeholder='Date of Birth'
                    label={labelDate}
                    value={datePicker}
                    labelStyle={styles.input_label_style}
                    inputStyle={styles.input_input_container}
                    inputContainerStyle={{width: wp(90)}}
                    // onChangeText={(text) => this.onChange(text, 'confirmPassword')}
                />

                <Spacer space={1}/>
                <View style={{width: wp(90)}}>
                    <Text style={{
                        fontFamily: FONT_FAMILY.RobotoBold,
                        fontSize: FONT.TextSmall,
                        color: COLORS.off_black,
                    }}>{STRINGS.GENDER_TEXT}</Text>
                </View>
                <Spacer space={1.5}/>
                <TouchableOpacity onPress={() => this.showMenuGender()}>
                    <View style={{
                        marginRight: wp('2%'),
                        borderColor: COLORS.off_black,
                        width: wp(90),

                    }}>
                        <Menu
                            ref={this.setMenuRefGender}
                            style={{width: wp(90)}}
                            button={<Input
                                inputContainerStyle={{
                                    borderBottomColor: COLORS.placeHolder_color,
                                    width: wp(90),
                                    height: wp('9%'),
                                    paddingHorizontal: wp(1),
                                }}
                                editable={false}
                                pointerEvents="none"
                                placeholder={STRINGS.GENDER_TEXT}
                                placeholderTextColor={COLORS.black_color}
                                value={gender}
                                returnKeyType={'next'}
                                inputStyle={{
                                    fontFamily: FONT_FAMILY.Roboto,
                                    paddingVertical: 0,
                                    height: wp('7%'),
                                    fontSize: FONT.TextMedium_2,
                                }}
                                containerStyle={{
                                    paddingBottom: 0,
                                    alignItems: 'center',
                                    width: wp(90),
                                    paddingRight: 10,
                                    height: wp('11%'),
                                    justifyContent: 'center',
                                    color: COLORS.black_color,
                                }}
                                rightIcon={{name: 'chevron-small-down', type: 'entypo', color: COLORS.black_color}}
                            />}
                        >
                            <MenuItem
                                textStyle={{
                                    fontFamily: FONT_FAMILY.Roboto,
                                    fontSize: FONT.TextSmall_2,
                                    color: COLORS.black_color,
                                }}
                                onPress={() => this.selectMenuGenderItem(STRINGS.MALE_TEXT)}>{STRINGS.MALE_TEXT}</MenuItem>
                            <MenuItem
                                textStyle={{
                                    fontFamily: FONT_FAMILY.Roboto,
                                    fontSize: FONT.TextSmall_2,
                                    color: COLORS.black_color,
                                }}
                                onPress={() => this.selectMenuGenderItem(STRINGS.FEMALE_TEXT)}>{STRINGS.FEMALE_TEXT}</MenuItem>
                            <MenuItem
                                textStyle={{
                                    fontFamily: FONT_FAMILY.Roboto,
                                    fontSize: FONT.TextSmall_2,
                                    color: COLORS.black_color,
                                }}
                                onPress={() => this.selectMenuGenderItem(STRINGS.OTHERS)}>{STRINGS.OTHERS}</MenuItem>
                        </Menu>
                    </View>
                </TouchableOpacity>


                <Spacer space={1}/>
                <View style={{width: wp(90)}}>
                    <Text style={{
                        fontFamily: FONT_FAMILY.RobotoBold,
                        fontSize: FONT.TextSmall,
                        color: COLORS.off_black,
                    }}>{STRINGS.PRIVACY_TEXT}</Text>
                </View>
                <Spacer space={1.5}/>
                <TouchableOpacity onPress={() => this.showMenu()}>
                    <View style={{
                        marginRight: wp('2%'),
                        borderColor: COLORS.off_black,
                    }}>
                        <Menu
                            ref={this.setMenuRef}
                            style={{width: wp('90%')}}
                            button={<Input
                                inputContainerStyle={{
                                    borderBottomColor: COLORS.placeHolder_color,
                                    width: wp(90),
                                    height: wp('9%'),
                                    paddingHorizontal: wp(1),

                                }}
                                editable={false}
                                pointerEvents="none"
                                placeholder={STRINGS.PRIVACY_TEXT}
                                placeholderTextColor={COLORS.black_color}
                                value={privacy}
                                returnKeyType={'next'}
                                inputStyle={{
                                    fontFamily: FONT_FAMILY.Roboto,
                                    paddingVertical: 0,
                                    height: wp('7%'),
                                    fontSize: FONT.TextMedium_2,
                                }}
                                containerStyle={{
                                    paddingBottom: 0,
                                    alignItems: 'center',
                                    width: wp('90%'),
                                    paddingRight: 10,
                                    height: wp('9%'),
                                    justifyContent: 'center',
                                    color: COLORS.black_color,
                                }}
                                rightIcon={{name: 'chevron-small-down', type: 'entypo', color: COLORS.black_color}}
                            />}
                        >
                            <MenuItem textStyle={{
                                fontFamily: FONT_FAMILY.Roboto,
                                fontSize: FONT.TextSmall_2,
                                color: COLORS.black_color,
                            }}
                                      onPress={() => this.selectMenuItem(STRINGS.PUBLIC_TEXT)}>{STRINGS.PUBLIC_TEXT}</MenuItem>
                            <MenuItem textStyle={{
                                fontFamily: FONT_FAMILY.Roboto,
                                fontSize: FONT.TextSmall_2,
                                color: COLORS.black_color,
                            }}
                                      onPress={() => this.selectMenuItem(STRINGS.PRIVATE_TEXT)}>{STRINGS.PRIVATE_TEXT}</MenuItem>
                        </Menu>


                    </View>
                </TouchableOpacity>
                <Spacer space={3}/>
                <TouchableOpacity onPress={() => this.chooseInterests()}>
                    <View style={{width: wp('90%'), flexDirection: 'row', justifyContent: 'space-between'}}>

                        <Text style={{
                            alignSelf: 'center',
                            fontFamily: FONT_FAMILY.PoppinsBold,
                            fontSize: FONT.TextSmall,
                            color: COLORS.off_black,
                        }}>
                            {STRINGS.CHOOSE_INTEREST}
                        </Text>
                        <RightArrow name={'chevron-right'} size={wp(7)} color={COLORS.arrow_color}/>

                    </View>
                </TouchableOpacity>
                <Spacer space={1}/>
                <View style={{
                    flexWrap: 'wrap',
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                    width: wp(90),
                }}>
                    {this.state.StaticArray.map((item, index) => {
                            return (
                                <Chip
                                    onClose={() => this.onDelete(item)}
                                    style={{
                                        margin: wp(1),
                                        height: wp(8),
                                        backgroundColor: COLORS.button_Light_color,
                                    }}
                                >{item.categoryName}</Chip>
                            );
                        },
                    )}
                </View>


                <Spacer space={3}/>
                <TouchableOpacity onPress={() => this.moreDetails()}>
                    <View style={{width: wp('90%'), flexDirection: 'row', justifyContent: 'space-between'}}>

                        <Text style={{
                            alignSelf: 'center',
                            fontFamily: FONT_FAMILY.PoppinsBold,
                            fontSize: FONT.TextSmall,
                            color: COLORS.off_black,
                        }}>
                            {STRINGS.ADD_MORE_DETAILS}
                        </Text>
                        <RightArrow name={'chevron-right'} size={wp(7)} color={COLORS.arrow_color}/>

                    </View>
                </TouchableOpacity>
            </View>
        );
    };


    /*    chooseAvatar () {
            const {navigate} = this.props.navigation;
            navigate('chooseAvatar')
        }*/


// =============================================================================================
    // Render method for Custom Loader
    // =============================================================================================

    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={STRINGS.LOADING_TEXT}/>
        );
    };

    render() {
        const {areAllValuesValid, profileImage} = this.state;
        const {navigation} = this.props;
        const {navigate} = navigation;
        return (
            <SafeAreaViewContainer>
                <TopHeader
                    rightComponentNotVisible={true}
                    text={'Edit Profile'}
                    onPressBackArrow={() => navigation.goBack()}/>
                <KeyboardAvoidingView
                    style={styles.keyboard_style}
                    behavior={(Platform.OS === 'ios') ? 'padding' : null}>
                    <TouchableWithoutFeedback
                        onPress={() => {
                            Keyboard.dismiss();
                        }}>
                        <ScrollContainer>
                            <MainContainer>
                                <Spacer space={2}/>
                                <View style={{flexDirection: 'column'}}>
                                    <Image style={{height: wp(30), width: wp(30), borderRadius: wp(30) / 2}}
                                           source={{uri: profileImage}} resizeMode={'cover'}/>

                                    <EditIcon style={{position: 'absolute', right: wp(1), bottom: -wp(3.5)}}
                                              size={40}
                                              color={COLORS.app_theme_color}
                                        //onPress={alert("CAMERA Coming Soon")}
                                              onPress={() => this.onEditPress()}
                                              name={'pencil-circle'}/>
                                    {/* <Image
                                        source={ICONS.NEXT_PAGE_IMAGE} resizeMode={'contain'}/>*/}


                                </View>
                                <Spacer space={5}/>
                                {this._renderInputs()}
                                <Spacer space={4}/>
                                <PrimaryButton
                                    onPress={() => this.onEmailEditingEnd()}
                                    isDisabled={!areAllValuesValid}
                                    bgColor={areAllValuesValid ? COLORS.app_theme_color : COLORS.app_theme_color_disabled}
                                    text={STRINGS.SAVE_TEXT}/>
                                <Spacer space={4}/>
                            </MainContainer>
                            {this._renderCustomLoader()}
                        </ScrollContainer>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </SafeAreaViewContainer>
        );
    }
}


export default PersonalEditProfile;
