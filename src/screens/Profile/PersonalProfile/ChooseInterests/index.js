import React from 'react';
import {
    Image,
    View,
    Text, Alert, TouchableOpacity,
} from 'react-native';
import BaseClass from '../../../../utils/BaseClass';
import {SafeAreaViewContainer} from '../../../../utils/BaseStyle';
import {Chip} from 'react-native-paper';
import STRINGS from '../../../../utils/Strings';
import {Spacer} from '../../../../customComponents/Spacer';
import {PrimaryButton} from '../../../../customComponents/ButtonComponent';
import TopHeader from '../../../../customComponents/Header';
import OrientationLoadingOverlay from '../../../../customComponents/CustomLoader';
import COLORS from '../../../../utils/Colors';
// import {RandomAvatarAction} from "../../../../redux/actions/RandomAvatarAction";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

import AsyncStorage from '@react-native-community/async-storage';
import {GetUserInterests} from '../../../../redux/actions/ChooseInterestAction';
import {CommonActions} from "@react-navigation/native"

/*
const StaticArray = ["Education", "Technology", "Food", "Art", "Sports", "Gardening", "Fashion", "Business", "Shopping", "Entrepreneurship", "Shopping", "Health", "Management", "Marketing", "Finance", "Games",]
*/

class ChooseInterests extends BaseClass {
    constructor(props) {
        super(props);
        const {interestData} = this.props.route.params;
        this.state = {
            isLoading: false,
            image: '',
            authToken: undefined,
            data: [],
            StaticArray: interestData,

        };
    }


    componentDidMount() {
        const {navigation} = this.props;
        AsyncStorage.getItem(STRINGS.accessToken, async (error, result) => {
            if (result !== undefined && result !== null) {
                let loginData = JSON.parse(result);
                console.log('result is', result);
                await this.setState({
                    authToken: loginData,
                });
                this.getInterestData(loginData);
            }
        });
        // this._unsubscribe = navigation.addListener('focus', () => {
        //     this.onFocusFunction();
        // });


    }


    //------------get Avatar data--------------
    getInterestData = (loginData) => {
        const {authToken} = this.state;

        // this.showDialog();

        GetUserInterests(
            {
                accessToken: loginData,
            }, data => this.getInterestResponse(data));
    };


    getInterestResponse = (response) => {
        console.warn('response is----', response);
        const {StaticArray} = this.state;
        const {navigation} = this.props;
        if (response === 'Network request failed') {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET);
        } else {
            const {message, code, data} = response;
            console.log('data is----', data);
            if (code === '200') {
                this.hideDialog();
                let res = [];
                data.map((item, index) =>
                    res.push({
                        categoryName: item.name,
                        id: item.id,
                        isChecked: false,
                    }),
                );

                res.map((item, index) => {
                    StaticArray.map((item1, index1) => {
                        if (item.categoryName === item1.categoryName) {
                            return item.isChecked = true;
                        }
                    });
                });

                this.setState({
                    data: res,
                });

            } else if (code === '111') {
                this.hideDialog();
                console.warn(message);
            } else if (code === '201') {
                this.hideDialog();
                this.showToastAlert('User Unauthorized');
            } else if (code === '401') {
                this.hideDialog();
                AsyncStorage.removeItem(STRINGS.accessToken);
                AsyncStorage.removeItem(STRINGS.loginData);
                AsyncStorage.removeItem(STRINGS.userPersonalInfo);
                navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [
                            {name: 'login'},
                        ],
                    }),
                );
                this.showToastAlert(message);
            }
            else {
                this.showToastAlert('Session expired');
                this.hideDialog();
            }
        }
    };


    // onFocusFunction = () => {
    //     //    this.showDialog();
    //     RandomAvatarAction({}, data => this.isAvatarResponse(data));
    //     //  ChooseInterestAction({}, data => this.isChooseAvatarResponse(data));
    //     this.componentDidMount();
    //     //  this.componentDidMount();
    // };

    // componentWillUnmount() {
    //     this._unsubscribe();
    // }

    isAvatarResponse = (response) => {
        if (response !== 'Network request failed') {
            //   console.warn("tttt", response)
            this.hideDialog();
            const {code, message, data} = response;
            if (code === '200') {
                this.hideDialog();
                this.setState(
                    {
                        image: data[0].avatar_url,
                    },
                );
            } else if (code === '403') {
                this.hideDialog();
            }
        } else {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET);
        }
    };


// =============================================================================================
    // Render method for Custom Loader
    // =============================================================================================

    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={STRINGS.LOADING_TEXT}/>
        );
    };

    iconSelectClick = (item) => {
        const {navigation} = this.props;
        const {navigate} = navigation;
        navigate('accounts', {selectedIcon: item});
    };

    onEmailEditingEnd = () => {
        const {navigation} = this.props;
        const {navigate} = navigation;
        navigate('personalEditProfile', {
            selectedinterest: this.state.data.filter((item) => {
                if (item.isChecked) {
                    return item;
                }
            }),
        });

    };

    onSelectInterest = (item, index) => {
        const {data} = this.state;
        const {navigation, route} = this.props;
        data.map((item1, index1) => {
            if (index === index1) {
                item1.isChecked = !item1.isChecked;
            }
        });

        this.setState({data: data});
        /* route.params.onGoBack(item, 'Category');
         navigation.goBack();*!/*/

    };


    render() {
        const {navigation} = this.props;
        const {data, image} = this.state;
        return (
            <SafeAreaViewContainer>
                <TopHeader
                    rightComponentNotVisible={true}
                    text={'Choose Interests'}
                    onPressBackArrow={() => navigation.goBack()}/>

                <View style={{flex: 1, alignItems: 'center', backgroundColor: COLORS.backGround_color}}>
                    <Spacer space={4}/>
                    <Image style={{height: hp(20), width: wp(40)}}
                           source={{uri: image}} resizeMode={'contain'}/>
                    <View style={{
                        flexWrap: 'wrap',
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: wp(85),


                    }}>
                        {data.map((item, index) => {
                                console.warn(item);
                                return (
                                    <Chip
                                        onPress={() => this.onSelectInterest(item, index)}

                                        style={{
                                            margin: wp(1),
                                            backgroundColor: item.isChecked ? COLORS.app_theme_color : COLORS.button_Light_color,
                                            paddingHorizontal: 2,
                                        }}
                                        textStyle={{color: item.isChecked ? COLORS.white_color : COLORS.black_color}}


                                    >
                                        {item.categoryName}</Chip>
                                );
                            },
                        )}
                    </View>


                    <Spacer space={6}/>

                    <PrimaryButton
                        onPress={() => this.onEmailEditingEnd()}
                        //isDisabled={!areAllValuesValid}
                        //bgColor={areAllValuesValid ? COLORS.app_theme_color : COLORS.app_theme_color_disabled}
                        bgColor={COLORS.app_theme_color}
                        text={STRINGS.SAVE_TEXT}/>
                </View>

                <Spacer space={2}/>

            </SafeAreaViewContainer>
        );
    }
}


export default ChooseInterests;
