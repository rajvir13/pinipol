import React from 'react';

import {Image, View, SafeAreaView, Text, TextInput, Alert} from 'react-native';
import BaseClass from '../../utils/BaseClass';
import COLORS from '../../utils/Colors';

import {MainContainer, SafeAreaViewContainer, ScrollContainer, ShadowViewContainer} from '../../utils/BaseStyle';

import TopHeader from '../../customComponents/Header';

import TopTab from './popularHome/topTabBar';

export default class PopularPoll extends BaseClass {
    constructor(props) {
        super(props);
        const {route} = this.props;
        this.state = {
            isLogOut: false,
            authToken: '',
            searchText: '',
        };
    }

    render() {
        const {navigation} = this.props;
        const {searchText} = this.state;
        return (
            <SafeAreaViewContainer>
                {/*<TopHeader isLogout={true} onPressBackArrow={() => this.logoutUser()}/>*/}
                <TopHeader
                           onPressBackArrow={() => navigation.goBack()}
                           //onRightArrow={() => this.editProfile()}
                           isRight={true}
                           text={'Popular Polls'}
                           image={'https://www.cornwallbusinessawards.co.uk/wp-content/uploads/2017/11/dummy450x450-300x300.jpg'}
                         //  isRight={true}
                           isSearchIcon={true}
                           onRightArrow={() => alert("Coming Soon")}
                />
                <View style={{backgroundColor: COLORS.backGround_color, flex: 1}}>
                    <TopTab props={this.props}/>
                </View>
            </SafeAreaViewContainer>

        );
    }
}

