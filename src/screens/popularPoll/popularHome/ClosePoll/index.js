import React from 'react';
import {FlatList, View, Image, Text} from 'react-native';
import BaseClass from '../../../../utils/BaseClass';
import {MainContainer, ShadowViewContainer} from '../../../../utils/BaseStyle';
/*import {PrimaryButton} from '../../../../customComponents/ButtonComponent';*/
import COLORS from '../../../../utils/Colors';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {FONT_FAMILY} from '../../../../utils/Font';
import {FONT} from '../../../../utils/FontSizes';
import {Spacer} from '../../../../customComponents/Spacer';
/*import {deeppink, lightgoldenrodyellow, lightpink, yellowgreen} from 'color-name';*/

import HomeCard from '../../../commonComponents/HomeCard';
import {ICONS} from '../../../../utils/ImagePaths';
import AsyncStorage from '@react-native-community/async-storage';
import STRINGS from '../../../../utils/Strings';
import { GetUserPollAction} from '../../../../redux/actions/GetUserPolls';
import {CommonActions} from "@react-navigation/native"


class ClosePolls extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            optionArr: [{image: '', title: 'tea'}, {image: '', title: 'coffee'}],
            authToken: '',
        };
    }

    // componentDidMount() {
    //     AsyncStorage.getItem(STRINGS.accessToken, async (error, result) => {
    //         if (result !== undefined && result !== null) {
    //             let loginData = JSON.parse(result);
    //             await this.setState({
    //                 authToken: loginData,
    //             });
    //             GetUserPollAction({accessToken: loginData}, data => this.getUserPolls(data));
    //         }
    //     });
    // }

    componentDidMount() {
        AsyncStorage.getItem(STRINGS.accessToken, async (error, result) => {
            if (result !== undefined && result !== null) {
                let token = JSON.parse(result);
                await this.setState({
                    authToken: token,
                });
                GetUserPollAction({accessToken: token}, data => this.getUserPolls(data));
                AsyncStorage.getItem(STRINGS.loginData, async (error, result) => {
                    if (result !== undefined && result !== null) {
                        let loginData = JSON.parse(result);
                        await this.setState({
                            account_type_id: loginData.account_type,

                        });
                    }
                });
            }
        });
    }

    getUserPolls = (response) => {
        const {navigation} = this.props;
        const {navigate} = navigation;
        const {from} = this.state;
        if (response !== 'Network request failed') {
            this.hideDialog();
            const {code, message, data} = response;
            if (code === '200') {
                this.hideDialog();

                if(data.length !== 0){
                    this.setState({data: data});
                }
                // this.showToastSucess(message);
            } else if (code === '201') {
                this.hideDialog();
                this.showToastAlert(message);
            }else if (code === '401') {
                this.hideDialog();
                AsyncStorage.removeItem(STRINGS.accessToken);
                AsyncStorage.removeItem(STRINGS.loginData);
                AsyncStorage.removeItem(STRINGS.userPersonalInfo);
                navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [
                            {name: 'login'},
                        ],
                    }),
                );
                this.showToastAlert(message);
            }
        } else {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET);
        }
    };

    //============================= pop up Menu handling ========================


    render() {
        const {data, optionArr,account_type_id} = this.state;
        const {navigation} = this.props;
        const {navigate} = navigation;
        return (
            <MainContainer style={{backgroundColor: COLORS.backGround_color,justifyContent:'center'}}>

                <Text style={{color:COLORS.black_color,fontSize:FONT.TextMedium}}>Coming Soon</Text>

            </MainContainer>
        );
    }
}

export default ClosePolls;

