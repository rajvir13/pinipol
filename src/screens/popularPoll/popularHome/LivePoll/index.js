import React from 'react';

import BaseClass from '../../../../utils/BaseClass';
import {MainContainer} from '../../../../utils/BaseStyle';
import STRINGS from '../../../../utils/Strings';
import {Image, View, Text, FlatList} from 'react-native';
import {MostPopularPollAction} from '../../../../redux/actions/GetUserPolls';
import COLORS from '../../../../utils/Colors';
import {FONT} from '../../../../utils/FontSizes';
import {FONT_FAMILY} from '../../../../utils/Font';
import PopularCard from '../../../commonComponents/PopularCard';
import {ICONS} from '../../../../utils/ImagePaths';

import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
import HomeCard from '../../../commonComponents/HomeCard';
import PopularPollModal from '../../../../customComponents/Modals/EmailModal/PopularPollImage';
import {CommonActions} from "@react-navigation/native"

class LivePolls extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            optionArr: [{image: '', title: 'tea'}, {image: '', title: 'coffee'}],
            authToken: '',
            account_type_id: 0,
            isVisible: false,
            modalValues: '',
        };
    }

    //
    // componentDidMount() {
    //     AsyncStorage.getItem(STRINGS.accessToken, async (error, result) => {
    //         if (result !== undefined && result !== null) {
    //             let loginData = JSON.parse(result);
    //             await this.setState({
    //                 authToken: loginData,
    //             });
    //         }
    //     });
    // }

    componentDidMount() {
        AsyncStorage.getItem(STRINGS.accessToken, async (error, result) => {
            if (result !== undefined && result !== null) {
                let token = JSON.parse(result);
                await this.setState({
                    authToken: token,
                });
                MostPopularPollAction({accessToken: token}, data => this.getUserPolls(data));
                AsyncStorage.getItem(STRINGS.loginData, async (error, result) => {
                    if (result !== undefined && result !== null) {
                        let loginData = JSON.parse(result);
                        await this.setState({
                            account_type_id: loginData.account_type,

                        });
                    }
                });
            }
        });
    }

    getUserPolls = (response) => {
        const {navigation} = this.props;
        const {navigate} = navigation;
        const {from} = this.state;
        if (response !== 'Network request failed') {
            this.hideDialog();
            const {code, message, data} = response;
            if (code === '200') {

                this.hideDialog();
                if (data.length !== 0) {
                    this.setState({data: data});
                }
                // this.showToastSucess(message);
            } else if (code === '201') {
                this.hideDialog();
                this.showToastAlert(message);
            }else if (code === '401') {
                this.hideDialog();
                AsyncStorage.removeItem(STRINGS.accessToken);
                AsyncStorage.removeItem(STRINGS.loginData);
                AsyncStorage.removeItem(STRINGS.userPersonalInfo);
                navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [
                            {name: 'login'},
                        ],
                    }),
                );
                this.showToastAlert(message);
            }
        } else {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET);
        }
    };

    onImagePress = (opItem, index) => {
        const {navigation} = this.props;
        this.setState({
            isVisible: true,
            modalValues: opItem,
        });
    };

    crossPress = () => {
        const {navigation} = this.props;
        this.setState({
            isVisible: false,
        });

    };

    //============================= pop up Menu handling ========================


    render() {
        const {data, optionArr, account_type_id,isVisible, modalValues} = this.state;
        const {navigation} = this.props;
        const {navigate} = navigation;

        return (
            <MainContainer>
                {data.length === 0 ?
                    <View style={{alignItems: 'center', marginTop: wp(5)}}>
                        <Text style={{
                            color: COLORS.off_black,
                            fontSize: FONT.TextMedium,
                            fontFamily: FONT_FAMILY.PoppinsSemiBold,
                        }}>No Data</Text>
                    </View>
                    : <FlatList
                        data={data}
                        keyExtractor={(item, index) => index}
                        extraData={this.state}
                        renderItem={({item, index}) =>
                            <PopularCard index={index}
                                         item={item}
                                         optionArr={item.option}
                                         onMenuItemPress={() => this.showToastAlert(STRINGS.COMING_SOON)}
                                         onImageClick={(opItem, opIndex) => this.onImagePress(opItem, opIndex)}
                            />
                        }
                    />}
                <PopularPollModal
                    image={modalValues}
                    crossClick={() => this.crossPress()}
                    isVisible={ isVisible}
                />
            </MainContainer>
        );
    }
}

export default LivePolls;
