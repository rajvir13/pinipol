import React from 'react';
import {
    View,
    Text,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Keyboard,
    TouchableOpacity,
    Image, FlatList, TextInput, Platform, ScrollView,
} from 'react-native';
import BaseClass from '../../../utils/BaseClass';
import {SafeAreaViewContainer, MainContainer, ScrollContainer} from '../../../utils/BaseStyle';
import TopHeader from '../../../customComponents/Header';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {FONT} from '../../../utils/FontSizes';
import {FONT_FAMILY} from '../../../utils/Font';
import {Spacer} from '../../../customComponents/Spacer';
import {Input} from 'react-native-elements';
import STRINGS from '../../../utils/Strings';
import COLORS from '../../../utils/Colors';
import Autocomplete from '../../../customComponents/ModalDropdown/AutoComplete';
import {CityAction} from '../../../redux/actions/DropDownACtions';
import ChooseImageButton from '../../commonComponents/CreatePoll/ChooseImageButton';
import RightArrow from 'react-native-vector-icons/Entypo';
import {SecondaryButton} from '../../../customComponents/ButtonComponent';
import CrossIcon from 'react-native-vector-icons/Entypo';
import {CreatePollAction} from '../../../redux/actions/CreatePollAction';
import AsyncStorage from '@react-native-community/async-storage';
import {CategoriesAction} from '../../../redux/actions/CategoriesAction';
import OrientationLoadingOverlay from '../../../customComponents/CustomLoader';
import {CommonActions} from "@react-navigation/native"

export default class CreateAPoll extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            pollTitleLabel: '',
            pollTitle: '',
            validPollTitle: '',
            pollDescriptionLabel: '',
            validPollDescription: '',
            pollDescription: '',

            labelCategory: '',
            categoryError: '',
            isCategorySelected: true,
            selectedCategory: {},
            categoryData: {},
            imageData: [],
            settingData: {},

            optionResponse: [
                {
                    label: '',
                    placeholder: 'Option A',
                    value: '',
                    error: '',
                    image: '',
                    imageChosen: false,
                },
                {label: '', placeholder: 'Option B', value: '', error: '', image: '', imageChosen: false},
            ],
            imageIndex: 0,
            isPollSetting: false,
            areAllValuesValid: false,

            authToken: '',
        };
    }

    componentDidMount(): void {
        AsyncStorage.getItem(STRINGS.accessToken, (error, result) => {
            if (result !== undefined && result !== null) {
                let token = JSON.parse(result);
                this.setState({
                    authToken: token,
                });
            }
        });
    }


    chooseImage = (item1, index1) => {
        this.setState({imageIndex: index1});
        this.props.navigation.navigate('addPollImages', {onGoBack: (data, from) => this.refresh(data, from)});
    };


    onChange = async (text, type) => {
        let arr = [1];
        await arr.map(item => {
            switch (type) {
                case 'title':
                    if (text.trim().length === 0) {
                        this.setState({
                            pollTitleLabel: '',
                            validPollTitle: 'Poll Title can not be empty.',
                        });
                    } else if (text.length <= 50) {
                        this.setState({
                            pollTitleLabel: `Poll Title`,
                            validPollTitle: '',
                        });
                    } else {
                        this.setState({
                            validPollTitle: '',
                        });
                    }
                    this.setState({
                        pollTitle: text,
                    });
                    break;

                case 'description':
                    if (text.trim().length === 0) {
                        this.setState({
                            pollDescriptionLabel: '',
                            validPollDescription: 'Poll Description can not be empty.',
                        });
                    } else if (text.length <= 320) {
                        this.setState({
                            pollDescriptionLabel: `Poll Description`,
                            validPollDescription: '',
                        });
                    } else {
                        this.setState({
                            validPollDescription: '',
                        });
                    }
                    this.setState({
                        pollDescription: text,
                    });
                    break;

                case 'category':
                    if (Object.keys(text).length !== 0) {
                        this.setState({
                            categoryError: '',
                        });
                    } else {
                        this.setState({categoryError: 'This field can not be empty'});
                    }

                    this.setState({
                        selectedCategory: {'title': text.title},
                    });
                    break;

                case 'Option A':
                    let optionArr = this.state.optionResponse;
                    optionArr.map((item, index) => {
                        if (index === 0) {
                            if (text.path === undefined) {
                                if (text.trim().length === 0) {
                                    item.label = '';
                                    item.error = 'Option can not be empty.';
                                } else if (text.length <= 200) {
                                    item.label = `Option A`;
                                    item.error = '';
                                } else {
                                    item.error = '';
                                }
                                item.value = text;
                            } else {
                                item.image = text.path;
                                item.imageChosen = true;
                            }
                        }
                    });
                    this.setState({optionResponse: optionArr});
                    break;

                case 'Option B':
                    let optionArr2 = this.state.optionResponse;
                    optionArr2.map((item, index) => {
                        if (index === 1) {
                            if (text.path === undefined) {
                                if (text.trim().length === 0) {
                                    item.label = '';
                                    item.error = 'Option can not be empty.';
                                } else if (text.length <= 200) {
                                    item.label = `Option B`;
                                    item.error = '';
                                } else {
                                    item.error = '';
                                }
                                item.value = text;
                            } else {
                                item.image = text.path;
                                item.imageChosen = true;
                            }
                        }
                    });

                    this.setState({optionResponse: optionArr2});
                    break;
                case 'settings':
                    this.setState({isPollSetting: true});
                    break;
            }
        });
        this.checkAllValues();
    };

    checkAllValues = () => {
        const {validPollTitle, pollTitle, validPollDescription, selectedCategory, pollDescription, optionResponse, isPollSetting} = this.state;
        let res = optionResponse.map((item, index) => {
            if (item.error !== '' || item.image === '' || item.value === '') {
                return false;
            }
        });

        if (validPollTitle !== '' || pollTitle.length === 0) {
            this.setState({areAllValuesValid: false});
        } else if (validPollDescription !== '' || pollDescription.length === 0) {
            this.setState({areAllValuesValid: false});
        } else if (selectedCategory.title === undefined) {
            this.setState({areAllValuesValid: false});
        } else if (isPollSetting === false) {
            this.setState({areAllValuesValid: false});
        } else if (res[0] === false || res[1] === false) {
            this.setState({areAllValuesValid: false});
        } else {
            this.setState({areAllValuesValid: true});
        }
    };

    refresh = (item, from) => {
        const {optionResponse, imageIndex, imageData} = this.state;
        switch (from) {
            case 'Category':
                this.setState({categoryData: item});
                this.onChange(item, 'category');
                break;
            case 'Settings':
                this.setState({settingData: item});
                this.onChange(item, 'settings');
                break;
            case 'Images':
                let arr = imageData;
                arr.push(item);
                this.setState({imageData: arr});
                optionResponse.map((item1, index1) => {
                    if (index1 === imageIndex) {
                        this.onChange(item, item1.placeholder);
                    }
                });
                break;
        }
    };

    onPostPoll = () => {
        const {
            categoryData, settingData, imageData, authToken,
            pollTitle, pollDescription, optionResponse,
        } = this.state;
        let arr = [];
        imageData.map((item, index) => {
                arr.push(`data:${item.mime};base64,${item.data}`);
            },
        );

        let optionsArray = [];
        optionResponse.map((item, index) => {
            arr.map((item1, index1) => {
                if (index1 === index) {
                    optionsArray.push({
                        'name': item.value,
                        'image': item1,
                    });
                }
            });
        });

        CreatePollAction({
            poll_cat: categoryData.id,
            poll_title: pollTitle,
            poll_description: pollDescription,
            poll_type: "3",
            make_poll_as: settingData.makeAnonymous ? 1 : 2,
            created_by: 1,  // created by user=1, organization=2
            allow_votes: settingData.voteFrom,
            start_date: settingData.startDate,
            end_date: settingData.endDate,
            thus_votes: settingData.numberOfVotes,
            poll_result: settingData.pollResult,
            poll_result_validation: settingData.pollResultVisibleTo,
            options: optionsArray,
            accessToken: authToken,
            imagesArray: imageData,
        }, data => this.createPollResponse(data));

        this.showDialog();
    };

    createPollResponse = (response) => {
        const {navigation} = this.props;
        const {navigate} = navigation;
        const {from} = this.state;

        console.warn(response, from);

        if (response !== 'Network request failed') {
            this.hideDialog();
            const {code, message, data, technical_message} = response;
            const {navigation} = this.props
            if (code === '200') {
                this.hideDialog();
                this.showToastSucess(message);
                navigation.goBack();
            } else if (code === 201) {
                this.hideDialog();
                if (Object.keys(technical_message).length !== 0) {
                    if (technical_message.poll_description !== undefined) {
                        this.showToastAlert(technical_message.poll_description[0]);
                    }
                }
            } else if (code === '401') {
                this.hideDialog();
                AsyncStorage.removeItem(STRINGS.accessToken);
                AsyncStorage.removeItem(STRINGS.loginData);
                AsyncStorage.removeItem(STRINGS.userPersonalInfo);
                navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [
                            {name: 'login'},
                        ],
                    }),
                );
                this.showToastAlert(message);
            }
            else {
                this.hideDialog();
                this.showToastAlert('Option Images can not be empty.');
            }
        } else {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET);
        }
    };

    renderInputs = () => {
        const {pollTitleLabel, pollTitle, validPollTitle, pollDescriptionLabel, validPollDescription, pollDescription} = this.state;
        return (
            <View style={{alignSelf: 'center'}}>
                {pollTitleLabel.length !== 0 && <Text style={{
                    fontFamily: FONT_FAMILY.RobotoSemiBold,
                    fontSize: FONT.TextSmall_2,
                    color: COLORS.off_black,
                    marginLeft: wp(2),
                }}>{pollTitleLabel}<Text style={{color: COLORS.modal_color}}> ({pollTitle.length}/50)</Text></Text>}
                <Input
                    placeholder={'Poll Title'}
                    ref={'Title'}
                    // label={pollTitleLabel}
                    labelStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}
                    errorMessage={validPollTitle}
                    errorStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                        color: COLORS.invalid_color,
                    }}

                    value={pollTitle}
                    maxLength={50}
                    multiline={true}
                    inputStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                        lineHeight: wp(6),
                    }}
                    inputContainerStyle={{width: wp(90)}}
                    onChangeText={(text) => this.onChange(text, 'title')}
                    returnKeyType={'next'}
                    onSubmitEditing={() => {
                        this.refs.Description.focus();
                    }}
                />
                {/*<Spacer space={1}/>*/}
                {pollDescriptionLabel.length !== 0 && <Text style={{
                    fontFamily: FONT_FAMILY.RobotoSemiBold,
                    fontSize: FONT.TextSmall_2,
                    color: COLORS.off_black,
                    marginLeft: wp(2),
                }}>{pollDescriptionLabel}<Text
                    style={{color: COLORS.modal_color}}> ({pollDescription.length}/320)</Text></Text>}
                <Input
                    placeholder={'Poll Description'}
                    ref={'Description'}
                    // label={pollDescriptionLabel}
                    labelStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}
                    errorMessage={validPollDescription}
                    errorStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                        color: COLORS.invalid_color,
                    }}

                    value={pollDescription}
                    multiline={true}
                    maxLength={320}
                    inputStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                        lineHeight: wp(6),
                    }}
                    inputContainerStyle={{width: wp(90)}}
                    onChangeText={(text) => this.onChange(text, 'description')}
                    returnKeyType={'done'}
                    onSubmitEditing={() => {
                        Keyboard.dismiss();
                    }}
                />
            </View>
        );
    };

    _renderSelectCategory = () => {
        const {navigation} = this.props;
        const {navigate} = navigation;
        const {categoryError, selectedCategory} = this.state;
        return (
            <View style={{width: wp(90)}}>
                <TouchableOpacity
                    onPress={() => navigate('pollCategories', {onGoBack: (data, from) => this.refresh(data, from)})}
                    style={{
                        width: wp(90),
                        paddingVertical: wp(1),
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}>
                    <Text style={{
                        width: wp(80),
                        marginRight: wp(2),
                        fontSize: FONT.TextMedium_2,
                        fontFamily: FONT_FAMILY.PoppinsSemiBold,
                        fontStyle: 'normal',
                    }}>Select Category</Text>
                    <RightArrow name={'chevron-right'} size={wp(7)} color={COLORS.arrow_color}/>
                </TouchableOpacity>
                {Object.keys(selectedCategory).length === 0 ? <Text style={{
                        paddingTop: wp(1),
                        color: COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}>{categoryError}</Text>
                    :
                    <View style={{
                        flexDirection: 'row',
                        padding: wp(2),
                        alignItems: 'center',
                        borderBottomWidth: wp(0.2),
                        borderBottomColor: COLORS.arrow_color,

                    }}>
                        <Text style={{
                            textAlign: 'center',
                            color: COLORS.black_color,
                            fontFamily: FONT_FAMILY.Roboto,
                            fontSize: FONT.TextMedium_2,
                        }}>{selectedCategory.title}</Text>
                        <Spacer row={2}/>
                        {/*<CrossIcon name={'circle-with-cross'} size={wp(7)}*/}
                        {/*           onPress={() => this.setState({rememberData: []})}/>*/}

                    </View>
                }
            </View>
        );
    };

    _renderChooseImages = () => {
        const {optionResponse} = this.state;
        return (
            <View style={{width: wp(90)}}>
                <FlatList
                    keyboardShouldPersistTaps={'handles'}
                    keyExtractor={(item, index) => index}
                    extraData={this.state}
                    data={optionResponse}
                    numColumns={2}
                    scrollEnabled={false}

                    renderItem={({item, index}) =>
                        <>
                            <View style={{width: wp(43)}}>
                                {item.imageChosen ?
                                    <Image
                                        style={{width: wp(43), height: wp(45), borderRadius: wp(1.5)}}
                                        source={{uri: item.image}}
                                    />
                                    :
                                    <View style={{
                                        width: wp(43),
                                        height: wp(45),
                                        borderRadius: wp(1.5),
                                        backgroundColor: COLORS.image_background_color,
                                    }}/>
                                }
                                <Spacer space={2}/>
                                <ChooseImageButton
                                    onPress={() => this.chooseImage(item, index)}
                                    text={item.imageChosen ? 'Change Image' : 'Choose Image'}/>
                                <Spacer space={2}/>
                                {item.label.length !== 0 && <Text style={{
                                    fontFamily: FONT_FAMILY.RobotoSemiBold,
                                    fontSize: FONT.TextSmall_2,
                                    color: COLORS.off_black,
                                    marginLeft: wp(2),
                                }}>{item.label}<Text
                                    style={{color: COLORS.modal_color}}> ({item.value.length}/200)</Text></Text>}
                                <Input
                                    placeholder={item.placeholder}
                                    ref={'Description'}
                                    // label={item.label}
                                    labelStyle={{
                                        fontFamily: FONT_FAMILY.Roboto,
                                        fontSize: FONT.TextSmall,
                                        color: COLORS.black_color,
                                    }}
                                    errorMessage={item.error}
                                    errorStyle={{
                                        fontFamily: FONT_FAMILY.Roboto,
                                        fontSize: FONT.TextSmall_2,
                                        color: COLORS.invalid_color,
                                    }}

                                    value={item.value}
                                    multiline={true}
                                    maxLength={200}
                                    inputStyle={{
                                        fontFamily: FONT_FAMILY.Roboto,
                                        fontSize: FONT.TextMedium_2,
                                        color: COLORS.black_color,
                                        lineHeight: wp(6),
                                    }}
                                    inputContainerStyle={{width: wp(40), height: wp(15)}}
                                    onChangeText={(text) => this.onChange(text, item.placeholder)}
                                    returnKeyType={'done'}
                                    onSubmitEditing={() => {
                                        Keyboard.dismiss();
                                    }}
                                />
                            </View>
                            <Spacer row={2}/>
                        </>
                    }
                />

            </View>
        );
    };

    _renderPollSettings = () => {
        const {navigation} = this.props;
        const {navigate} = navigation;
        return (
            <TouchableOpacity
                onPress={() => navigate('pollSettings', {onGoBack: (data, from) => this.refresh(data, from)})}
                style={{
                    width: wp(90),
                    paddingVertical: wp(3),
                    flexDirection: 'row',
                    alignItems: 'center',
                }}>
                <Text style={{
                    width: wp(80),
                    marginRight: wp(2),
                    fontSize: FONT.TextMedium_2,
                    fontFamily: FONT_FAMILY.PoppinsSemiBold,
                    fontStyle: 'normal',
                }}>Poll Settings</Text>
                <RightArrow name={'chevron-right'} size={wp(7)} color={COLORS.arrow_color}/>
            </TouchableOpacity>
        );
    };

    _renderButtons = () => {
        const {areAllValuesValid} = this.state;
        const {navigation} = this.props;
        return (
            <View style={{flexDirection: 'row'}}>
                <SecondaryButton
                    givenWidth={wp(43)}
                    bgColor={COLORS.white_color}
                    text={'Cancel'}
                    textColor={COLORS.black_color}
                    onPress={() => navigation.goBack()}
                    isCancel={true}
                />
                <Spacer row={2}/>
                <SecondaryButton
                    isDisabled={!areAllValuesValid}
                    givenWidth={wp(43)}
                    bgColor={areAllValuesValid ? COLORS.app_theme_color : COLORS.app_theme_color_disabled}
                    text={'Post Poll'}
                    textColor={COLORS.white_color}
                    onPress={() => this.onPostPoll()}
                />
            </View>
        );
    };

    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={STRINGS.LOADING_TEXT}/>
        );
    };

    render() {
        const {navigation} = this.props;
        return (
            <SafeAreaViewContainer>
                <TopHeader onPressBackArrow={() => navigation.goBack()}
                           text={STRINGS.CREATE_POLL}/>
                <KeyboardAvoidingView
                    style={{flex: 1, backgroundColor: COLORS.backGround_color}}
                    behavior={(Platform.OS === 'ios') ? 'padding' : null}>
                    <TouchableWithoutFeedback
                        onPress={() => {
                            //     Keyboard.dismiss();
                        }}
                    >
                        <ScrollContainer>
                            <MainContainer>
                                <Text style={{
                                    alignSelf: 'flex-start',
                                    fontSize: FONT.TextMedium_2,
                                    fontFamily: FONT_FAMILY.Poppins,
                                    fontWeight: 'bold',
                                    padding: wp(5),
                                }}>Question</Text>
                                {this.renderInputs()}
                                {/*<Spacer space={0.5}/>*/}
                                {this._renderSelectCategory()}
                                <Spacer space={3}/>
                                <View style={{backgroundColor: COLORS.arrow_color, width: wp(100), height: wp(0.2)}}/>
                                <Spacer space={3}/>
                                <Text style={{
                                    alignSelf: 'flex-start',
                                    fontSize: FONT.TextMedium_2,
                                    fontFamily: FONT_FAMILY.Poppins,
                                    fontWeight: 'bold',
                                    paddingHorizontal: wp(5),
                                }}>Options/Choices</Text>
                                <Spacer space={2}/>
                                {this._renderChooseImages()}
                                <Spacer space={1}/>
                                {this._renderPollSettings()}
                                <Spacer space={4}/>
                                {this._renderButtons()}
                                <Spacer space={5}/>
                            </MainContainer>
                            {this._renderCustomLoader()}
                        </ScrollContainer>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </SafeAreaViewContainer>
        );
    }
}
