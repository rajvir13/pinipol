import React from 'react';
import {FlatList, View, Image, Text, TouchableOpacity} from 'react-native';
import BaseClass from '../../../../utils/BaseClass';
import {MainContainer, ScrollContainer, ShadowViewContainer} from '../../../../utils/BaseStyle';
import {PrimaryButton} from '../../../../customComponents/ButtonComponent';
import COLORS from '../../../../utils/Colors';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {FONT_FAMILY} from '../../../../utils/Font';
import {FONT} from '../../../../utils/FontSizes';
import {Spacer} from '../../../../customComponents/Spacer';
import Menu, {MenuItem} from 'react-native-material-menu';
import RightArrow from 'react-native-vector-icons/Entypo';
import HomeCard from '../../../commonComponents/HomeCard';
import FBIconSVG from '../../../../customComponents/imagesComponents/FBIcon';
import {ICONS} from '../../../../utils/ImagePaths';
import AsyncStorage from '@react-native-community/async-storage';
import STRINGS from '../../../../utils/Strings';
import {GetAllPollAction} from '../../../../redux/actions/GetUserPolls';
import {VoteOnPollAction} from '../../../../redux/actions/VoteOnAPollAction';
import propTypes from 'prop-types';
import OrientationLoadingOverlay from '../../../../customComponents/CustomLoader';
import PollImageModal from '../../../../customComponents/Modals/EmailModal/PollImageModal';
import {CommonActions} from "@react-navigation/native"

let checkVar = false;

class GlobalPolls extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            optionArr: [{image: '', title: 'tea'}, {image: '', title: 'coffee'}],
            authToken: '',
            account_type_id: 0,
            isVisible: false,
            modalValues: '',
        };
    }

    static propTypes = {
        routeKey: propTypes.number,
    };

    componentDidMount() {
        this.hitApi();
        const {navigation} = this.props;
        this._unsubscribe = navigation.addListener('focus', () => {
            this.hitApi();
        });
    }

    componentDidUpdate(prevProps, preState, snapShot) {
        if (this.props.routeKey === 1) {
            if (checkVar === true) {
                checkVar = false;
                this.hitApi();
            }
        } else if (this.props.routeKey === 0) {
            checkVar = true;
        }
    }


    componentWillUnmount() {
        this._unsubscribe();
    }

    hitApi = () => {
        console.warn('hit api 2');
        AsyncStorage.getItem(STRINGS.accessToken, async (error, result) => {
            if (result !== undefined && result !== null) {
                let token = JSON.parse(result);
                await this.setState({
                    authToken: token,
                });
                GetAllPollAction({accessToken: token}, data => this.getAllUserPolls(data));
                AsyncStorage.getItem(STRINGS.loginData, async (error, result) => {
                    if (result !== undefined && result !== null) {
                        let loginData = JSON.parse(result);
                        await this.setState({
                            account_type_id: loginData.account_type,

                        });
                    }
                });
                // this.showDialog();
            }
        });
    };

    getAllUserPolls = (response) => {
        const {navigation} = this.props;
        const {navigate} = navigation;
        const {from} = this.state;
        if (response !== 'Network request failed') {
            this.hideDialog();
            const {code, message, data} = response;
            if (code === '200') {
                this.hideDialog();
                this.setState({data: data});
            } else if (code === '201') {
                this.hideDialog();
                this.showToastAlert(message);
            }
            else if (code === '401') {
                this.hideDialog();
                AsyncStorage.removeItem(STRINGS.accessToken);
                AsyncStorage.removeItem(STRINGS.loginData);
                AsyncStorage.removeItem(STRINGS.userPersonalInfo);
                navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [
                            {name: 'login'},
                        ],
                    }),
                );
                this.showToastAlert(message);
            }
        } else {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET);
        }
    };

    onOptionPress = (opItem, opIndex) => {
        const {authToken} = this.state;
        this.showDialog();
        VoteOnPollAction({
            'poll_id': opItem.poll_id,
            'option_id': opItem.id,
            anonymously: 0,
            accessToken: authToken,
        }, data => this.voteOnPollAction(data));
    };

    onImagePress = (opItem, index) => {
        const {navigation} = this.props;
        this.setState({
            isVisible: true,
            modalValues: opItem,
        });
    };

    crossPress = () => {
        const {navigation} = this.props;
        this.setState({
            isVisible: false,
        });

    };

    voteOnPollAction = (response) => {
        const {navigation} = this.props;
        const {navigate} = navigation;
        const {authToken} = this.state;
        if (response !== 'Network request failed') {
            this.hideDialog();
            const {code, message, data} = response;
            if (code === '200') {
                this.hideDialog();
                this.showToastSucess(message);
                GetAllPollAction({accessToken: authToken}, data => this.getAllUserPolls(data));
            } else if (code === '201') {
                this.hideDialog();
                this.showToastAlert(message);
            }
        } else {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET);
        }
    };

    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={STRINGS.LOADING_TEXT}/>
        );
    };

    //============================= pop up Menu handling ========================


    render() {
        const {data, optionArr, account_type_id, isVisible, modalValues} = this.state;
        const {navigation} = this.props;
        const {navigate} = navigation;

        return (
            <MainContainer>
                {data.length === 0 ?
                    <View style={{alignItems: 'center', marginTop: wp(5)}}>
                        <Text style={{
                            color: COLORS.off_black,
                            fontSize: FONT.TextMedium,
                            fontFamily: FONT_FAMILY.PoppinsSemiBold,
                        }}>No Data</Text>
                    </View>
                    : <FlatList
                        data={data}
                        keyExtractor={(item, index) => index}
                        extraData={this.state}
                        renderItem={({item, index}) =>
                            <HomeCard index={index}
                                      item={item}
                                      optionArr={item.option_value}
                                      onOptionClick={(opItem, opIndex) => this.onOptionPress(opItem, opIndex)}
                                      onMenuItemPress={() => this.showToastAlert(STRINGS.COMING_SOON)}
                                      onImageClick={(opItem, opIndex) => this.onImagePress(opItem, opIndex)}
                            />
                        }
                    />}
                <View onTouchEnd={() => account_type_id === '1' ? navigate('pCreatePoll') : navigate('orgCreatePoll')}
                      style={{position: 'absolute', bottom: wp(5), right: wp(5)}}>
                    <Image style={{width: wp(15), height: wp(15)}} source={ICONS.PLUS_ICON}/>
                </View>
                <PollImageModal
                    image={modalValues}
                    crossClick={() => this.crossPress()}
                    isVisible={isVisible}
                />
                {this._renderCustomLoader()}
            </MainContainer>
        );
    }
}

export default GlobalPolls;
