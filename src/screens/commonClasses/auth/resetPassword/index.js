import React from 'react';

import {
    Image,
    View,
    SafeAreaView,
    Text,
    TextInput,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Keyboard, ScrollView,
} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {ICONS} from '../../../../utils/ImagePaths';
import BaseClass from '../../../../utils/BaseClass';
import {FONT_FAMILY} from '../../../../utils/Font';
import {FONT} from '../../../../utils/FontSizes';
import COLORS from '../../../../utils/Colors';
import STRINGS from '../../../../utils/Strings';
import {MainContainer, SafeAreaViewContainer} from '../../../../utils/BaseStyle';
import styles from './styles' ;
import {Spacer} from '../../../../customComponents/Spacer';
import {PrimaryButton} from '../../../../customComponents/ButtonComponent';
import EmailModal from '../../../../customComponents/Modals/EmailModal';
import {Input} from 'react-native-elements';
import {validatePassword} from '../../../../utils/Validations';
import {ResetPasswordAction} from '../../../../redux/actions/ResetPasswordAction';
import {CommonActions} from '@react-navigation/native';
import OrientationLoadingOverlay from '../../../../customComponents/CustomLoader';

class ResetPassword extends BaseClass {
    constructor(props) {
        super(props);
        const {route} = this.props;
        const {pin} = route.params;
        this.state = {
            isVisible: false,

            validPassword: '',
            labelPassword: '',
            password: '',

            validCPassword: '',
            labelCPassword: '',
            confirmPassword: '',
            pin: pin,

            areAllValuesValid: false,
            isLoading:false
        };
    }


    _confirmPassword = () => {
        const {pin, password} = this.state;
        ResetPasswordAction(
            {
                otp: pin,
                password: password,
            },
            data => this.resetPassResponse(data),
            this.showDialog()
        );
    };

    resetPassResponse = (response) => {
        if (response !== 'Network request failed') {
            this.hideDialog();
            const {code, message, data} = response;
            if (code === '200') {
                this.hideDialog();
                this.showToastSucess(message);
                this.setState({
                    isVisible: true,
                });
            } else if (code === '201') {
                this.hideDialog();
                this.showToastAlert(message);
            }
        } else {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET);
        }
    };


    _goBackToLogin = () => {
        const {navigation} = this.props;
        navigation.dispatch(
            CommonActions.reset({
                index: 0,
                routes: [{name: 'login'}],
            }),
        );
    };
    accept = () => {
        const {navigation} = this.props;
        this.setState({
            isVisible: false,
        });
        navigation.dispatch(
            CommonActions.reset({
                index: 0,
                routes: [{name: 'login'}],
            }),
        );

    };

    onChange = async (text, type) => {
        const {password} = this.state;
        let arr = [1];
        await arr.map(item => {
            switch (type) {
                case 'password':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelPassword: '',
                            confirmPassword: '',
                        });
                    } else if (!validatePassword(text)) {
                        this.setState({
                            validPassword: STRINGS.enter_valid_password,
                            labelPassword: 'New Password',
                        });
                    } else {
                        this.setState({
                            validPassword: '',
                        });
                    }
                    this.setState({password: text});
                    break;

                case 'confirmPassword':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelCPassword: '',
                        });
                    } else if (password !== text) {
                        this.setState({
                            validCPassword: STRINGS.password_does_not_match,
                            labelCPassword: 'Confirm New Password',
                        });
                    } else {
                        this.setState({
                            validCPassword: '',
                        });
                    }
                    this.setState({confirmPassword: text});
                    break;

                default:
                    break;

            }
        });
        this.checkAllValues();
    };

    checkAllValues = () => {
        const {validPassword, validCPassword, password, confirmPassword} = this.state;
        if (validPassword !== '' || password.length === 0) {
            this.setState({areAllValuesValid: false});
        } else if (validCPassword !== '' || confirmPassword.length === 0) {
            this.setState({areAllValuesValid: false});
        } else {
            this.setState({areAllValuesValid: true});
        }
    };

    _renderInput = () => {
        const {labelPassword, validPassword, password, labelCPassword, validCPassword, confirmPassword} = this.state;
        return (
            <View style={{justifyContent: 'center'}}>
                <Input
                    placeholder='New Password'
                    ref={"Password"}
                    label={labelPassword}
                    secureTextEntry={true}
                    labelStyle={styles.input_label_style}
                    errorStyle={styles.input_error_style}
                    errorMessage={validPassword}
                    inputStyle={styles.input_input_container}
                    inputContainerStyle={{width: wp(90)}}
                    value={password}
                    onChangeText={(text) => this.onChange(text, 'password')}
                    onSubmitEditing={()=> this.refs.CPassword.focus()}
                    returnKeyType={'next'}
                />
                <Spacer space={1}/>
                <Input
                    placeholder='Confirm New Password'
                    ref={"CPassword"}
                    label={labelCPassword}
                    secureTextEntry={true}
                    labelStyle={styles.input_label_style}
                    errorStyle={styles.input_error_style}
                    errorMessage={validCPassword}
                    inputStyle={styles.input_input_container}
                    inputContainerStyle={{width: wp(90)}}
                    value={confirmPassword}
                    onChangeText={(text) => this.onChange(text, 'confirmPassword')}
                    returnKeyType={'done'}
                />
            </View>
        );
    };
    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={STRINGS.LOADING_TEXT}/>
        );
    };

    render() {
        const {isVisible, areAllValuesValid} = this.state;
        return (
            <SafeAreaViewContainer>
                <KeyboardAvoidingView
                    style={styles.keyboard_style}
                    behavior={(Platform.OS === 'ios') ? 'padding' : null}>
                    <TouchableWithoutFeedback
                        onPress={() => {
                            Keyboard.dismiss();
                        }}>
                        <ScrollView
                            bounces={false}
                            contentContainerStyle={{
                                flexGrow: 1,
                                justifyContent: 'space-between',
                                flexDirection: 'column',
                            }}>
                            <MainContainer style={{flex: 1, alignItems: 'center'}}>
                                <Image style={{ height: wp(50), width: wp(50)}}
                                       source={ICONS.LOGO} resizeMode={'contain'}/>
                                <Text style={{
                                    fontFamily: FONT_FAMILY.PoppinsBold,
                                    fontSize: FONT.TextNormal,
                                    fontStyle: 'normal',
                                    color: COLORS.app_theme_color,
                                    alignItems: 'center',
                                    textAlign: 'center',
                                }}>{STRINGS.forgot_header_title}</Text>
                                <Spacer space={0.7}/>
                                <View style={{width: wp(85)}}>
                                    <Text style={{
                                        fontFamily: FONT_FAMILY.Roboto,
                                        fontStyle: 'normal',
                                        fontWeight: 'normal',
                                        fontSize: FONT.TextSmall,
                                        textAlign: 'center',
                                        color: COLORS.off_black,
                                    }}>{STRINGS.create_Password}</Text>
                                </View>
                                <Spacer space={10}/>
                                {this._renderInput()}
                                <Spacer space={4}/>
                                <PrimaryButton
                                    isDisabled={!areAllValuesValid}
                                    bgColor={areAllValuesValid ? COLORS.app_theme_color : COLORS.app_theme_color_disabled}
                                    onPress={() => this._confirmPassword()}
                                    text={STRINGS.confirm_Text}/>

                                <View style={styles.back_view}>
                                    <Text
                                        onPress={() => this._goBackToLogin()}
                                        style={styles.back_to_login_text}>{STRINGS.back_to_login}</Text>
                                </View>

                            </MainContainer>
                            <EmailModal
                                // decline={() => this.decline()}
                                accept={() => this.accept()}
                                isVisible={isVisible}
                            />
                            {this._renderCustomLoader()}
                        </ScrollView>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </SafeAreaViewContainer>
        );
    }
}

export default ResetPassword;
