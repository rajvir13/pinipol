import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {StyleSheet} from 'react-native';
import COLORS from '../../../../utils/Colors';
import {FONT_FAMILY} from '../../../../utils/Font';
import {FONT} from '../../../../utils/FontSizes';


const styles = StyleSheet.create({
    keyboard_style: {
        flex: 1,
        backgroundColor: COLORS.white_color,
    },
    logo_style: {
        paddingVertical: wp(25),
    },
    text_style: {
        fontFamily: FONT_FAMILY.PoppinsBold,
        fontSize: FONT.TextNormal,
        fontStyle: 'normal',
        lineHeight: wp(10),
        color: COLORS.app_theme_color,
        alignItems: 'center',
        textAlign: 'center',
        width: wp(80),
    },
    input_label_style: {
        fontFamily: FONT_FAMILY.Roboto,
        fontSize: FONT.TextSmall,
        color: COLORS.black_color,
    },
    input_error_style: {
        color: COLORS.invalid_color,
        fontFamily: FONT_FAMILY.Roboto,
        fontSize: FONT.TextSmall_2,
    },
    input_input_container: {
        fontFamily: FONT_FAMILY.Roboto,
        fontSize: FONT.TextMedium_2,
        color: COLORS.black_color,
    },
    back_view: {
        flex:1,
        justifyContent: 'flex-end',
        marginBottom: wp(10),
    },
    back_to_login_text: {
        fontFamily: FONT_FAMILY.Poppins,
        fontSize: FONT.TextSmall_2,
        color: COLORS.app_theme_color,
        fontWeight: 'bold',
    },
});

export default styles;
