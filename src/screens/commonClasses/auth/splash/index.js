import React, {Component} from 'react';
import {Image, TouchableOpacity, View, Text, NativeModules} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {CommonActions} from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import STRINGS from '../../../../utils/Strings';

class Splash extends Component {
    constructor(props) {
        super(props);
        this.state = {
            token: '',
        };
    }

    componentDidMount() {
        const {navigation} = this.props;
        AsyncStorage.getItem(STRINGS.accessToken, (error, result) => {
            if (result !== undefined && result !== null) {
                let loginData = JSON.parse(result);
                setTimeout(() => {
                    navigation.dispatch(
                        CommonActions.reset({
                            index: 0,
                            routes: [
                                {
                                    name: 'personalHome',
                                    params: {authToken: loginData}
                                },

                            ],
                        }),
                    );
                }, 4900);
            } else {
                setTimeout(() => {
                    navigation.dispatch(
                        CommonActions.reset({
                            index: 0,
                            routes: [
                                {name: 'login'},
                            ],
                        }),
                    );
                }, 4900);
            }
        });
    }


    render() {
        return (
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Image
                    resizeMode='contain'
                    style={{height: wp(80), width: wp(90)}}
                    source={require('../../../../assets/images/Splash.gif')}
                />
            </View>
        );
    }
}

export default Splash;
