import React from 'react';

import {
    Image,
    View,
    SafeAreaView,
    Text,
    TextInput,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Keyboard, ScrollView,
} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {ICONS} from '../../../../utils/ImagePaths';
import BaseClass from '../../../../utils/BaseClass';
import {FONT_FAMILY} from '../../../../utils/Font';
import {FONT} from '../../../../utils/FontSizes';
import COLORS from '../../../../utils/Colors';
import STRINGS from '../../../../utils/Strings';
import {MainContainer, SafeAreaViewContainer} from '../../../../utils/BaseStyle';

import {Spacer} from '../../../../customComponents/Spacer';
import {PrimaryButton} from '../../../../customComponents/ButtonComponent';
import {Input} from 'react-native-elements';
import styles from './ForgotStyle';
import {validateEmail} from '../../../../utils/Validations';
import {ForgotPasswordAction} from '../../../../redux/actions/ForgotPasswordAction';
import OrientationLoadingOverlay from '../../../../customComponents/CustomLoader';
import WrongEmailModal from '../../../../customComponents/Modals/WrongEmailModal';


class ForgotPassword extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            validUsername: '',
            labelUsername: '',
            username: '',
            areAllValuesValid: false,
            isLoading: false,
            isVisible: false,
        };
    }


    _sendEmail = () => {
        this.showDialog();
        ForgotPasswordAction(
            {username: this.state.username},
            data => this.forgotResponse(data));
        // this.hideDialog()
    };


    forgotResponse(response) {
        const {navigation} = this.props;
        const {navigate} = navigation;
        this.hideDialog();

        if (response !== 'Network request failed') {
            this.hideDialog();
            const {code, message, data, technical_message} = response;
            if (code === '200') {
                this.setState({
                    isVisible: false,
                });
                this.hideDialog();
                this.showToastSucess(message);
                navigate('enterPin',
                    {
                        username: this.state.username,
                        from: 'Forgot',
                    });
            } else if (code === '201') {
                setTimeout(() =>
                    this.setState({
                        isVisible: true,
                    }), 1000);

                // this.hideDialog();
                // this.showToastAlert('Email does not exist.');
            } else if (code === '422') {
                this.hideDialog();
                this.setState({
                    isVisible: false,
                });
                this.showToastAlert(technical_message.social_id[0]);
            }
        } else {
            this.setState({
                isVisible: false,
            });
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET);
        }

    }

    onChange = async (text) => {
        let arr = [1];
        await arr.map(item => {
            if (text.trim().length === 0) {
                this.setState({
                    labelUsername: '',
                });
            } else if (text.length < 3) {
                this.setState({
                    validUsername: 'Username can not be less then 3 letters.',
                    labelUsername: 'UserName',
                });
            } else {
                this.setState({
                    validUsername: '',
                });
            }
            this.setState({username: text});
        });
        this.checkAllValues();
    };

    checkAllValues = () => {
        const {validUsername, username} = this.state;
        if (validUsername !== '' || username.length === 0) {
            this.setState({areAllValuesValid: false});
        } else {
            this.setState({areAllValuesValid: true});
        }
    };

    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={STRINGS.LOADING_TEXT}/>
        );
    };
    accept = () => {
        this.setState({
            isVisible: false,
        });
    };

    render() {
        const {validUsername, labelUsername, username, areAllValuesValid, isVisible} = this.state;
        const {navigation} = this.props;
        return (
            <SafeAreaViewContainer>
                <KeyboardAvoidingView
                    style={styles.keyboard_style}
                    behavior={(Platform.OS === 'ios') ? 'padding' : null}>
                    <TouchableWithoutFeedback
                        onPress={() => {
                            Keyboard.dismiss();
                        }}>
                        <ScrollView
                            bounces={false}
                            contentContainerStyle={{
                                flexGrow: 1,
                                justifyContent: 'space-between',
                                flexDirection: 'column',
                            }}>
                            <MainContainer>
                                <Image style={{height: wp(50), width: wp(50)}}
                                       source={ICONS.LOGO} resizeMode={'contain'}/>
                                <Text style={styles.header_text}>{STRINGS.forgot_header_title}</Text>
                                <Spacer space={2}/>
                                <Text style={styles.detail_text}>{STRINGS.forgot_password}</Text>
                                <Spacer space={10}/>
                                <View style={{justifyContent: 'center'}}>
                                    <Input
                                        placeholder='User Name'
                                        label={labelUsername}
                                        labelStyle={styles.input_label_style}
                                        errorStyle={styles.input_error_style}
                                        errorMessage={validUsername}
                                        inputStyle={styles.input_input_container}
                                        inputContainerStyle={{width: wp(90)}}
                                        value={username}
                                        onChangeText={(text) => this.onChange(text)}
                                        returnKeyType={'done'}
                                        keyboardType={'email-address'}
                                    />
                                </View>
                                <Spacer space={5}/>
                                <PrimaryButton
                                    onPress={() => this._sendEmail()}
                                    isDisabled={!areAllValuesValid}
                                    bgColor={areAllValuesValid ? COLORS.app_theme_color : COLORS.app_theme_color_disabled}
                                    text={STRINGS.send_email}/>
                                <Spacer space={3}/>

                                <View style={styles.back_view}>
                                    <Text
                                        onPress={() => navigation.goBack()}
                                        style={styles.back_to_login_text}>{STRINGS.back_to_login}</Text>
                                </View>

                            </MainContainer>

                            <WrongEmailModal
                                isVisible={this.state.isLoading ? false : isVisible}
                                accept={() => this.accept()}
                            />
                            {this._renderCustomLoader()}

                        </ScrollView>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </SafeAreaViewContainer>
        );
    }
}

export default ForgotPassword;
