import React from 'react';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {StyleSheet} from 'react-native';
import {FONT_FAMILY} from '../../../../utils/Font';
import {FONT} from '../../../../utils/FontSizes';
import COLORS from '../../../../utils/Colors';

const styles = StyleSheet.create({
    select_text: {
        fontFamily: FONT_FAMILY.PoppinsBold,
        fontSize: FONT.TextNormal,
        fontStyle: 'normal',
        lineHeight: wp(10),
        color: COLORS.app_theme_color,
        alignItems: 'center',
        textAlign: 'center',
    },
    select_text_details: {
        fontFamily: FONT_FAMILY.Roboto,
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: FONT.TextSmall,
        textAlign: 'center',
        color: COLORS.text_Color,
    },
    option_view: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: wp(90),
        height: wp(30),
        borderRadius: wp(1.5),
    },
    option_title: {
        width: wp(70),
        fontFamily: FONT_FAMILY.PoppinsBold,
        fontSize: FONT.TextNormal_2,
        fontStyle: 'normal',
        lineHeight: wp(10),
        alignItems: 'center',
    },
    // option_detail: {
    //     width: wp(70),
    //     fontFamily: FONT_FAMILY.Roboto,
    //     fontStyle: 'normal',
    //     fontWeight: 'normal',
    //     fontSize: FONT.TextSmall,
    // },
    option_detail:{
        width: wp(70),
        fontFamily: FONT_FAMILY.Roboto,
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: FONT.TextSmall,
    },
    option_detail1:{
        width: wp(70),
        fontFamily: FONT_FAMILY.RobotoBold,
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: FONT.TextSmall,
    }

});

export default styles;
