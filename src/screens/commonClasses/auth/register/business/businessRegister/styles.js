import styled from 'styled-components/native/dist/styled-components.native.esm';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLORS from '../../../../../../utils/Colors';
import {FONT} from '../../../../../../utils/FontSizes';
import {FONT_FAMILY} from '../../../../../../utils/Font';


const CreateBusinessText = styled.Text`
color: ${COLORS.app_theme_color};
fontSize: ${FONT.TextNormal};
fontFamily: ${FONT_FAMILY.PoppinsBold}; 
width: ${wp('80%')};
lineHeight:${wp(10)};
alignItems: ${'center'};
textAlign: ${'center'};
fontStyle: ${'normal'};
`;
const AlreadyHaveText = styled.Text`
color: ${COLORS.text_Color};
fontSize: ${FONT.textSmall1};
fontFamily: ${FONT_FAMILY.Roboto}; 
alignItems: ${'center'};
textAlign: ${'center'};
fontStyle: ${'normal'};
`;
const LoginText = styled.Text`
color: ${COLORS.app_theme_color};
fontSize: ${FONT.textSmall1};
fontFamily: ${FONT_FAMILY.PoppinsBold}; 
alignItems: ${'center'};
textAlign: ${'center'};
fontStyle: ${'normal'};
fontWeight: ${'bold'};
`;

export {
    CreateBusinessText,
    AlreadyHaveText,
    LoginText,
};
