import React from 'react';
import {Image, Keyboard, KeyboardAvoidingView, TouchableOpacity, TouchableWithoutFeedback, View} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

import {Input} from 'react-native-elements';
import BaseClass from '../../../../../../utils/BaseClass';
import COLORS from '../../../../../../utils/Colors';
import {ICONS} from '../../../../../../utils/ImagePaths';
import {FONT} from '../../../../../../utils/FontSizes';
import {FONT_FAMILY} from '../../../../../../utils/Font';
import {
    MainContainer,
    SafeAreaViewContainer,
    ScrollContainer,

} from '../../../../../../utils/BaseStyle';
import {
    CreateBusinessText,
    AlreadyHaveText,
    LoginText,
} from './styles';
import STRINGS from '../../../../../../utils/Strings';
import {validateEmail, validatePassword} from '../../../../../../utils/Validations';
import {Spacer} from '../../../../../../customComponents/Spacer';
import {PrimaryButton} from '../../../../../../customComponents/ButtonComponent';
import TopHeader from '../../../../../../customComponents/Header';
import {CommonActions} from '@react-navigation/native';
import {UserAvailabillityAction} from '../../../../../../redux/actions/UserAvailabilityAction';
import {VerifyEmailAction} from '../../../../../../redux/actions/VerifyEmailAction';
import OrientationLoadingOverlay from '../../../../../../customComponents/CustomLoader';


export default class BusinessRegister extends BaseClass {
    constructor(props) {
        super(props);

        const {route} = this.props;
        const {socialId, socialPlatform, socialEmail, id} = route.params;

        this.state = {
            areAllValuesValid: false,

            validUserName: '',
            labelUserName: '',
            userName: '',

            validUName: '',
            labelUName: '',
            orgName: '',

            validEmail: '',
            labelEmail: '',
            email: socialEmail,

            validPassword: '',
            labelPassword: '',
            password: '',

            validCPassword: '',
            labelCPassword: '',
            cPassword: '',

            socialId: socialId,
            socialPlatform: socialPlatform,
            socialEmail: socialEmail,
            id: id,
            isLoading: false,
        };
    }

    onUserEditingEnd = () => {
        const {userName} = this.state;
        if (userName.trim().length > 1) {
            UserAvailabillityAction({
                username: this.state.userName,
            }, data => this.isUserAvailableResponse(data));
            // } else {
            //     this.setState({
            //         validUserName: 'Enter atleast 5 characters',
            //     });
        }
    };

    isUserAvailableResponse = (response) => {
        if (response !== 'Network request failed') {
            this.hideDialog();
            const {code, message, data} = response;
            if (code === '200') {
                this.hideDialog();
                this.setState({
                    validUserName: message,
                });
            } else if (code === '403') {
                this.hideDialog();
                this.setState({
                    validUserName: 'Username Unavailable',
                });
            }
        } else {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET);
        }
    };


    onChange = async (text, type) => {
        const {password, socialId} = this.state;
        let arr = [1];
        await arr.map(item => {
            switch (type) {
                case 'userName':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelUserName: '',
                            validUserName: '',
                        });
                    } else if (text.trim().length < 3) {
                        this.setState({
                            // validUName: 'Username Unavailable',
                            labelUserName: 'Create Username',
                        });
                        // } else {
                        //     this.setState({
                        //         validUName: 'Username Available',
                        //     });
                    }
                    if (socialId !== '') {
                        this.setState({userName: socialId});
                    } else {
                        this.setState({userName: text.trim()});
                    }

                    break;
                case 'orgName':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelUName: '',
                        });
                    } else if (text.trim().length < 3) {
                        this.setState({
                            validUName: STRINGS.name_minimum_three,
                            labelUName: STRINGS.BS_ORGANISATION_NAME_PLACEHOLDER,
                        });
                    } else {
                        this.setState({
                            validUName: '',
                        });
                    }
                    this.setState({orgName: text});
                    break;
                case 'email':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelEmail: '',
                        });
                    } else if (!validateEmail(text)) {
                        this.setState({
                            validEmail: STRINGS.valid_email,
                            labelEmail: 'Email',
                        });
                    } else {
                        this.setState({
                            validEmail: '',
                        });
                    }
                    this.setState({email: text});
                    break;
                case 'password':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelPassword: '',
                            cPassword: '',
                        });
                    } else if (!validatePassword(text)) {
                        this.setState({
                            validPassword: STRINGS.enter_valid_password,
                            labelPassword: 'Create Password',
                        });
                    } else {
                        this.setState({
                            validPassword: '',
                        });
                    }
                    if (socialId !== '') {
                        this.setState({password: 'Mind@123'});
                    } else {
                        this.setState({password: text});
                    }
                    break;
                case 'confirmPassword':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelCPassword: '',
                        });
                    } else if (password !== text) {
                        this.setState({
                            validCPassword: STRINGS.password_does_not_match,
                            labelCPassword: 'Confirm Password',
                        });
                    } else {
                        this.setState({
                            validCPassword: '',
                        });
                    }
                    this.setState({cPassword: text});
                    break;

                default:
                    break;

            }
        });

        this.checkAllValues();
    };

    checkAllValues = () => {
        const {
            validUName, orgName, validEmail, email, userName, validUserName,
            validPassword, password, validCPassword, cPassword, socialId,
        } = this.state;
        if (socialId === '') {
            if (validUserName === 'Username Unavailable' || userName.length === 0) {
                this.setState({areAllValuesValid: false});
            } else if (validUName !== '' || orgName.length === 0) {
                this.setState({areAllValuesValid: false});
            } else if (validEmail !== '' || email.length === 0) {
                this.setState({areAllValuesValid: false});
            } else if (validPassword !== '' || password.length === 0) {
                this.setState({areAllValuesValid: false});
            } else if (validCPassword !== '' || cPassword.length === 0) {
                this.setState({areAllValuesValid: false});
            } else {
                this.setState({areAllValuesValid: true});
            }
        } else {
            if (validUName !== '' || orgName.length === 0) {
                this.setState({areAllValuesValid: false});
            } else if (validEmail !== '' || email.length === 0) {
                this.setState({areAllValuesValid: false});
            } else {
                this.setState({areAllValuesValid: true});
            }
        }
    };

    _nextClick = () => {

        // VerifyEmailAction({
        //     email: this.state.email,
        // }, data => this.isEmailAvailableResponse(data));
        // this.showDialog();

        const {navigation} = this.props;
        const {navigate} = navigation;
        const {id, orgName, userName, email, cPassword, password, socialId, socialPlatform, socialEmail} = this.state;

        let user_name = '';
        if (socialId !== '') {
            user_name = socialId;
        } else {
            user_name = userName;
        }
        navigate('tellAboutBusiness', {
            orgData: {
                userName: user_name,
                email: email,
                orgName: orgName,
                password: password,
                cPassword: cPassword,
                id: id,
                socialId: socialId,
                socialPlatform: socialPlatform,
                socialEmail: socialEmail,
            },
        });
    };


    // isEmailAvailableResponse = (response) => {
    //     const {navigation} = this.props;
    //     const {navigate} = navigation;
    //     const {id, orgName, userName, email, cPassword, password, socialId, socialPlatform, socialEmail} = this.state;
    //     if (response !== 'Network request failed') {
    //         this.hideDialog();
    //         const {code, message, data} = response;
    //         if (code === '200') {
    //             this.hideDialog();
    //             navigate('tellAboutBusiness', {
    //                 orgData: {
    //                     userName: userName,
    //                     email: email,
    //                     orgName: orgName,
    //                     password: password,
    //                     cPassword: cPassword,
    //                     id: id,
    //                     socialId: socialId,
    //                     socialPlatform: socialPlatform,
    //                     socialEmail: socialEmail,
    //
    //                 },
    //             });
    //         } else if (code === '403') {
    //             this.hideDialog();
    //             this.showToastAlert(message);
    //         }
    //     } else {
    //         this.hideDialog();
    //         this.showToastAlert(STRINGS.CHECK_INTERNET);
    //     }
    // };


    onPressBack = () => {
        const {navigation} = this.props;
        navigation.goBack();
    };

    _goBackToLogin = () => {
        const {navigation} = this.props;
        navigation.dispatch(
            CommonActions.reset({
                index: 0,
                routes: [{name: 'login'}],
            }),
        );
    };

    _renderInputs = () => {
        const {
            validUserName, labelUserName, userName, validUName, labelUName, orgName, validEmail, labelEmail, email,
            validPassword, labelPassword, password, validCPassword, labelCPassword, cPassword, socialId,
        } = this.state;
        return (
            <View style={{alignItems: 'center'}}>
                <Input
                    placeholder={STRINGS.BS_ORGANISATION_NAME_PLACEHOLDER}
                    ref={'Organization'}
                    label={labelUName}
                    labelStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}
                    errorStyle={{
                        color: COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}
                    errorMessage={validUName}
                    value={orgName}
                    inputStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                    }}
                    inputContainerStyle={{width: wp(90)}}
                    onChangeText={(text) => this.onChange(text, 'orgName')}
                    returnKeyType={'next'}
                    onSubmitEditing={() => {
                        this.refs.Email.focus();
                    }}
                />
                <Input
                    placeholder={STRINGS.BS_CREATE_EMAIL_ID_PLACEHOLDER}
                    ref={'Email'}
                    label={labelEmail}
                    labelStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}
                    errorStyle={{
                        color: COLORS.invalid_color, fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}
                    errorMessage={validEmail}
                    value={email}
                    inputStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                    }}
                    inputContainerStyle={{width: wp(90)}}
                    onChangeText={(text) => this.onChange(text, 'email')}
                    keyboardType={'email-address'}
                    returnKeyType={'next'}
                    onSubmitEditing={() => {
                        this.refs.Username.focus();
                    }}
                />
                <Input
                    disabled={socialId !== ''}
                    placeholder={STRINGS.USER_TEXT}
                    ref={'Username'}
                    label={labelUserName}
                    labelStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}
                    errorMessage={validUserName}
                    errorStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                        color: validUserName === 'Username available' ? COLORS.valid_Color : COLORS.invalid_color,
                    }}

                    value={userName}
                    inputStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                    }}
                    inputContainerStyle={{width: wp(90)}}
                    onChangeText={(text) => this.onChange(text, 'userName')}
                    onEndEditing={() => this.onUserEditingEnd()}
                    returnKeyType={'next'}
                    onSubmitEditing={() => {
                        this.refs.Password.focus();
                    }}
                />
                <Input
                    disabled={socialId !== ''}
                    placeholder={STRINGS.BS_CREATE_PASSWORD_PLACEHOLDER}
                    ref={'Password'}
                    label={labelPassword}
                    labelStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}
                    errorStyle={{
                        color: COLORS.invalid_color, fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}
                    errorMessage={validPassword}
                    value={password}
                    inputStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                    }}
                    inputContainerStyle={{width: wp(90)}}
                    onChangeText={(text) => this.onChange(text, 'password')}
                    secureTextEntry={true}
                    returnKeyType={'next'}
                    onSubmitEditing={() => {
                        this.refs.CPassword.focus();
                    }}
                />
                <Input
                    disabled={socialId !== ''}
                    placeholder={STRINGS.BS_CONFIRM_PASSWORD_PLACEHOLDER}
                    ref={'CPassword'}
                    label={labelCPassword}
                    labelStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}
                    errorStyle={{
                        color: COLORS.invalid_color, fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}
                    errorMessage={validCPassword}
                    value={cPassword}
                    inputStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                    }}
                    inputContainerStyle={{width: wp(90)}}
                    onChangeText={(text) => this.onChange(text, 'confirmPassword')}
                    secureTextEntry={true}
                    returnKeyType={'done'}
                />
            </View>
        );
    };

    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={STRINGS.LOADING_TEXT}/>
        );
    };


    render() {
        const {areAllValuesValid} = this.state;
        return (
            <SafeAreaViewContainer>
                <TopHeader
                    onPressBackArrow={() => this.onPressBack()}/>
                <KeyboardAvoidingView
                    style={{flex: 1, backgroundColor: COLORS.backGround_color}}
                    behavior={(Platform.OS === 'ios') ? 'padding' : null}>
                    <TouchableWithoutFeedback
                        onPress={() => {
                            Keyboard.dismiss();
                        }}>
                        <ScrollContainer>
                            <MainContainer>
                                <Image style={{height: wp(50), width: wp(50)}}
                                       source={ICONS.LOGO} resizeMode={'contain'}/>
                                <CreateBusinessText>{STRINGS.BS_CREATE_BUSINESS_TEXT}</CreateBusinessText>
                                <Spacer space={5}/>
                                {this._renderInputs()}
                                <PrimaryButton
                                    isDisabled={!areAllValuesValid}
                                    bgColor={areAllValuesValid ? COLORS.app_theme_color : COLORS.app_theme_color_disabled}
                                    onPress={() => this._nextClick()}
                                    text={STRINGS.BS_NEXT_TEXT}/>
                                <Spacer space={5}/>
                                <AlreadyHaveText>{STRINGS.BS_ALREADY_HAVE_TEXT}</AlreadyHaveText>
                                <Spacer space={2}/>
                                <LoginText onPress={() => this._goBackToLogin()}>{STRINGS.BS_LOGIN_TEXT}</LoginText>
                                <Spacer space={2}/>
                            </MainContainer>
                            {this._renderCustomLoader()}
                        </ScrollContainer>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </SafeAreaViewContainer>
        );
    }
}
