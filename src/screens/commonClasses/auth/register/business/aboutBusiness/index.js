import React from 'react';
import {
    Image,
    Keyboard,
    KeyboardAvoidingView,
    TouchableOpacity,
    Text,
    ImageBackground,
    TouchableWithoutFeedback,
    View, Platform,
} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {Input, Header, CheckBox} from 'react-native-elements';
import BaseClass from '../../../../../../utils/BaseClass';
import COLORS from '../../../../../../utils/Colors';
// import {ICONS} from '../../../../../../utils/ImagePaths';
import {FONT} from '../../../../../../utils/FontSizes';
import {FONT_FAMILY} from '../../../../../../utils/Font';
import CountryPicker from 'react-native-country-picker-modal';

import {
    CreateBusinessText,
    StartText,
    styles,
} from './styles';
import STRINGS from '../../../../../../utils/Strings';
import {Spacer} from '../../../../../../customComponents/Spacer';
import {MainContainer, SafeAreaViewContainer, ScrollContainer} from '../../../../../../utils/BaseStyle';
import {PrimaryButton} from '../../../../../../customComponents/ButtonComponent';
import TopHeader from '../../../../../../customComponents/Header';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import Autocomplete from '../../../../../../customComponents/ModalDropdown/AutoComplete';
import UnCheckSVG from '../../../../../../customComponents/imagesComponents/Uncheck';
import CheckSVG from '../../../../../../customComponents/imagesComponents/Check';
import {
    TransformDropdownData,
    TransformCountryData,
    TransformCityData,
} from '../../../../../../utils/DataTransformation';
import {
    CityAction,
    CountryAction,
    OrganizationTypeAction,
    SectorTypeAction,
} from '../../../../../../redux/actions/DropDownACtions';
import {BusinessRegisterAction} from '../../../../../../redux/actions/PersonalRegisterAction';
import {validateWebsite} from '../../../../../../utils/Validations';
import OrientationLoadingOverlay from '../../../../../../customComponents/CustomLoader';
import AsyncStorage from '@react-native-community/async-storage';

// const userCountryData = "91";

export default class AboutBusiness extends BaseClass {
    constructor(props) {
        super(props);
        const {route} = this.props;
        const {orgData} = route.params;
        const {
            email, orgName, password, cPassword,
            id, socialId, userName, socialPlatform, socialEmail,
        } = orgData;
        let userLocaleCountryCode = 'IN';
        this.state = {
            validfirstName: '',
            labelfirstName: '',
            lFirstName: '',

            validLastName: '',
            labelLastName: '',
            lLastName: '',

            validPositionName: '',
            labelPositionName: '',
            postionName: '',

            organisationType: '',
            isOrganizationType: true,
            labelOrganization: '',
            organizationResponse: [],
            orgId: 0,
            orgError: '',

            sector: '',
            isSectorType: true,
            labelSector: '',
            sectorResponse: [],
            sectorId: 0,
            sectorError: '',

            phoneNoText: '',

            streetAddress: '',
            validStreetAddress: '',
            labelStreetAddress: '',

            countryName: '',
            isCountryName: true,
            countryId: 0,
            labelCountry: '',
            countryResponse: [],
            countryError: '',

            cityResponse: [],
            cityName: '',
            isCityName: true,
            labelCity: '',
            cityId: 0,
            cityError: '',

            zipCode: '',
            validZipCode: '',
            labelZipCode: '',

            website: '',
            validWebsite: '',
            labelWebsite: '',

            isChecked: false,
            areAllValuesValid: false,

            cca2: userLocaleCountryCode,
            callingCode: '+91',
            isDropvisible: false,


            orgEmail: email,
            orgName: orgName,
            password: password,
            cPassword: password,
            userName: userName,
            socialId: socialId,
            socialPlatform: socialPlatform,
            socialEmail: socialEmail,
            id: id,

            isLoading: false,
        };
    }

    _renderBusinessContact = () => {
        const {
            validfirstName, labelfirstName, lFirstName,
            validLastName, labelLastName, lLastName,
            validPositionName, labelPositionName, postionName,
        } = this.state;
        return (
            <View style={{alignItems: 'center'}}>
                <Input
                    placeholder={STRINGS.ABOUT_BUSINESS_LEGAL_FIRST_PLACEHOLDER}
                    ref={'FirstName'}
                    label={labelfirstName}
                    labelStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}
                    errorStyle={{
                        color: COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}
                    errorMessage={validfirstName}
                    inputStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                    }}
                    inputContainerStyle={{width: wp(90)}}
                    value={lFirstName}
                    onChangeText={(text) => this.onChange(text, 'firstName')}
                    onSubmitEditing={() => this.refs.LastName.focus()}
                    returnKeyType={'next'}
                />
                <Input
                    placeholder={STRINGS.ABOUT_BUSINESS_LEGAL_LAST_PLACEHOLDER}
                    ref={'LastName'}
                    label={labelLastName}
                    labelStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}
                    errorStyle={{
                        color: COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}
                    errorMessage={validLastName}
                    inputStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                    }}
                    inputContainerStyle={{width: wp(90)}}
                    value={lLastName}
                    onChangeText={(text) => this.onChange(text, 'lastname')}
                    onSubmitEditing={() => this.refs.Position.focus()}
                    returnKeyType={'next'}
                />
                <Input
                    placeholder={STRINGS.ABOUT_BUSINESS_POSITION_PLACEHOLDER}
                    ref={'Position'}
                    label={labelPositionName}
                    labelStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}
                    errorStyle={{
                        color: validPositionName === 'Position Available' ? COLORS.valid_Color : COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}
                    errorMessage={validPositionName}
                    inputStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                    }}
                    inputContainerStyle={{width: wp(90)}}
                    value={postionName}
                    onChangeText={(text) => this.onChange(text, 'position')}
                    onSubmitEditing={() => this.refs.OrgType.focus()}
                    returnKeyType={'done'}
                />
            </View>
        );
    };
    _renderBusinessDetail = () => {
        const {
            organisationType, sector, isOrganizationType, organizationResponse, orgError,
            labelOrganization, isSectorType, sectorResponse, labelSector, sectorError,
        } = this.state;
        return (
            <>
                <View style={{
                    flex: 1,
                    // position:'absolute',
                    zIndex: 10,
                    width: wp(90),
                }}>
                    {labelOrganization.length !== 0 && <Text style={{
                        fontFamily: FONT_FAMILY.RobotoBold,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}>{STRINGS.ABOUT_BUSINESS_ORGANISATION_TYPE_PLACEHOLDER}</Text>}
                    <Autocomplete
                        placeholder={STRINGS.ABOUT_BUSINESS_ORGANISATION_TYPE_PLACEHOLDER}
                        placeholderTextColor={COLORS.placeHolder_color}
                        ref={'OrgType'}
                        // labelPlace
                        data={organizationResponse}
                        style={{
                            paddingVertical: wp(1),
                            fontFamily: FONT_FAMILY.Roboto,
                            fontSize: FONT.TextMedium_2,
                            color: COLORS.black_color,
                        }}
                        inputContainerStyle={{
                            paddingVertical: wp(2),
                            backgroundColor: COLORS.backGround_color,
                            width: wp(90),
                            borderWidth: 0,
                            borderBottomWidth: wp(0.35),
                            borderBottomColor: 'rgb(163,171,180)',
                        }}
                        listStyle={{
                            height: wp(45),
                            width: wp(90),
                            marginLeft: 0,
                        }}
                        hideResults={isOrganizationType}
                        defaultValue={organisationType}
                        onChangeText={(text) => this.onChange(text, 'org')}
                        renderItem={({item, i}) => (
                            <TouchableOpacity
                                onPress={() =>
                                    this.setState({
                                        organisationType: item.label,
                                        isOrganizationType: true,
                                        orgId: parseInt(item.value),
                                    })
                                }>
                                <Text style={{
                                    padding: wp(2), fontFamily: FONT_FAMILY.Roboto,
                                    fontSize: FONT.TextMedium_2,
                                    color: COLORS.black_color,
                                }}>{item.label}</Text>
                                <View style={{height: wp(0.2), backgroundColor: 'rgb(163,171,180)', width: wp(90)}}/>
                                <Spacer space={0.5}/>
                            </TouchableOpacity>
                        )}
                    />
                    {orgError !== '' && <Text style={{
                        paddingTop: wp(1),
                        color: COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}>{orgError}</Text>}
                </View>
                <Spacer space={2}/>
                <View style={{
                    flex: 1,
                    zIndex: 8,
                    width: wp(90),
                }}>
                    {labelSector.length !== 0 && <Text style={{
                        fontFamily: FONT_FAMILY.RobotoBold,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}>{STRINGS.ABOUT_BUSINESS_SECTOR_PLACEHOLDER}</Text>}
                    <Autocomplete
                        placeholder={STRINGS.ABOUT_BUSINESS_SECTOR_PLACEHOLDER}
                        placeholderTextColor={COLORS.placeHolder_color}
                        data={sectorResponse}
                        style={{
                            paddingVertical: wp(1),
                            fontFamily: FONT_FAMILY.Roboto,
                            fontSize: FONT.TextMedium_2,
                            color: COLORS.black_color,
                        }}
                        inputContainerStyle={{
                            paddingVertical: wp(2),
                            backgroundColor: COLORS.backGround_color,
                            width: wp(90),
                            borderWidth: 0,
                            borderBottomWidth: wp(0.35),
                            borderBottomColor: 'rgb(163,171,180)',
                        }}
                        listContainerStyle={{
                            width: wp(90),
                        }}
                        listStyle={{
                            height: wp(45),
                            width: wp(90),
                            marginLeft: 0,
                        }}
                        hideResults={isSectorType}
                        defaultValue={sector}
                        onChangeText={text => this.onChange(text, 'sector')}
                        renderItem={({item, i}) => (
                            <TouchableOpacity onPress={() => this.setState({
                                sector: item.label,
                                isSectorType: true,
                                sectorId: parseInt(item.value),
                            })}>
                                <Text style={{
                                    padding: wp(2), fontFamily: FONT_FAMILY.Roboto,
                                    fontSize: FONT.TextMedium_2,
                                    color: COLORS.black_color,
                                }}>{item.label}</Text>
                                <View style={{height: wp(0.2), backgroundColor: 'rgb(163,171,180)', width: wp(90)}}/>
                                <Spacer space={0.5}/>
                            </TouchableOpacity>
                        )}
                    />
                    {sectorError !== '' && <Text style={{
                        paddingTop: wp(1),
                        color: COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}>{sectorError}</Text>}
                </View>
            </>

        );
    };


    //===========================================================
    //===========================country code====================
    //===========================================================

    onCountrySelect = (value) => {
        this.setState({
            cca2: value.cca2,
            callingCode: `+${value.callingCode[0]}`,
        });
    };

    DropDownView = () => {
        const {isDropvisible} = this.state;
        return (
            <TouchableOpacity onPress={() => this.setState({isDropvisible: !isDropvisible})}>
                <EntypoIcon name={'chevron-down'} size={wp(5)}/>
            </TouchableOpacity>
        );
    };

    _renderCountryPicker = () => {
        return (
            <CountryPicker
                renderFlagButton={this.DropDownView}
                countryList={this.state.countryData}
                onSelect={(value) => {
                    this.onCountrySelect(value);
                }}
                hideAlphabetFilter={true}
                showCallingCode={true}
                filterPlaceholder={STRINGS.SEARCH_TEXT}
                closeable={true}
                filterable={true}
                cca2={this.state.cca2}
                translation="eng"
                autoFocusFilter={false}
                countryCode={this.state.cca2}
                withFilter={true}
                visible={this.state.isDropvisible}
                onClose={() => this.setState({isDropvisible: false})}
            />
        );
    };

    _renderBusinessPhone = () => {
        const {phoneNoText, callingCode} = this.state;
        return (
            <View style={{
                flexDirection: 'row',
            }}>
                <Input
                    inputContainerStyle={{}}
                    editable={false}
                    pointerEvents={'none'}
                    placeholder={`+${91}`}
                    // leftIcon={this._renderCountryPicker}
                    leftIconContainerStyle={{marginLeft: 0, alignItems: 'center'}}
                    value={callingCode}
                    containerStyle={{
                        width: wp('35%'),
                    }}
                    rightIcon={
                        this._renderCountryPicker
                    }
                    inputStyle={{
                        fontSize: FONT.TextMedium_2,
                        marginHorizontal: wp(1),
                        // marginTop:wp(2)
                    }}
                />
                <Spacer row={3}/>
                <Input
                    inputContainerStyle={{
                        width: wp('54%'),
                    }}
                    placeholder={STRINGS.MOBILE_NUMBER}
                    placeholderTextColor={COLORS.off_black}
                    ref="Mobile"
                    value={phoneNoText}
                    onChangeText={(text) =>
                        this.setState({
                            phoneNoText: text.trim() && text.replace(/^0+/, ''),
                        })
                    }
                    maxLength={10}
                    keyboardType="phone-pad"
                    returnKeyType={'next'}
                    inputStyle={{
                        paddingLeft: wp(0.5),
                        fontSize: FONT.TextMedium_2,
                    }}
                    containerStyle={{
                        paddingBottom: 0,
                        alignItems: 'center',
                        width: wp('50%'),
                    }}
                    rightIcon={<EntypoIcon size={wp(6)}/>}
                />
            </View>
        );
    };

    _renderCountryAndCity = () => {
        const {
            streetAddress, validStreetAddress, labelStreetAddress, countryName, cityName, countryError,
            isCountryName, isCityName, labelCountry, labelCity, countryResponse, cityResponse, cityError,
        } = this.state;
        return (
            <>
                <View style={{alignItems: 'center'}}>
                    <Input
                        inputContainerStyle={{width: wp(90)}}
                        placeholder={STRINGS.ABOUT_BUSINESS_STREET_ADDRESS_PLACEHOLDER}
                        label={labelStreetAddress}
                        labelStyle={{
                            fontFamily: FONT_FAMILY.Roboto,
                            fontSize: FONT.TextSmall,
                            color: COLORS.black_color,
                        }}
                        errorStyle={{
                            color: COLORS.invalid_color,
                            fontFamily: FONT_FAMILY.Roboto,
                            fontSize: FONT.TextSmall_2,
                        }}
                        errorMessage={validStreetAddress}
                        inputStyle={{
                            fontFamily: FONT_FAMILY.Roboto,
                            fontSize: FONT.TextMedium_2,
                            color: COLORS.black_color,
                        }}
                        value={streetAddress}
                        onChangeText={(text) => this.onChange(text, 'street')}
                    />
                </View>
                <View style={{
                    flex: 1,
                    // position:'absolute',
                    zIndex: 10,
                    width: wp(90),
                }}>
                    {labelCountry.length !== 0 && <Text style={{
                        fontFamily: FONT_FAMILY.RobotoBold,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}>{STRINGS.ABOUT_BUSINESS_COUNTRY_PLACEHOLDER}</Text>}
                    <Autocomplete
                        placeholder={STRINGS.ABOUT_BUSINESS_COUNTRY_PLACEHOLDER}
                        placeholderTextColor={COLORS.placeHolder_color}
                        data={countryResponse}
                        style={{
                            paddingVertical: wp(1),
                            fontFamily: FONT_FAMILY.Roboto,
                            fontSize: FONT.TextMedium_2,
                            color: COLORS.black_color,
                        }}
                        inputContainerStyle={{
                            paddingVertical: wp(2),
                            backgroundColor: COLORS.backGround_color,
                            width: wp(90),
                            borderWidth: 0,
                            borderBottomWidth: wp(0.35),
                            borderBottomColor: 'rgb(163,171,180)',
                        }}
                        listContainerStyle={{
                            width: wp(90),
                        }}
                        listStyle={{
                            height: wp(45),
                            width: wp(90),
                            marginLeft: 0,
                        }}
                        hideResults={isCountryName}
                        defaultValue={countryName}
                        onChangeText={(text) => this.onChange(text, 'country')}
                        renderItem={({item, i}) => (
                            <TouchableOpacity
                                onPress={() => this.setState({
                                    countryName: item.label,
                                    isCountryName: true,
                                    countryId: parseInt(item.value),
                                })}>
                                <Text style={{
                                    padding: wp(2), fontFamily: FONT_FAMILY.Roboto,
                                    fontSize: FONT.TextMedium_2,
                                    color: COLORS.black_color,
                                }}>{item.label}</Text>
                                <View style={{height: wp(0.2), backgroundColor: 'rgb(163,171,180)', width: wp(90)}}/>
                                <Spacer space={0.5}/>
                            </TouchableOpacity>
                        )}
                    />
                    {countryError !== '' && <Text style={{
                        paddingTop: wp(1),
                        color: COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}>{countryError}</Text>}
                </View>
                <Spacer space={2}/>
                <View style={{
                    flex: 1,
                    zIndex: 8,
                    width: wp(90),
                }}>
                    {labelCity.length !== 0 && <Text style={{
                        fontFamily: FONT_FAMILY.RobotoBold,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}>{STRINGS.ABOUT_BUSINESS_CITY_PLACEHOLDER}</Text>}
                    <Autocomplete
                        placeholder={STRINGS.ABOUT_BUSINESS_CITY_PLACEHOLDER}
                        placeholderTextColor={COLORS.placeHolder_color}
                        data={cityResponse}
                        style={{
                            paddingVertical: wp(1),
                            fontFamily: FONT_FAMILY.Roboto,
                            fontSize: FONT.TextMedium_2,
                            color: COLORS.black_color,
                        }}
                        inputContainerStyle={{
                            paddingVertical: wp(2),
                            backgroundColor: COLORS.backGround_color,
                            width: wp(90),
                            borderWidth: 0,
                            borderBottomWidth: wp(0.35),
                            borderBottomColor: 'rgb(163,171,180)',
                        }}
                        listContainerStyle={{
                            width: wp(90),
                        }}
                        listStyle={{
                            height: wp(45),
                            width: wp(90),
                            marginLeft: 0,
                        }}
                        hideResults={isCityName}
                        defaultValue={cityName}
                        onChangeText={text => this.onChange(text, 'city')}
                        renderItem={({item, i}) => (
                            <TouchableOpacity onPress={() => this.setState({
                                cityName: item.label,
                                isCityName: true,
                                cityId: parseInt(item.value),
                            })}>
                                <Text style={{
                                    padding: wp(2), fontFamily: FONT_FAMILY.Roboto,
                                    fontSize: FONT.TextMedium_2,
                                    color: COLORS.black_color,
                                }}>{item.label}</Text>
                                <View style={{height: wp(0.2), backgroundColor: 'rgb(163,171,180)', width: wp(90)}}/>
                                <Spacer space={0.5}/>
                            </TouchableOpacity>
                        )}
                    />
                    {cityError !== '' && <Text style={{
                        paddingTop: wp(1),
                        color: COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}>{cityError}</Text>}
                </View>
            </>
        );
    };


    _renderBusinessAddress = () => {
        const {validZipCode, labelZipCode, zipCode, validWebsite, labelWebsite, website} = this.state;
        return (
            <View style={{alignItems: 'center'}}>
                <Input
                    inputContainerStyle={{width: wp(90)}}
                    placeholder={STRINGS.ABOUT_BUSINESS_ZIP_PLACEHOLDER}
                    label={labelZipCode}
                    labelStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}
                    errorStyle={{
                        color: COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}
                    errorMessage={validZipCode}
                    inputStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                    }}
                    value={zipCode}
                    maxLength={6}
                    keyboardType="phone-pad"
                    onChangeText={(text) => {
                        this.onChange(text, 'zip');
                    }}
                />
                <Input
                    inputContainerStyle={{width: wp(90)}}
                    placeholder={`${STRINGS.ABOUT_BUSINESS_WEBSITE_PLACEHOLDER} (Optional)`}
                    label={labelWebsite}
                    labelStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}
                    errorStyle={{
                        color: COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}
                    errorMessage={validWebsite}
                    inputStyle={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextMedium_2,
                        color: COLORS.black_color,
                    }}
                    value={website}
                    onChangeText={(text) => {
                        this.onChange(text, 'website');
                    }}
                />
            </View>
        );
    };
    _renderCheckBox = () => {
        const {isChecked} = this.state;
        const {navigation} = this.props;
        const {navigate} = navigation;
        return (
            <TouchableOpacity
                style={{
                    width: wp(90),
                    flexDirection: 'row',
                }}
                onPress={() => this.onChange('', 'terms')}>
                {isChecked ? <CheckSVG/> : <UnCheckSVG/>}
                <Spacer row={2}/>
                <Text style={{
                    fontSize: FONT.TextSmall,
                    color: COLORS.black_color,
                    fontFamily: FONT_FAMILY.Roboto,
                }}>I accept the
                    <Text style={{color: COLORS.app_theme_color}} onPress={() => navigate('terms')}> terms and
                        conditions</Text></Text>
            </TouchableOpacity>
        );
    };

    onSelectValue = (item) => {
        console.warn('item', item);
    };

    onChange = async (text, type) => {
        const {isChecked, countryName} = this.state;
        let arr = [1];
        await arr.map(item => {
            switch (type) {
                case 'firstName':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelfirstName: '',
                        });
                    } else if (text.trim().length < 3) {
                        this.setState({
                            validfirstName: STRINGS.name_minimum_three,
                            labelfirstName: STRINGS.ABOUT_BUSINESS_LEGAL_FIRST_PLACEHOLDER,
                        });
                    } else {
                        this.setState({
                            validfirstName: '',
                        });
                    }
                    this.setState({lFirstName: text});
                    break;
                case 'lastname':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelLastName: '',
                        });
                    } else if (text.trim().length < 3) {
                        this.setState({
                            validLastName: STRINGS.name_minimum_three,
                            labelLastName: STRINGS.ABOUT_BUSINESS_LEGAL_LAST_PLACEHOLDER,
                        });
                    } else {
                        this.setState({
                            validLastName: '',
                        });
                    }
                    this.setState({lLastName: text});
                    break;
                case 'position':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelPositionName: '',
                        });
                    } else if (text.trim().length < 3) {
                        this.setState({
                            validPositionName: 'Position can not be less then 3 letters.',
                            labelPositionName: 'Position',
                        });
                    } else {
                        this.setState({
                            validPositionName: '',
                        });
                    }
                    this.setState({postionName: text});
                    break;
                case 'street':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelStreetAddress: '',
                            validStreetAddress: 'Street Address can not be empty.',
                        });
                    } else if (text.trim().length < 3) {
                        this.setState({
                            labelStreetAddress: STRINGS.ABOUT_BUSINESS_STREET_ADDRESS_PLACEHOLDER,
                        });
                    } else {
                        this.setState({
                            validStreetAddress: '',
                        });
                    }
                    this.setState({streetAddress: text});
                    break;
                case 'zip':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelZipCode: '',
                            validZipCode: 'ZipCode can not be empty.',
                        });
                    } else if (text.trim().length < 6) {
                        this.setState({
                            labelZipCode: STRINGS.ABOUT_BUSINESS_ZIP_PLACEHOLDER,
                            validZipCode: '',
                        });
                    } else {
                        this.setState({
                            validZipCode: '',
                        });
                    }
                    this.setState({zipCode: text.trim()});
                    break;
                case 'website':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelWebsite: '',
                            // validWebsite: 'Website can not be empty.',
                        });
                    } else if (!validateWebsite(text)) {
                        this.setState({
                            labelWebsite: `${STRINGS.ABOUT_BUSINESS_WEBSITE_PLACEHOLDER} (Optional)`,
                            validWebsite: 'Enter valid website.',
                        });
                    } else {
                        this.setState({
                            validWebsite: '',
                        });
                    }
                    this.setState({website: text.trim()});
                    break;
                case 'org':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelOrganization: '',
                            orgError: '',
                        });
                    } else {
                        this.setState({labelOrganization: STRINGS.BS_ORGANISATION_NAME_PLACEHOLDER});
                    }
                    if (text.trim().length > 0) {
                        OrganizationTypeAction({
                            search_text: text,
                        }, data => this.organizatonTypeResponse(data, 'org'));
                    }
                    this.setState({
                        isOrganizationType: text.trim().length < 1,
                    });
                    break;
                case 'sector':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelSector: '',
                            sectorError: '',
                        });
                    } else {
                        this.setState({labelSector: STRINGS.BS_ORGANISATION_NAME_PLACEHOLDER});
                    }
                    if (text.trim().length > 0) {
                        SectorTypeAction({
                            search_text: text,
                        }, data => this.organizatonTypeResponse(data, 'sector'));
                    }
                    this.setState({
                        isSectorType: text.trim().length < 1,
                    });
                    break;
                case 'country':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelCountry: '',
                            countryError: '',
                        });
                    } else {
                        this.setState({labelCountry: STRINGS.BS_ORGANISATION_NAME_PLACEHOLDER});
                    }
                    if (text.trim().length > 0) {
                        CountryAction({
                            search_text: text,
                        }, data => this.organizatonTypeResponse(data, 'country'));
                    }
                    this.setState({
                        isCountryName: text.trim().length < 1,
                    });
                    break;
                case 'city':
                    if (countryName.length !== 0) {
                        if (text.trim().length === 0) {
                            this.setState({
                                labelCity: '',
                                cityError: '',
                            });
                        } else {
                            this.setState({labelCity: STRINGS.BS_ORGANISATION_NAME_PLACEHOLDER});
                        }
                        if (text.trim().length > 0) {
                            CityAction({
                                country_name: countryName,
                                search_text: text,
                            }, data => this.organizatonTypeResponse(data, 'city'));
                        }
                        this.setState({
                            isCityName: text.trim().length < 1,
                        });
                    } else {
                        // this.showToastAlert('Please choose county first.');
                        this.setState({
                            cityError: 'Please choose county first',
                        });
                    }
                    break;
                case 'terms':
                    this.setState({isChecked: !isChecked});
                    break;
                default:
                    break;

            }
        });
        this.checkAllValues();
    };

    checkAllValues = () => {
        const {
            validfirstName, lFirstName, validLastName, lLastName, validPositionName, postionName, organisationType, sector, phoneNoText,
            streetAddress, validStreetAddress, countryName, cityName, zipCode, validZipCode, website, validWebsite, isChecked, socialId,
        } = this.state;
        if (validfirstName !== '' || lFirstName.length === 0) {
            this.setState({areAllValuesValid: false});
        } else if (validLastName !== '' || lLastName.length === 0) {
            this.setState({areAllValuesValid: false});
        } else if (validPositionName !== '' || postionName.length === 0) {
            this.setState({areAllValuesValid: false});
        } else if (organisationType.length === 0) {
            this.setState({areAllValuesValid: false});
        } else if (sector.length === 0) {
            this.setState({areAllValuesValid: false});
        } else if (countryName.length === 0) {
            this.setState({areAllValuesValid: false});
        } else if (cityName.length === 0) {
            this.setState({areAllValuesValid: false});
        } else if (phoneNoText.length === 0 || phoneNoText.length < 10) {
            this.setState({areAllValuesValid: false});
        } else if (validStreetAddress !== '' || streetAddress.length === 0) {
            this.setState({areAllValuesValid: false});
        } else if (validZipCode !== '' || zipCode.length === 0) {
            this.setState({areAllValuesValid: false});
            // } else if (validWebsite !== '' || website.length === 0) {
            //     this.setState({areAllValuesValid: false});
        } else if (!isChecked) {
            this.setState({areAllValuesValid: false});
        } else {
            this.setState({areAllValuesValid: true});
        }
    };

    _onNextPress() {
        const {navigation} = this.props;
        const {navigate} = navigation;
        const {
            lFirstName, lLastName, postionName, orgId, sectorId, phoneNoText, streetAddress,
            countryId, cityId, zipCode, website, isChecked, socialId, callingCode, socialPlatform,
            userName, orgName, password, orgEmail, id, countryName,
        } = this.state;

        BusinessRegisterAction({
            'firstName': lFirstName,
            'lastName': lLastName,
            'position': postionName,
            'organizationType': orgId,
            'sector': sectorId,
            'phone': phoneNoText,
            'street': streetAddress,
            'country': countryName,
            // 'country': countryId,
            'city': cityId,
            'zipCode': zipCode,
            'website': website,
            'socialId': socialId,
            'socialProfile': socialPlatform,
            'email': orgEmail,
            'userName': socialId !== '' ? socialId : userName,
            'password': socialId !== '' ? 'Mind@123' : password,
            'countryCode': callingCode,
            'orgName': orgName,
            'id': id,
        }, data => this.registerApiResponse(data));
        this.showDialog();
    }

    onPressBack = () => {
        const {navigation} = this.props;
        navigation.goBack();
    };

    registerApiResponse = (response) => {
        const {navigation} = this.props;
        const {navigate} = navigation;
        console.warn(response);

        const {userName} = this.state;
        if (response !== 'Network request failed') {
            this.hideDialog();
            const {code, message, data, technical_message} = response;
            if (code === '200') {
                this.hideDialog();
                this.showToastSucess('PIN Send to registered email.');
                navigate('enterPin', {username: userName, from: 'Personal'});
            } else if (code === '201') {
                this.hideDialog();
                this.showToastAlert(message);
            } else if (code === '422') {
                this.hideDialog();
                this.showToastAlert(technical_message.social_id[0]);
            }else if (code === '403') {
                this.hideDialog();
                this.showToastAlert(message);
            }
        } else {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET);
        }
    };

    organizatonTypeResponse = (response, type) => {
        if (response !== 'Network request failed') {
            this.hideDialog();
            const {code, message, data, technical_message} = response;
            if (code === '200') {
                this.hideDialog();
                switch (type) {
                    case 'org':
                        this.setState({
                            organizationResponse: TransformDropdownData(data),
                            orgError: '',
                        });
                        break;
                    case 'sector':
                        this.setState({
                            sectorResponse: TransformDropdownData(data),
                            sectorError: '',
                        });
                        break;
                    case 'country':
                        this.setState({
                            countryResponse: TransformCountryData(data),
                            countryError: '',
                        });
                        break;
                    case 'city':
                        this.setState({
                            cityResponse: TransformCityData(data),
                            cityError: '',
                        });
                        break;
                }

            } else if (code === '201') {
                this.hideDialog();
                // this.showToastAlert(message);
                switch (type) {
                    case 'org':
                        this.setState({
                            orgError: message,
                            organizationResponse: [],
                        });
                        break;
                    case 'sector':
                        this.setState({
                            sectorError: message,
                            sectorResponse: [],
                        });
                        break;
                    case 'country':
                        this.setState({
                            countryError: message,
                            countryResponse: [],
                        });
                        break;
                    case 'city':
                        this.setState({
                            cityError: message,
                            cityResponse: [],
                        });
                        break;
                }
            } else if (code === '422') {
                this.hideDialog();
                this.showToastAlert(technical_message.social_id[0]);
            }
        } else {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET);
        }
    };


    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={STRINGS.LOADING_TEXT}/>
        );
    };


    render() {
        const {navigation} = this.props;
        const {areAllValuesValid} = this.state;
        return (
            <SafeAreaViewContainer>
                <TopHeader
                    onPressBackArrow={() => this.onPressBack()}/>
                <KeyboardAvoidingView
                    style={{flex: 1, backgroundColor: COLORS.backGround_color}}
                    behavior={(Platform.OS === 'ios') ? 'padding' : null}
                >
                    <TouchableWithoutFeedback
                        onPress={() => {
                            Keyboard.dismiss();
                        }}>
                        <ScrollContainer>
                            <MainContainer>
                                <Spacer space={1}/>
                                <CreateBusinessText>{STRINGS.ABOUT_BUSINESS_TELL_US_TEXT}</CreateBusinessText>
                                <Spacer space={5}/>
                                <StartText>{STRINGS.ABOUT_BUSINESS_CONTACT_TEXT}</StartText>
                                <Spacer space={2}/>
                                {this._renderBusinessContact()}
                                <StartText>{STRINGS.ABOUT_BUSINESS_DETAIL_TEXT}</StartText>
                                <Spacer space={2}/>
                                {this._renderBusinessDetail(false)}
                                <Spacer space={2}/>
                                <StartText>{STRINGS.ABOUT_BUSINESS_PHONE_NUMBER_TEXT}</StartText>
                                <Spacer space={2}/>
                                {this._renderBusinessPhone()}
                                <StartText>{STRINGS.ABOUT_BUSINESS_ADDRESS_TEXT}</StartText>
                                <Spacer space={2}/>
                                {this._renderCountryAndCity()}
                                <Spacer space={2}/>
                                {this._renderBusinessAddress()}
                                <Spacer space={1}/>
                                {this._renderCheckBox()}
                                <Spacer space={2}/>
                                <PrimaryButton
                                    onPress={() => this._onNextPress()}
                                    text={STRINGS.ABOUT_BUSINESS_AGREE_CREATE_TEXT}
                                    isNextIcon={false}
                                    textWidth={wp(85)}
                                    isDisabled={!areAllValuesValid}
                                    bgColor={areAllValuesValid ? COLORS.app_theme_color : COLORS.app_theme_color_disabled}
                                />
                                <Spacer space={3}/>
                            </MainContainer>
                            {this._renderCustomLoader()}
                        </ScrollContainer>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </SafeAreaViewContainer>
        );
    }
}

