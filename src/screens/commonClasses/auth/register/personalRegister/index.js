import React from 'react';
import {
    Image,
    KeyboardAvoidingView,
    Keyboard,
    Text,
    TouchableWithoutFeedback,
    View, TouchableOpacity,
} from 'react-native';
import {Input} from 'react-native-elements';

import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

import moment from 'moment';
import BaseClass from '../../../../../utils/BaseClass';
import STRINGS from '../../../../../utils/Strings';
import {Spacer} from '../../../../../customComponents/Spacer';
import CheckSVG from '../../../../../customComponents/imagesComponents/Check';
import UnCheckSVG from '../../../../../customComponents/imagesComponents/Uncheck';
import {MainContainer, SafeAreaViewContainer, ScrollContainer} from '../../../../../utils/BaseStyle';
import TopHeader from '../../../../../customComponents/Header';
import COLORS from '../../../../../utils/Colors';
import {PrimaryButton} from '../../../../../customComponents/ButtonComponent';
import {ICONS} from '../../../../../utils/ImagePaths';
import styles from './styles';
import {validateEmail, validatePassword} from '../../../../../utils/Validations';
import {UserAvailabillityAction} from '../../../../../redux/actions/UserAvailabilityAction';
import {VerifyEmailAction} from '../../../../../redux/actions/VerifyEmailAction';
import {PersonalRegisterAction} from '../../../../../redux/actions/PersonalRegisterAction';
import OrientationLoadingOverlay from '../../../../../customComponents/CustomLoader';
import AsyncStorage from '@react-native-community/async-storage';

class PersonalRegister extends BaseClass {
    constructor(props) {
        super(props);
        const {route} = this.props;
        const {socialId, socialPlatform, socialEmail} = route.params;

        this.state = {
            validFName: '',
            labelFName: '',
            firstName: '',

            validLName: '',
            labelLName: '',
            lastName: '',

            validUName: '',
            labelUName: '',
            userName: '',

            validEmail: '',
            labelEmail: '',
            email: socialEmail,

            validPassword: '',
            labelPassword: '',
            password: '',

            validCPassword: '',
            labelCPassword: '',
            confirmPassword: '',

            // datePicker: moment(Date()).subtract(13, 'years').format('DD-MM-YYYY'),
            labelDate: '',
            validDate: '',
            datePicker: new Date(),
            datePicker1: new Date(),
            showPicker: false,

            checked: false,
            areAllValuesValid: false,

            socialId: socialId,
            socialPlatform: socialPlatform,

            isLoading: false,
        };
        this.isPickerShowing = false;
    }


    onUserEditingEnd = () => {
        UserAvailabillityAction({
            username: this.state.userName,
        }, data => this.isUserAvailableResponse(data));
    };

    isUserAvailableResponse = (response) => {

        if (response !== 'Network request failed') {
            this.hideDialog();
            const {code, message, data} = response;
            if (code === '200') {
                this.hideDialog();
                this.setState({
                    validUName: message,
                });
            } else if (code === '403') {
                this.hideDialog();
                this.setState({
                    validUName: 'Username Unavailable',
                });
            }
        } else {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET);
        }
    };

    onEmailEditingEnd = () => {
        // VerifyEmailAction({
        //     email: this.state.email,
        // }, data => this.isEmailAvailableResponse(data));
        // this.showDialog();

        const {userName, firstName, lastName, email, password, datePicker, socialId, socialPlatform} = this.state;
        if (socialId === '') {
            PersonalRegisterAction(
                {
                    username: userName,
                    firstname: firstName,
                    lastname: lastName,
                    email: email,
                    password: password,
                    dob: datePicker,
                    socialId: socialId,
                    socialPlatform: socialPlatform,
                },
                data => this.personalRegisterResponse(data));
            this.showDialog();
        } else {
            PersonalRegisterAction(
                {
                    username: socialId,
                    firstname: firstName,
                    lastname: lastName,
                    email: email,
                    password: 'Mind@123',
                    dob: datePicker,
                    socialId: socialId,
                    socialPlatform: socialPlatform,
                },
                data => this.personalRegisterResponse(data));
            this.showDialog();
        }
    };

    // isEmailAvailableResponse = (response) => {
    //     const {userName, firstName, lastName, email, password, datePicker, socialId, socialPlatform} = this.state;
    //     if (response !== 'Network request failed') {
    //         this.hideDialog();
    //         const {code, message, data} = response;
    //         if (code === '200') {
    //             this.hideDialog();
    //             if (socialId === '') {
    //                 PersonalRegisterAction(
    //                     {
    //                         username: userName,
    //                         firstname: firstName,
    //                         lastname: lastName,
    //                         email: email,
    //                         password: password,
    //                         dob: datePicker,
    //                         socialId: socialId,
    //                         socialPlatform: socialPlatform,
    //                     },
    //                     data => this.personalRegisterResponse(data));
    //                 this.showDialog();
    //             } else {
    //                 PersonalRegisterAction(
    //                     {
    //                         username: socialId,
    //                         firstname: firstName,
    //                         lastname: lastName,
    //                         email: email,
    //                         password: 'Mind@123',
    //                         dob: datePicker,
    //                         socialId: socialId,
    //                         socialPlatform: socialPlatform,
    //                     },
    //                     data => this.personalRegisterResponse(data));
    //                 this.showDialog();
    //             }
    //
    //         } else if (code === '403') {
    //             this.hideDialog();
    //             this.showToastAlert(message);
    //         }
    //     } else {
    //         this.hideDialog();
    //         this.showToastAlert(STRINGS.CHECK_INTERNET);
    //     }
    // };

    personalRegisterResponse = (response) => {
        const {navigation} = this.props;
        const {navigate} = navigation;
        const {userName, socialId} = this.state;
        let user_name = '';
        if (socialId !== '') {
            user_name = socialId;
        } else {
            user_name = userName;
        }

        if (response !== 'Network request failed') {
            this.hideDialog();
            const {code, message, data} = response;
            if (code === '200') {
                this.hideDialog();
                this.showToastSucess('PIN Send to registered email.');
                navigate('enterPin', {username: user_name, from: 'Personal'});
            } else if (code === '403') {
                this.hideDialog();
                this.showToastAlert(message);
            }
        } else {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET);
        }
        this.hideDialog();
    };

    onChange = async (text, type) => {
        const {password, checked, datePicker1} = this.state;
        let arr = [1];
        await arr.map(item => {
            switch (type) {
                case 'firstName':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelFName: '',
                        });
                    } else if (text.trim().length < 3) {
                        this.setState({
                            validFName: STRINGS.name_minimum_three,
                            labelFName: 'First Name',
                        });
                    } else {
                        this.setState({
                            validFName: '',
                        });
                    }
                    this.setState({firstName: text});
                    break;

                case 'lastName':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelLName: '',
                        });
                    } else if (text.trim().length < 3) {
                        this.setState({
                            validLName: STRINGS.name_minimum_three,
                            labelLName: 'Last Name',
                        });
                    } else {
                        this.setState({
                            validLName: '',
                        });
                    }
                    this.setState({lastName: text});
                    break;

                case 'userName':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelUName: '',
                            validUName: '',
                        });
                    } else if (text.trim().length < 5) {
                        this.setState({
                            // validUName: 'Username Unavailable',
                            labelUName: 'Create Username',
                        });
                        // } else {
                        //     this.setState({
                        //         validUName: 'Username Available',
                        //     });
                    }
                    this.setState({userName: text});
                    break;

                case 'email':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelEmail: '',
                        });
                    } else if (!validateEmail(text)) {
                        this.setState({
                            validEmail: STRINGS.valid_email,
                            labelEmail: 'Email',
                        });
                    } else {
                        this.setState({
                            validEmail: '',
                        });
                    }
                    this.setState({email: text});
                    break;

                case 'password':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelPassword: '',
                            confirmPassword: '',
                        });
                    } else if (!validatePassword(text)) {
                        this.setState({
                            validPassword: STRINGS.enter_valid_password,
                            labelPassword: 'Create Password',
                        });
                    } else {
                        this.setState({
                            validPassword: '',
                        });
                    }
                    this.setState({password: text});
                    break;

                case 'confirmPassword':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelCPassword: '',
                        });
                    } else if (password !== text) {
                        this.setState({
                            validCPassword: STRINGS.password_does_not_match,
                            labelCPassword: 'Confirm Password',
                        });
                    } else {
                        this.setState({
                            validCPassword: '',
                        });
                    }
                    this.setState({confirmPassword: text});
                    break;

                case 'dateOfBirth':
                    this.hideDatePicker();

                    let dateValue = text.toISOString();
                    dateValue = parseInt(dateValue.slice(0, 4));

                    let dateNew = new Date().toISOString();
                    dateNew = parseInt(dateNew.slice(0, 4));
                    console.warn(dateNew, dateValue);
                    if (text !== '') {
                        this.setState({
                            labelDate: 'Date of Birth',
                        });
                    }
                    if (dateNew === dateValue) {
                        console.warn('abc');
                        this.setState({
                            validDate: 'Current Date can not user\'s Date of Birth',
                        });
                    } else {
                        this.setState({
                            labelDate: '',
                            validDate: '',
                        });
                    }
                    let date = moment(text).format('YYYY-MM-DD');
                    console.warn('text', date);
                    this.setState({
                        datePicker: date,
                        datePicker1: text,
                    });
                    break;
                case 'terms':
                    this.setState({checked: !checked});
                    break;

                default:
                    break;

            }
        });
        this.checkAllValues();
    };

    checkAllValues = () => {
        const {
            validFName, validLName, validUName, datePicker, validEmail, validPassword, validCPassword,
            checked, email, firstName, lastName, userName, password, confirmPassword, socialId, datePicker1,
        } = this.state;

        let dateValue = datePicker1.toISOString();
        dateValue = parseInt(dateValue.slice(0, 4));

        let dateNew = new Date().toISOString();
        dateNew = parseInt(dateNew.slice(0, 4));

        if (socialId === '') {
            if (validFName !== '' || firstName.length === '') {
                this.setState({areAllValuesValid: false});
            } else if (validLName !== '' || lastName.length === 0) {
                this.setState({areAllValuesValid: false});
            } else if (validUName === 'Username Unavailable' || userName.length === 0) {
                this.setState({areAllValuesValid: false});
            } else if (validEmail !== '' || email.length === 0) {
                this.setState({areAllValuesValid: false});
            } else if (validPassword !== '' || password.length === 0) {
                this.setState({areAllValuesValid: false});
            } else if (validCPassword !== '' || confirmPassword.length === 0) {
                this.setState({areAllValuesValid: false});
            } else if (dateNew === dateValue) {
                this.setState({areAllValuesValid: false});
            } else if (!checked) {
                this.setState({areAllValuesValid: false});
            } else {
                this.setState({areAllValuesValid: true});
            }
        } else {
            if (validFName !== '' || firstName.length === '') {
                this.setState({areAllValuesValid: false});
            } else if (validLName !== '' || lastName.length === 0) {
                this.setState({areAllValuesValid: false});
            } else if (validEmail !== '' || email.length === 0) {
                this.setState({areAllValuesValid: false});
            } else if (dateNew === dateValue) {
                this.setState({areAllValuesValid: false});
            } else if (!checked) {
                this.setState({areAllValuesValid: false});
            } else {
                this.setState({areAllValuesValid: true});
            }
        }
    };

    hideDatePicker = () => {
        this.isPickerShowing = false;
        this.setState({
            showPicker: false,
        });
    };

    _renderInputs = () => {
        const {
            validFName, labelFName, validLName, labelLName, validUName, labelUName, datePicker,
            email, firstName, lastName, userName, password, confirmPassword, socialId, labelDate,
            validEmail, labelEmail, validPassword, labelPassword, validCPassword, labelCPassword, checked, validDate,
        } = this.state;
        console.warn('date', validDate);
        const {navigation} = this.props;
        const {navigate} = navigation;
        return (
            <View style={{alignItems: 'center'}}>
                <Input
                    placeholder='First Name'
                    ref={'FirstName'}
                    label={labelFName}
                    labelStyle={styles.input_label_style}
                    errorStyle={styles.input_error_style}
                    errorMessage={validFName}
                    inputStyle={styles.input_input_container}
                    inputContainerStyle={{width: wp(90)}}
                    value={firstName}
                    onChangeText={(text) => this.onChange(text, 'firstName')}
                    onSubmitEditing={() => this.refs.LastName.focus()}
                    returnKeyType={'next'}
                />
                <Spacer space={1}/>
                <Input
                    placeholder='Last Name'
                    ref={'LastName'}
                    label={labelLName}
                    labelStyle={styles.input_label_style}
                    errorStyle={styles.input_error_style}
                    errorMessage={validLName}
                    inputStyle={styles.input_input_container}
                    inputContainerStyle={{width: wp(90)}}
                    value={lastName}
                    onChangeText={(text) => this.onChange(text, 'lastName')}
                    onSubmitEditing={() => this.refs.UserName.focus()}
                    returnKeyType={'next'}
                />
                <Spacer space={1}/>
                <Input
                    disabled={socialId !== ''}
                    placeholder='Create Username'
                    ref={'UserName'}
                    label={labelUName}
                    labelStyle={styles.input_label_style}
                    errorStyle={[styles.input_error_style,
                        {color: validUName === 'Username available' ? COLORS.valid_Color : COLORS.invalid_color}]}
                    errorMessage={validUName}
                    inputStyle={styles.input_input_container}
                    inputContainerStyle={{width: wp(90)}}
                    value={userName}
                    onChangeText={(text) => this.onChange(text, 'userName')}
                    onEndEditing={() => this.onUserEditingEnd()}
                    onSubmitEditing={() => this.refs.Email.focus()}
                    returnKeyType={'next'}
                />
                <Spacer space={1}/>
                <Input
                    placeholder='Email'
                    ref={'Email'}
                    label={labelEmail}
                    labelStyle={styles.input_label_style}
                    errorStyle={styles.input_error_style}
                    errorMessage={validEmail}
                    inputStyle={styles.input_input_container}
                    inputContainerStyle={{width: wp(90)}}
                    value={email}
                    keyboardType={'email-address'}
                    onChangeText={(text) => this.onChange(text, 'email')}
                    onSubmitEditing={() => this.refs.Password.focus()}
                    returnKeyType={'next'}
                />
                <Spacer space={1}/>
                <Input
                    disabled={socialId !== ''}
                    placeholder='Create Password'
                    ref={'Password'}
                    label={labelPassword}
                    secureTextEntry={true}
                    labelStyle={styles.input_label_style}
                    errorStyle={styles.input_error_style}
                    errorMessage={validPassword}
                    inputStyle={styles.input_input_container}
                    inputContainerStyle={{width: wp(90)}}
                    value={password}
                    onChangeText={(text) => this.onChange(text, 'password')}
                    onSubmitEditing={() => this.refs.CPassword.focus()}
                    returnKeyType={'next'}
                />
                <Spacer space={1}/>
                <Input
                    disabled={socialId !== ''}
                    placeholder='Confirm Password'
                    ref={'CPassword'}
                    label={labelCPassword}
                    secureTextEntry={true}
                    labelStyle={styles.input_label_style}
                    errorStyle={styles.input_error_style}
                    errorMessage={validCPassword}
                    inputStyle={styles.input_input_container}
                    inputContainerStyle={{width: wp(90)}}
                    value={confirmPassword}
                    onChangeText={(text) => this.onChange(text, 'confirmPassword')}
                    returnKeyType={'done'}
                />
                <Spacer space={1}/>

                <TouchableOpacity onPress={() => {
                    this.isPickerShowing = true;
                    this.setState({showPicker: true});
                }}>
                    <Input
                        pointerEvents={'none'}
                        editable={false}
                        placeholder='Date of Birth'
                        label={labelDate}
                        value={datePicker}
                        errorStyle={styles.input_error_style}
                        errorMessage={validDate}
                        labelStyle={styles.input_label_style}
                        inputStyle={styles.input_input_container}
                        inputContainerStyle={{width: wp(90)}}
                        // onChangeText={(text) => this.onChange(text, 'confirmPassword')}
                    />
                </TouchableOpacity>
                <Spacer space={2}/>
                <TouchableOpacity
                    style={styles.check_box_touchable}
                    onPress={() => this.onChange('', 'terms')}>
                    {checked ? <CheckSVG/> : <UnCheckSVG/>}
                    <Spacer row={2}/>
                    <Text style={styles.i_accept_text}>I accept the
                        <Text style={styles.terms_conditions} onPress={() => navigate('terms')}> terms and
                            conditions</Text></Text>
                </TouchableOpacity>

            </View>
        );
    };

    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={STRINGS.LOADING_TEXT}/>
        );
    };

    render() {
        let dateValue = new Date().toISOString();
        dateValue = parseInt(dateValue.slice(0, 4));
        const {datePicker, showPicker, areAllValuesValid} = this.state;
        const {navigation} = this.props;
        return (
            <SafeAreaViewContainer>
                <TopHeader onPressBackArrow={() => navigation.goBack()}/>
                <KeyboardAvoidingView
                    style={styles.keyboard_style}
                    behavior={(Platform.OS === 'ios') ? 'padding' : null}>
                    <TouchableWithoutFeedback
                        onPress={() => {
                            Keyboard.dismiss();
                        }}>
                        <ScrollContainer>
                            <MainContainer>
                                <Image style={styles.logo_style}
                                       source={ICONS.LOGO} resizeMode={'contain'}/>
                                <Text style={styles.text_style}>Create<Text>{'\n'}Personal Account</Text></Text>
                                <Spacer space={5}/>
                                {this._renderInputs()}
                                {this.isPickerShowing && <DateTimePickerModal
                                    isVisible={showPicker}
                                    date={new Date(datePicker)}
                                    mode="date"
                                    // maximumDate={new Date(dateValue - 13, 8, 25)}
                                    onConfirm={(date) => this.onChange(date, 'dateOfBirth')}
                                    onCancel={() => this.hideDatePicker()}
                                />}
                                <Spacer space={4}/>
                                <PrimaryButton
                                    onPress={() => this.onEmailEditingEnd()}
                                    isDisabled={!areAllValuesValid}
                                    bgColor={areAllValuesValid ? COLORS.app_theme_color : COLORS.app_theme_color_disabled}
                                    text={STRINGS.register_btn_text}/>
                                <Spacer space={4}/>
                            </MainContainer>
                            {this._renderCustomLoader()}
                        </ScrollContainer>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </SafeAreaViewContainer>
        );
    }
}

export default PersonalRegister;
