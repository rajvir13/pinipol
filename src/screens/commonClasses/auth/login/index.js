import React from 'react';
import {Input} from 'react-native-elements';
import {
    Image,
    View,
    Text,
    TouchableOpacity,
    NativeModules,
    KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard,
} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {ICONS} from '../../../../utils/ImagePaths';
import BaseClass from '../../../../utils/BaseClass';
import COLORS from '../../../../utils/Colors';
import STRINGS from '../../../../utils/Strings';
import {SafeAreaViewContainer, MainContainer, ScrollContainer} from '../../../../utils/BaseStyle';
import {Spacer} from '../../../../customComponents/Spacer';
import {PrimaryButton} from '../../../../customComponents/ButtonComponent';
import styles from './LoginStyle';
import CheckSVG from '../../../../customComponents/imagesComponents/Check';
import UnCheckSVG from '../../../../customComponents/imagesComponents/Uncheck';
import {validatePassword} from '../../../../utils/Validations';
import OrSVG from '../../../../customComponents/imagesComponents/ORSVG';
import {GoogleSignin} from '@react-native-community/google-signin';
import {facebookService} from '../../../../customComponents/FacebookButton';
import {googleService} from '../../../../customComponents/GoogleButton';
import CookieManager from '@react-native-community/cookies';
import {instagramService} from '../../../../customComponents/InstagramButton';
import {twitterService} from '../../../../customComponents/TwitterButton';
import {LoginAction} from '../../../../redux/actions/LoginAction';
import {connect} from 'react-redux';
import * as _ from 'lodash';
import {SocialLoginAction} from '../../../../redux/actions/SocialLogin';
import OrientationLoadingOverlay from '../../../../customComponents/CustomLoader';
import AsyncStorage from '@react-native-community/async-storage';
import {CommonActions} from '@react-navigation/native';
import Autocomplete from '../../../../customComponents/ModalDropdown/AutoComplete';
import {FONT_FAMILY} from '../../../../utils/Font';
import {FONT} from '../../../../utils/FontSizes';
import LogoIconSVG from '../../../../customComponents/imagesComponents/LogoIcon';
import CrossIcon from 'react-native-vector-icons/Entypo';

const {RNTwitterSignIn} = NativeModules;

class LoginScreen extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            rememberData: [],
            rememberData2: [],
            userName: '',
            password: '',
            checked: false,
            labelUserName: '',
            validUserName: '',
            labelPassword: '',
            validPassword: '',
            areAllValuesValid: false,
            isLoading: false,
        };
        this.social_platform = '';
        this.social_id = '';
        this.social_email = '';
    }

    componentDidMount(): void {
        GoogleSignin.configure({
            iosClientId: '170093893117-rj95qreeppt2tntpfthdahisi65i782l.apps.googleusercontent.com',
        });
        AsyncStorage.getItem('remenberMe', (error, result) => {
            if (result !== undefined && result !== null) {
                let loginData = JSON.parse(result);

                this.setState({
                    name: loginData.userName,
                    rememberData2: loginData,
                });
            }
        });
    }

    componentWillReceiveProps(nextProps, nextContext) {
        const {navigation} = this.props;
        const {navigate} = navigation;
        const {loginResponse} = nextProps.loginState;
        const {checked, userName, password} = this.state;

        if (loginResponse !== 'Network request failed') {
            this.hideDialog();
            const {code, message, data} = loginResponse;
            if (code === '200') {
                this.hideDialog();
                if (checked === true) {
                    AsyncStorage.setItem('remenberMe', JSON.stringify({
                        userName: userName, password: password,
                    }));
                }
                if (loginResponse.usertype === 'Pinipol') {
                    this.setState({
                        userName: '',
                        password: '',
                    });
                    navigation.dispatch(
                        CommonActions.reset({
                            index: 0,
                            routes: [
                                {
                                    name: 'personalHome',
                                    params: {authToken: loginResponse.token},
                                },
                            ],
                        }),
                    );
                    AsyncStorage.setItem(STRINGS.accessToken, JSON.stringify(loginResponse.token));
                    AsyncStorage.setItem(STRINGS.loginData, JSON.stringify(loginResponse.data));
                    AsyncStorage.setItem(STRINGS.userPersonalInfo, JSON.stringify({
                        'profile_image': data.profile_image,
                        'firstname': data.firstname,
                        'lastname': data.lastname,
                        'account_type': data.account_type,
                        'org_name': data.name,
                    }));
                    this.showToastSucess('Logged in successfully.');
                } else {
                    this.showToastAlert('This is not Pinipol User.');
                }
            } else if (code === '201') {
                this.hideDialog();
                if (loginResponse.usertype === 'Pinipol') {
                    this.showToastAlert(message);
                    navigate('enterPin', {username: loginResponse.username, from: 'Personal'});
                } else {
                    this.showToastAlert('This is not Pinipol User.');
                }
            } else if (code === '202') {
                this.hideDialog();
                if (loginResponse.usertype === 'Pinipol') {
                    this.showToastAlert(message);
                } else {
                    this.showToastAlert('This is not Pinipol User.');
                }
            } else if (code === '203') {
                this.hideDialog();
                if (loginResponse.usertype === 'Pinipol') {
                    this.showToastAlert(message);
                } else {
                    this.showToastAlert('This is not Pinipol User.');
                }
            } else if (code === '403') {
                this.hideDialog();
                this.showToastAlert('Invalid username or password.');
            }
        } else {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET);
        }
    }

    forgotPassword() {
        const {navigate} = this.props.navigation;
        navigate('ForgotPassword');
    }

    onChange = async (text, type) => {
        const {checked, name, rememberData2} = this.state;
        let arr = [1];
        await _.map(arr, ({item}) => {
            switch (type) {
                case 'username':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelUserName: '',
                            validUserName: 'Username can not be empty.',
                        });
                    } else if (text.trim().length >= 1) {
                        this.setState({
                            validUserName: '',
                            labelUserName: 'Enter Username',
                        });
                    } else {
                        this.setState({
                            validUserName: '',
                        });
                    }

                    let res = name.startsWith(text);
                    console.warn('res', res);
                    if (text.length > 0) {
                        if (res) {
                            this.setState({
                                rememberData: [{label: rememberData2.userName, value: rememberData2.password}],
                                userName: text,
                            });
                        } else {
                            this.setState({
                                rememberData: [],
                                userName: text,
                            });
                        }
                    } else {
                        this.setState({
                            rememberData: [],
                            userName: text,
                        });
                    }
                    // this.setState({userName: text});
                    break;
                case 'password':
                    if (text.trim().length === 0) {
                        this.setState({
                            labelPassword: '',
                        });
                    } else if (!validatePassword(text)) {
                        this.setState({
                            validPassword: STRINGS.enter_valid_password,
                            labelPassword: 'Password',
                        });
                    } else {
                        this.setState({
                            validPassword: '',
                        });
                    }
                    this.setState({password: text});
                    break;

                case 'terms':
                    this.setState({checked: !checked});
                    break;


                default:
                    break;

            }

        });

        this.checkAllValues();
    };

    checkAllValues = () => {
        const {validUserName, validPassword, checked, userName, password} = this.state;
        if (validUserName !== '' || userName.length === 0) {
            this.setState({areAllValuesValid: false});
        } else if (validPassword !== '' || password.length === 0) {
            this.setState({areAllValuesValid: false});
            // } else if (!checked) {
            //     this.setState({areAllValuesValid: false});
        } else {
            this.setState({areAllValuesValid: true});
        }
    };

    login = () => {
        const {userName, password} = this.state;
        // console.warn(userName);
        this.props.loginApi({
            username: userName,
            password: password,
            deviceToken: this.DeviceToken(),
        });
        this.showDialog();
    };

    socialResponse = (response) => {
        const {navigation} = this.props;
        const {navigate} = navigation;
        if (response !== 'Network request failed') {
            this.hideDialog();
            const {code, message, technical_message, data} = response;
            if (code === '200') {
                this.hideDialog();
                if (response.usertype === 'Pinipol') {
                    this.setState({
                        userName: '',
                        password: '',
                    });
                    navigation.dispatch(
                        CommonActions.reset({
                            index: 0,
                            routes: [
                                {
                                    name: 'personalHome',
                                    params: {authToken: response.token},
                                },
                            ],
                        }),
                    );
                    AsyncStorage.setItem(STRINGS.accessToken, JSON.stringify(response.token));
                    AsyncStorage.setItem(STRINGS.loginData, JSON.stringify(response.data));
                    AsyncStorage.setItem(STRINGS.userPersonalInfo, JSON.stringify({
                        'profile_image': data.profile_image,
                        'firstname': data.firstname,
                        'lastname': data.lastname,
                        'account_type': data.account_type,
                        'org_name': data.name,
                    }));
                    this.showToastSucess('Logged in successfully.');
                } else {
                    this.showToastAlert('This is not Pinipol User.');
                }
            } else if (code === '201') {
                this.hideDialog();
                if (response.usertype === 'Pinipol') {
                    this.showToastAlert(message);
                    navigate('enterPin', {username: response.username, from: 'Personal'});
                } else {
                    this.showToastAlert('This is not Pinipol User.');
                }
            } else if (code === '202') {
                this.hideDialog();
                if (response.usertype === 'Pinipol') {
                    this.showToastAlert(message);
                } else {
                    this.showToastAlert('This is not Pinipol User.');
                }
            } else if (code === '203') {
                this.hideDialog();
                if (response.usertype === 'Pinipol') {
                    this.showToastAlert(message);
                } else {
                    this.showToastAlert('This is not Pinipol User.');
                }
            } else if (code === '204') {
                this.hideDialog();
                this.showToastAlert('This is not Pinipol User.');
            } else if (code === '403') {
                this.hideDialog();
                // this.showToastAlert('User does not exist.');
                navigate('selectAccount', {
                    socialId: this.social_id,
                    socialPlatform: this.social_platform,
                    socialEmail: this.social_email,
                });
            } else if (code === '422') {
                this.hideDialog();
                this.showToastAlert(technical_message.social_id[0]);
            }
        } else {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET);
        }

    };


    // ----------------------------------------
// ----------------------------------------
// RENDERS
// ----------------------------------------

    _renderFacebookButton = () => {
        return (
            <View>
                {
                    facebookService.facebookLoginButton((fbData) => {
                        // const profile = await facebookService.fetchProfile();
                        console.warn(fbData);

                        this.social_platform = 'Facebook';
                        this.social_email = fbData.email;
                        this.social_id = fbData.id;
                        SocialLoginAction(
                            {
                                social_id: fbData.id,
                                deviceToken: this.DeviceToken(),
                            },
                            data => this.socialResponse(data));
                        this.showDialog();
                    })
                }
            </View>
        );
    };

    _renderGoogleButton = () => {
        // const {fcmToken} = this.state;

        return (
            <View>
                {
                    googleService.googleLogin((data) => {
                        console.warn('data', data);
                        this.social_platform = 'Google';
                        this.social_email = data.user.email;
                        this.social_id = data.user.id;
                        SocialLoginAction(
                            {
                                social_id: data.user.id,
                                deviceToken: this.DeviceToken(),
                            },
                            data => this.socialResponse(data));

                        this.removeSessionFromDevice();
                        this.showDialog();

                    })
                }
            </View>
        );
    };

    removeSessionFromDevice = () => {
        GoogleSignin.signOut()
            .then(() => {
                /*
                  something
                */
            })
            .catch(err => {
                console.log(err);
                /*
                  something
                */
            })
            .done();
    };


    _renderInstagramButton = () => {
        return (
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
                {
                    instagramService.instagramButton((data) => {
                        console.warn('data', data);
                        this.social_platform = 'Instagram';
                        this.social_id = data.user_id;
                        this.social_email = '';
                        SocialLoginAction(
                            {
                                social_id: data.user_id,
                                deviceToken: this.DeviceToken(),
                            },
                            data => this.socialResponse(data));
                        this.onClear();
                        this.showDialog();
                    })
                }
            </View>
        );
    };

    onClear() {
        CookieManager.clearAll(true)
            .then((res) => {
                this.setState({token: null});
            });
    }

    _renderTwitterButton = () => {
        return (
            <View>
                {
                    twitterService.twitterLoginButton((twitterData) => {
                        console.warn('data', twitterData);
                        this.social_platform = 'Twitter';
                        this.social_email = twitterData.email;
                        this.social_id = twitterData.userID;
                        SocialLoginAction(
                            {
                                social_id: twitterData.userID,
                                deviceToken: this.DeviceToken(),
                            },
                            data => this.socialResponse(data));
                        RNTwitterSignIn.logOut();
                        this.showDialog();
                    })
                }
            </View>
        );
    };

    _renderInputs = () => {
        const {navigate} = this.props.navigation;
        const {
            validUserName, labelUserName, validPassword, labelPassword, checked, userName, password, rememberData,
        } = this.state;
        return (
            <>
                <View style={{
                    // flex: 1,
                    zIndex: 8,
                    width: wp(90),
                }}>
                    {labelUserName.length !== 0 && <Text style={{
                        fontFamily: FONT_FAMILY.RobotoBold,
                        fontSize: FONT.TextSmall,
                        color: COLORS.black_color,
                    }}>{labelUserName}</Text>}
                    <Autocomplete
                        // autoCapital={true}
                        placeholder={'Username'}
                        placeholderTextColor={COLORS.placeHolder_color}
                        data={rememberData}
                        style={{
                            paddingVertical: wp(1),
                            fontFamily: FONT_FAMILY.Roboto,
                            fontSize: FONT.TextMedium_2,
                            color: COLORS.black_color,
                        }}
                        inputContainerStyle={{
                            paddingVertical: wp(2),
                            backgroundColor: COLORS.backGround_color,
                            width: wp(90),
                            borderWidth: 0,
                            borderBottomWidth: wp(0.35),
                            borderBottomColor: 'rgb(163,171,180)',
                        }}
                        // listContainerStyle={{
                        //     // width: wp(90),
                        // }}
                        listStyle={{
                            height: wp(20),
                            width: wp(90),
                            marginLeft: 0,
                            borderRadius: wp(1),
                        }}
                        hideResults={rememberData.length === 0}
                        defaultValue={userName}
                        onChangeText={text => this.onChange(text, 'username')}
                        onSubmitEditing={() => {
                            this.refs.Password.focus();
                            this.setState({rememberData: []});
                        }}
                        renderItem={({item, i}) => (
                            <View style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                paddingHorizontal: wp(2),
                                height: wp(20),
                                backgroundColor: COLORS.modal_color,
                            }}>
                                <LogoIconSVG width={wp(12)} height={wp(12)}/>
                                <Spacer row={0.5}/>
                                <TouchableOpacity
                                    onPressIn={async () => {
                                        await this.setState({
                                            userName: item.label,
                                            rememberData: [],
                                            password: item.value,
                                            labelPassword: 'Password',
                                            validPassword: '',

                                        });
                                        this.checkAllValues();
                                    }}>
                                    <View style={{width: wp(65)}}>
                                        <Text style={{
                                            fontFamily: FONT_FAMILY.Roboto,
                                            fontSize: FONT.TextMedium_2,
                                            color: COLORS.black_color,
                                        }} onPress={async () => {
                                            await this.setState({
                                                userName: item.label,
                                                rememberData: [],
                                                password: item.value,
                                                labelPassword: 'Password',
                                                validPassword: '',
                                            });
                                            this.checkAllValues();
                                        }}>{item.label}</Text>
                                        <Spacer space={1}/>
                                        <Text style={{
                                            // padding: wp(2),
                                            fontFamily: FONT_FAMILY.Roboto,
                                            fontSize: FONT.TextMedium_2,
                                            color: COLORS.black_color,
                                        }} onPress={async () => {
                                            await this.setState({
                                                userName: item.label,
                                                rememberData: [],
                                                password: item.value,
                                                labelPassword: 'Password',
                                                validPassword: '',
                                            });
                                            this.checkAllValues();
                                        }}>********</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={{height: wp(20)}}>
                                    <CrossIcon name={'circle-with-cross'} size={wp(9)}
                                               onPress={() => this.setState({rememberData: []})}/>
                                </TouchableOpacity>
                            </View>
                        )}
                    />
                    {validUserName !== '' && <Text style={{
                        paddingTop: wp(1),
                        color: COLORS.invalid_color,
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: FONT.TextSmall_2,
                    }}>{validUserName}</Text>}
                </View>
                {/*<Input*/}
                {/*    placeholder='Username'*/}
                {/*    ref="Login"*/}
                {/*    autoCapitalize='none'*/}
                {/*    label={labelUserName}*/}
                {/*    labelStyle={styles.input_label_style}*/}
                {/*    errorStyle={[styles.input_error_style,*/}
                {/*        {color: validUserName === 'Username Available' ? COLORS.valid_Color : COLORS.invalid_color}]}*/}
                {/*    errorMessage={validUserName}*/}
                {/*    inputStyle={styles.input_input_container}*/}
                {/*    inputContainerStyle={{width: wp(90)}}*/}
                {/*    value={userName}*/}
                {/*    onChangeText={(text) => this.onChange(text.trim(), 'username')}*/}
                {/*    returnKeyType={'next'}*/}
                {/*    onSubmitEditing={() => {*/}
                {/*        this.refs.Password.focus();*/}
                {/*    }}*/}
                {/*/>*/}

                <Spacer space={1.5}/>
                <View style={{alignItems: 'center'}}>
                    <Input
                        containerStyle={{width: wp(95)}}
                        placeholder='Password'
                        ref='Password'
                        label={labelPassword}
                        secureTextEntry={true}
                        labelStyle={styles.input_label_style}
                        errorStyle={styles.input_error_style}
                        errorMessage={validPassword}
                        inputStyle={styles.input_input_container}
                        // inputContainerStyle={{width: wp(90)}}
                        value={password}
                        onChangeText={(text) => this.onChange(text, 'password')}
                        returnKeyType={'done'}
                    />

                    <Spacer space={2}/>
                    <View style={styles.forgot_and_check_view}>
                        <TouchableOpacity
                            style={styles.remember_me_touchable}
                            onPress={() => this.onChange('', 'terms')}>
                            {checked ? <CheckSVG/> : <UnCheckSVG/>}
                            <Spacer row={2}/>
                            <Text style={styles.i_accept_text}>Remember Me</Text>
                        </TouchableOpacity>
                        <Text
                            onPress={() =>
                                navigate('forgotPassword')
                            }
                            style={styles.forgot_text}
                        >Forgot Password?</Text>
                    </View>
                </View>
            </>
        );
    };


    // =============================================================================================
    // Render method for Custom Loader
    // =============================================================================================

    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={STRINGS.LOADING_TEXT}/>
        );
    };


    render() {
        const {areAllValuesValid} = this.state;
        const {navigation} = this.props;
        const {navigate} = navigation;
        return (
            <SafeAreaViewContainer>
                <KeyboardAvoidingView
                    style={styles.keyboard_style}
                    behavior={(Platform.OS === 'ios') ? 'padding' : null}>
                    <TouchableWithoutFeedback
                        // onPressOut={}
                        onPress={() => {
                            Keyboard.dismiss();

                        }}>
                        <ScrollContainer>
                            <MainContainer>
                                <Image style={{height: wp(50), width: wp(50)}}
                                       source={ICONS.LOGO} resizeMode={'contain'}/>
                                <Text style={styles.header_text}>{STRINGS.login_text}</Text>
                                <Spacer space={4}/>
                                {this._renderInputs()}
                                <Spacer space={1.5}/>
                                {/*{this._renderInput2()}*/}
                                <Spacer space={5}/>
                                <PrimaryButton
                                    onPress={() => this.login()}
                                    isDisabled={!areAllValuesValid}
                                    bgColor={areAllValuesValid ? COLORS.app_theme_color : COLORS.app_theme_color_disabled}
                                    text={STRINGS.login_text}/>
                                <Spacer space={3}/>
                                <OrSVG width={wp(80)}/>
                                <Spacer space={3}/>
                                <View style={{flexDirection: 'row'}}>
                                    {this._renderFacebookButton()}
                                    <Spacer row={2}/>
                                    {this._renderGoogleButton()}
                                    <Spacer row={2}/>
                                    {this._renderTwitterButton()}
                                    <Spacer row={2}/>
                                    {this._renderInstagramButton()}
                                </View>
                                <Spacer space={5}/>
                                <Text style={styles.do_have_account}>{'Do not have an account yet?'}
                                </Text>
                                <Spacer space={2}/>
                                <Text
                                    onPress={() => navigate('selectAccount', {
                                        socialId: '',
                                        socialPlatform: '',
                                        socialEmail: '',
                                    })}
                                    style={styles.text_signUp}>{'Signup'}
                                </Text>
                                <Spacer space={2}/>
                            </MainContainer>
                            {this._renderCustomLoader()}
                        </ScrollContainer>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </SafeAreaViewContainer>
        );
    }
}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = state => ({
    loginState: state.LoginReducer,
});

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
    return {
        loginApi: (payload) => dispatch(LoginAction(payload)),
    };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
