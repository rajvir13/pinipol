import React from 'react';
import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {FONT} from '../../../../utils/FontSizes';
import COLORS from '../../../../utils/Colors';
import {FONT_FAMILY} from '../../../../utils/Font';

//
//  const MainContainer = styled.View`
// flex: 1;
// justifyContent: center;
// alignItems: center;
// backgroundColor: pink`;
//
//
//  const InputTextsContainer = styled.View`
// width: ${wp("90%")};
// margin-vertical: ${wp("5%")};
// justifyContent: space-evenly;
// `;
//  const BorderViewContainer = styled.View`
//  borderColor:${COLORS.light_grey1};
//  borderWidth: 1 ;
//  borderRadius:10;
//  paddingVertical:4;
// `;
// const ForgotButton = styled.TouchableOpacity`
//     justifyContent: flex-end;
//     alignSelf: flex-end;
//      margin-right: 20;
//      `;
//
// const ForgotText = styled.Text`
//     justifyContent: flex-end;
//     fontSize: ${FONT.TextSmall_2};
//     color: ${COLORS.btn_color}
//     fontFamily: ${FONT_FAMILY.Roboto};
//     `;
//
// const forgotText= styled.Text`
//     color: COLORS.WHITE_COLOR,
//         fontSize: wp(4.5),
//         fontFamily: FONTNAME.ProximaNova,
// `;
// export {
//     MainContainer,
//     InputTextsContainer,
//     ForgotText,
//     ForgotButton,
//     BorderViewContainer,
//     forgotText
// }


const styles = StyleSheet.create({
    keyboard_style: {
        flex: 1,
        backgroundColor: COLORS.white_color,
    },
    input_label_style: {
        fontFamily: FONT_FAMILY.Roboto,
        fontSize: FONT.TextSmall,
        color: COLORS.black_color,
    },
    input_error_style: {
        color: COLORS.invalid_color,
        fontFamily: FONT_FAMILY.Roboto,
        fontSize: FONT.TextSmall_2,
    },
    input_input_container: {
        fontFamily: FONT_FAMILY.Roboto,
        fontSize: FONT.TextMedium_2,
        color: COLORS.black_color,
    },
    forgot_and_check_view: {
        width: wp(90),
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    remember_me_touchable: {
        width: wp(40),
        flexDirection: 'row',
    },
    i_accept_text: {
        fontSize: FONT.TextSmall,
        color: COLORS.black_color,
        fontFamily: FONT_FAMILY.Roboto,
    },
    forgot_text: {
        fontStyle: 'normal',
        alignItems: 'center',
        fontFamily: FONT_FAMILY.PoppinsBold,
        color: COLORS.app_theme_color,
        fontSize: FONT.TextSmall_2,
    },
    header_text: {
        fontFamily: FONT_FAMILY.PoppinsBold,
        fontSize: FONT.TextNormal,
        fontStyle: 'normal',
        lineHeight: wp(10),
        color: COLORS.app_theme_color,
        alignItems: 'center',
        textAlign: 'center',
    },
    do_have_account: {
        textAlign: 'center',
        color: COLORS.off_black,
        fontSize: FONT.TextSmall_2,
        fontFamily: FONT_FAMILY.Poppins,
    },
    text_signUp: {
        textAlign: 'center',
        color: COLORS.app_theme_color,
        fontSize: FONT.TextSmall_2,
        fontFamily: FONT_FAMILY.PoppinsBold,
    },
});

export default styles;
