import styled from "styled-components/native/dist/styled-components.native.esm";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";



const ListItemContainer = styled.View`
width: ${wp("90%")};
paddingVertical: ${wp("3%")};
paddingHorizontal: ${wp("2%")};
flexDirection: row;
alignItems: center;
marginBottom: ${wp("2%")};
borderRadius: ${wp("3%")}
`;



export {
    ListItemContainer,
}
