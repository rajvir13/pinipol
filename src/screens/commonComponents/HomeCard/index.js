import React from 'react';
import {ShadowViewContainer} from '../../../utils/BaseStyle';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {Spacer} from '../../../customComponents/Spacer';
import {FONT_FAMILY} from '../../../utils/Font';
import {FONT} from '../../../utils/FontSizes';
import COLORS from '../../../utils/Colors';
import Menu, {MenuItem} from 'react-native-material-menu';
import RightArrow from 'react-native-vector-icons/Entypo';
import Home from '../../personal/personalHome';
import moment from 'moment';

const HomeCard = ({item, index, optionArr, onOptionClick, onMenuItemPress, onImageClick}) => {

    const _menu = [];

    const hideMenu = (item, index) => {
        _menu[index].hide();
    };

    const showMenu = (item, index) => {
        _menu[index].show();
    };

    const onMenuPress = (item, index) => {
        hideMenu(item, index);
        onMenuItemPress();
    };

    const onDeletePress = (item, index) => {
        hideMenu(item, index);
    };

    const displayDropdown = (item, index) => {
        return (
            <View style={{
                paddingLeft: wp(4),
            }}>
                <Menu
                    ref={menu => {
                        _menu[index] = menu;
                    }}
                    button={
                        <TouchableOpacity onPress={() => showMenu(item, index)}>
                            <RightArrow name={'chevron-down'} size={wp(7)} color={COLORS.off_black}/>
                        </TouchableOpacity>
                    }>
                    <MenuItem onPress={() => onMenuPress(item, index)}>Report</MenuItem>
                    {/*<MenuItem onPress={() => onDeletePress(item, index)}>Delete</MenuItem>*/}
                </Menu>
            </View>
        );
    };

    let startDate = moment(new Date()).format('hh:mm A - DD MMMM YYYY');
    let endDate = moment(new Date()).format('DD MMMM YYYY');
    let user_profile = 'https://www.cornwallbusinessawards.co.uk/wp-content/uploads/2017/11/dummy450x450-300x300.jpg';
    let name = 'Anonymous';

    if (item.poll_setting !== undefined && item.poll_setting.length !== 0) {
        startDate = item.poll_setting[0].start_date;
        startDate = moment(startDate).format('hh:mm A - DD MMMM YYYY');

        endDate = item.poll_setting[0].end_date;
        endDate = moment(endDate).format('DD MMM, YYYY');
    }
    if (item.users_data !== 'Anonymous') {
        if (item.user_meta.length !== 0 && item.user_meta[0].profile_image !== null) {
            user_profile = item.user_meta[0].profile_image;
        }
        if (item.users_data.length !== 0) {
            if (item.users_data[0].name === null) {
                name = item.users_data[0].firstname + item.users_data[0].lastname;
            } else {
                name = item.users_data[0].name;
            }
        }
    }
    return (
        <ShadowViewContainer style={{width: wp(100), justifyContent: 'center', alignItems: 'center'}}>
            <View style={{width: wp(92), paddingVertical: wp(2)}}>
                <View style={{flexDirection: 'row'}}>
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        width: wp(55),
                    }}>
                        <Image
                            style={{
                                width: wp(15),
                                height: wp(15),
                                borderRadius: wp(15) / 2,
                            }}
                            source={{uri: user_profile}}/>
                        <Spacer row={1}/>
                        <Text style={{
                            width: wp(40),
                            fontFamily: FONT_FAMILY.Poppins,
                            fontSize: FONT.TextSmall_2,
                        }}>{name}</Text>
                    </View>
                    <Spacer row={0.5}/>
                    {item.total_vote_count > 90 && <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                        width: wp(36),
                    }}>

                        <Text style={{
                            textAlign: 'center',
                            width: wp(18),
                            fontFamily: FONT_FAMILY.Poppins,
                            fontSize: FONT.TextSmall_2,
                        }}>Guru</Text>
                        <Spacer row={1}/>
                        <Image
                            style={{
                                width: wp(14),
                                height: wp(14),
                                // borderRadius: wp(10) / 2,
                            }}
                            source={{uri: 'https://www.cornwallbusinessawards.co.uk/wp-content/uploads/2017/11/dummy450x450-300x300.jpg'}}/>

                    </View>}
                </View>
                <Spacer space={2}/>
                <View style={{flexDirection: 'row'}}>
                    <View style={{width: wp(80)}}>
                        <Text style={{
                            fontFamily: FONT_FAMILY.PoppinsSemiBold,
                            fontSize: FONT.TextMedium,
                            color: COLORS.black_color,
                        }}>{item.poll_title}</Text>
                        <Text numberOfLines={3}
                              style={{
                                  fontFamily: FONT_FAMILY.Roboto,
                                  fontSize: FONT.TextSmall,
                                  color: COLORS.grey_color,
                              }}>{item.poll_desc}</Text>
                    </View>
                    {displayDropdown(item, index)}
                </View>
                <Spacer space={2}/>
                {optionArr.map((item1, index1) => {
                    return <View>
                        <View style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                        }}>
                            <TouchableOpacity
                                onPress={() => onImageClick(item1, index1)}
                            >
                                <Image style={{
                                    width: wp(20),
                                    height: wp(20),
                                    borderRadius: wp(2),
                                }}
                                       source={{uri: item1.option_image !== null ? item1.option_image : item1.unsplash_mg}}/>

                            </TouchableOpacity>
                            <Spacer row={1}/>
                            <TouchableOpacity
                                disabled={item.vote_first_time === 1}
                                onPress={() => onOptionClick(item1, index1)}
                                style={{
                                    backgroundColor: item1.vote_on_option === '1' ? COLORS.poll_voted_color : COLORS.light_grey1,
                                    width: wp(70),
                                    paddingVertical: wp(8),
                                    borderRadius: wp(2),
                                }}>
                                <Text numberOfLines={2} style={{
                                    paddingHorizontal: wp(3),
                                    fontSize: FONT.TextSmall,
                                    fontFamily: FONT_FAMILY.Poppins,
                                    color: item1.vote_on_option === '1' ? COLORS.white_color : COLORS.black_color,
                                }}>{item1.option_name}</Text>
                            </TouchableOpacity>

                        </View>
                        <Spacer space={1}/>
                    </View>;
                })}

                <View>
                    <Text>{item.total_vote_count !== 0 ? item.total_vote_count : 0} Votes</Text>
                    <Spacer space={1}/>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <Text>{startDate}</Text>
                        <Text>Exp - {endDate}</Text>
                    </View>
                </View>
            </View>
        </ShadowViewContainer>
    );
};


export default HomeCard;
