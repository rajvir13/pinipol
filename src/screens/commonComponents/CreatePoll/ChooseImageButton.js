import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import COLORS from '../../../utils/Colors';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import UpArrowSVG from '../../../customComponents/imagesComponents/UpArrowSVG';
import {FONT} from '../../../utils/FontSizes';
import {FONT_FAMILY} from '../../../utils/Font';

const ChooseImageButton = ({text, onPress}) => {
    return (
        <View style={{width: wp(43)}}>
            <TouchableOpacity
                onPress={onPress}
                style={{
                    backgroundColor: COLORS.app_theme_color,
                    paddingVertical: wp(2),
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: wp(1.5),
                }}>
                <Text style={{
                    color: COLORS.white_color,
                    width: wp(30),
                    textAlign: 'center',
                    fontSize: FONT.TextSmall_2,
                    fontFamily: FONT_FAMILY.PoppinsBold,
                }}>{text}</Text>
                <UpArrowSVG/>
            </TouchableOpacity>
        </View>
    );
};

export default ChooseImageButton;
