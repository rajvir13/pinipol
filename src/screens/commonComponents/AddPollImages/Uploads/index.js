import React from 'react';
import {Image, View} from 'react-native';
import BaseClass from '../../../../utils/BaseClass';
import {MainContainer} from '../../../../utils/BaseStyle';
import {PrimaryButton, SecondaryButton} from '../../../../customComponents/ButtonComponent';
import COLORS from '../../../../utils/Colors';
import {Spacer} from '../../../../customComponents/Spacer';
import ImagePicker from 'react-native-image-crop-picker';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

class UploadImages extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            image: '',
            imageArray: [],
        };
    }

    openCamera = () => {
        ImagePicker.openCamera({
            compressImageQuality: 0.2,
            includeBase64: true,
            cropping: true,
            mediaType: 'photo',
        }).then(image => {
            console.log(image);
            this.setState({image: image.path, imageArray: image});

        });
    };

    openPicker = () => {
        ImagePicker.openPicker({
            compressImageQuality: 0.2,
            includeBase64: true,
            cropping: true,
            mediaType: 'photo',
        }).then(image => {
            console.warn(image);
            this.setState({image: image.path, imageArray: image});
        });
    };

    onSavePress = () => {
        const {passingProps} = this.props;
        const {route,navigation} = passingProps;
        const {imageArray} = this.state;
        route.params.onGoBack(imageArray,'Images');
        navigation.goBack();
    };

    render() {
        const {image} = this.state;
        return (
            <MainContainer>
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    {image.length === 0 ?
                        <>
                            <PrimaryButton
                                onPress={() => this.openCamera()}
                                bgColor={COLORS.app_theme_color} text={'Camera'}/>
                            <Spacer space={3}/>
                            <PrimaryButton
                                onPress={() => this.openPicker()}
                                bgColor={COLORS.app_theme_color} text={'Upload from Gallery'}/>
                        </> :
                        <>
                            <Image source={{uri: image}} style={{
                                width: wp(90),
                                height: wp(90),
                                backgroundColor: COLORS.image_background_color,
                            }} resizeMode={'cover'}/>
                            <Spacer space={4}/>
                            <View style={{flexDirection: 'row'}}>
                                <SecondaryButton
                                    onPress={() => this.setState({image: ''})}
                                    text={'Cancel'}
                                    bgColor={COLORS.white_color}
                                    textColor={COLORS.black_color}
                                    givenWidth={wp(40)}
                                    isCancel={true}
                                />
                                <Spacer row={3}/>
                                <SecondaryButton
                                    onPress={() => this.onSavePress()}
                                    text={'Save'}
                                    bgColor={COLORS.app_theme_color}
                                    textColor={COLORS.white_color}
                                    givenWidth={wp(40)}
                                />
                            </View>
                        </>
                    }
                </View>
            </MainContainer>
        );
    }
}

export default UploadImages;
