import React from 'react';
import {Platform, StatusBar, View} from 'react-native';
import BaseClass from '../../../utils/BaseClass';
import {SafeAreaViewContainer} from '../../../utils/BaseStyle';
import TopHeader from '../../../customComponents/Header';
import AddImagesTopTab from './TopTabBar';
import COLORS from '../../../utils/Colors';
import STRINGS from '../../../utils/Strings';


class AddPollImages extends BaseClass {

    render() {
        const {navigation} = this.props;
        return (
            <SafeAreaViewContainer>
                <TopHeader onPressBackArrow={() => navigation.goBack()} text={STRINGS.ADD_IMAGE_HEADER}/>
                <View style={{backgroundColor: COLORS.backGround_color, flex: 1}}>
                    <AddImagesTopTab props={this.props} />
                </View>
            </SafeAreaViewContainer>
        );
    }
}

export default AddPollImages;
