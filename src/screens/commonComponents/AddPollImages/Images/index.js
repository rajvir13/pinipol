import React from 'react';
import {Text, View} from 'react-native';
import BaseClass from '../../../../utils/BaseClass';
import {MainContainer} from '../../../../utils/BaseStyle';
import {PrimaryButton} from '../../../../customComponents/ButtonComponent';
import COLORS from '../../../../utils/Colors';
import {FONT} from '../../../../utils/FontSizes';
import {FONT_FAMILY} from '../../../../utils/Font';
import STRINGS from '../../../../utils/Strings';
import {widthPercentageToDP as wp} from "react-native-responsive-screen";

class UnsplashImages extends BaseClass {
    render() {
        return (
            <MainContainer
                // style={{backgroundColor: 'lightseagreen'}}
            >
                <View style={{alignItems: 'center', marginTop: wp(5)}}>
                    <Text style={{
                        color: COLORS.off_black,
                        fontSize: FONT.TextMedium,
                        fontFamily: FONT_FAMILY.PoppinsSemiBold,
                    }}>{STRINGS.COMING_SOON}</Text>
                </View>

            </MainContainer>
        );
    }
}

export default UnsplashImages;
