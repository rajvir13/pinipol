import React, {useEffect, useState, useCallback, useRef} from 'react';
import {
    Animated,
    Dimensions,
    StyleSheet,
    SafeAreaView,
    View, Text,
} from 'react-native';
import _ from 'lodash';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';

import COLORS from '../../../../utils/Colors';
import {FONT} from '../../../../utils/FontSizes';
import {FONT_FAMILY} from '../../../../utils/Font';
import UnsplashImages from '../Images';
import UploadImages from '../Uploads';


const TabIndicator = ({width, tabWidth, index}) => {
    const marginLeftRef = useRef(new Animated.Value(index ? tabWidth : 0))
        .current;
    useEffect(() => {
        Animated.timing(marginLeftRef, {
            toValue: tabWidth,
            duration: 400,
        }).start();
    }, [tabWidth]);

    return (
        <Animated.View
            style={{
                justifyContent: 'flex-end',
                alignItems: 'center',
                flex: 1,
                width: width,
                marginLeft: marginLeftRef,
                position: 'absolute',
                bottom: -12,
            }}
        >
            {/*<TabTipSVG fillColor={COLORS.app_theme_color}/>*/}
        </Animated.View>
    );
};

const AddImagesTopTab = ({props}) => {
    const [index, setIndex] = useState(0);
    const routes = [
        {key: 'first', title: 'Upload'},
        {key: 'second', title: 'Images'},
    ];

    const renderIndicator = useCallback(
        ({getTabWidth}) => {
            const tabWidth = _.sum(
                [...Array(index).keys()].map((i) => getTabWidth(i)),
            );

            return (
                <TabIndicator
                    width={getTabWidth(index)}
                    tabWidth={tabWidth}
                    index={index}
                />
            );
        },
        [index],
    );

    return (
        <View style={styles.container}>
            <TabView
                lazy={true}
                navigationState={{
                    index,
                    routes,
                }}
                renderScene={({route}) => {
                    switch (route.key) {
                        case 'first':
                            return <UploadImages passingProps={props} routeKey={index}/>;
                        case 'second':
                            return <UnsplashImages passingProps={props} routeKey={index}/>;
                        default:
                            return null;
                    }
                }}
                onIndexChange={setIndex}
                renderTabBar={(props) => (
                    <TabBar
                        {...props}
                        scrollEnabled
                        tabStyle={styles.tabBar}
                        indicatorStyle={styles.indicator}
                        style={styles.tabBarContainer}
                        // labelStyle={styles.labelStyle}
                        renderLabel={({route}) => {
                            if (index === 0) {
                                if (route.key === 'first') {
                                    return <Text style={[styles.labelStyle, {
                                        color: COLORS.black_color,
                                        fontFamily: FONT_FAMILY.PoppinsSemiBold,
                                    }]}>
                                        {route.title}</Text>;

                                }
                            }
                            if (index === 1) {
                                if (route.key === 'second') {
                                    return <Text style={[styles.labelStyle, {
                                        color: COLORS.black_color,
                                        fontFamily: FONT_FAMILY.PoppinsSemiBold,
                                    }]}>
                                        {route.title}</Text>;

                                }
                            }
                            return <Text style={[styles.labelStyle, {
                                color: COLORS.black_color,
                                fontFamily: FONT_FAMILY.Poppins,
                            }]}>
                                {route.title}</Text>;


                        }}
                    />
                )}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    tabBar: {
        width: wp(100) / 2,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: COLORS.app_theme_color,
        marginHorizontal: wp(0.3),
    },
    indicator: {
        // backgroundColor: "#ccc"
        backgroundColor: COLORS.app_theme_color,
    },
    tabBarContainer: {
        backgroundColor: COLORS.backGround_color,
        marginBottom: wp(3),
    },
    labelStyle: {
        marginVertical: 0,
        fontSize: FONT.TextSmall_2,
        // fontFamily: FONT_FAMILY.PoppinsSemiBold,
        textAlign: 'center',

    },
});

export default AddImagesTopTab;
