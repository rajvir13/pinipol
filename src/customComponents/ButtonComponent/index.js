import React from 'react';
import {TouchableOpacity, Text, View} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {FONT_FAMILY} from '../../utils/Font';
import {FONT} from '../../utils/FontSizes';
import COLORS from '../../utils/Colors';
import ArrowForward from '../imagesComponents/ArrowForward';

const PrimaryButton = ({onPress, text, isNextIcon, bgColor, isDisabled}) => {
    return (
        <TouchableOpacity
            disabled={isDisabled}
            style={{
                backgroundColor: bgColor,
                width: wp(90),
                paddingVertical: wp(4),
                borderRadius: wp(1.5),
                alignItems: 'center',
                justifyContent: 'center',
                flexDirection: 'row',
            }} onPress={onPress}>
            <Text style={{
                fontFamily: FONT_FAMILY.RobotoBold,
                fontSize: FONT.TextMedium_2,
                color: COLORS.white_color,
                width: wp(55),
                textAlign: 'center',
            }}>{text}</Text>
            {isNextIcon && <ArrowForward/>}

        </TouchableOpacity>
    );
};

const SecondaryButton = ({onPress, text, bgColor, textColor, isDisabled, givenWidth, isCancel}) => {
    return (
        <TouchableOpacity
            disabled={isDisabled}
            style={{
                backgroundColor: bgColor,
                width: givenWidth,
                paddingVertical: wp(4),
                borderRadius: wp(1.5),
                alignItems: 'center',
                justifyContent: 'center',
                flexDirection: 'row',
                borderColor: isCancel && COLORS.black_color,
                borderWidth: isCancel ? wp(0.1) : 0,
            }} onPress={onPress}>
            <Text style={{
                fontFamily: FONT_FAMILY.RobotoBold,
                fontSize: FONT.TextMedium_2,
                color: textColor,
                width: wp(55),
                textAlign: 'center',
            }}>{text}</Text>
        </TouchableOpacity>
    );
};

export {PrimaryButton, SecondaryButton};
