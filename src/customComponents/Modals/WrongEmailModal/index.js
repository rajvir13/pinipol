import {Spacer} from '../../Spacer';
import ModalButton from '../ModalButton';
import React, {useState} from 'react';
import {View, Text, Modal} from 'react-native';
import COLORS from '../../../utils/Colors';
import {FONT_FAMILY} from '../../../utils/Font';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import propTypes from 'prop-types';
import {FONT} from '../../../utils/FontSizes';

const WrongEmailModal = (props) => {


    const accept = () => {
        // setVisible(false);
        props.accept();
    };

    return (
        <Modal
            animationType="slide"
            transparent={true}
            onShow={() => console.warn('called')}
            visible={props.isVisible}>
            <View style={{
                flex: 1,
                justifyContent: 'center',
                backgroundColor: 'rgba(0, 0, 0, 0.5)',
            }}>
                <View
                    style={{paddingVertical: wp(85), width: wp(95), paddingLeft: wp(4)}}>
                    <View style={{
                        backgroundColor: COLORS.white_color,
                        borderWidth: 1,
                        borderRadius: wp(1),
                        borderColor: 'transparent',
                    }}>
                        <View style={{padding: wp(5)}}>
                            <Text style={{
                                textAlign: 'center',
                                alignSelf: 'center',
                                color: COLORS.black_color, fontFamily: FONT_FAMILY.PoppinsBold, fontSize: wp(4.8),
                            }}>Invalid Username !</Text>
                            <Spacer space={2}/>
                            <Text
                                numberOfLines={4}
                                style={{
                                    color: COLORS.off_black,
                                    fontSize: FONT.TextSmall_2,
                                    fontFamily: FONT_FAMILY.Roboto,
                                    alignSelf: 'center',
                                    textAlign: 'center',
                                }}>
                                The entered username is incorrect. Please enter the correct username associated with the
                                account. </Text>
                        </View>
                        <ModalButton
                            onButtonPress={() => accept()}
                            label={'OK'}/>
                        <Spacer space={2}/>
                    </View>
                </View>
            </View>
        </Modal>
    );

};

export default WrongEmailModal;
