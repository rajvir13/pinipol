// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import {View, Text, Modal, TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import React, {useState} from 'react';
// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import COLORS from '../../../utils/Colors';
import {FONT_FAMILY} from '../../../utils/Font';
import STRINGS from "../../../utils/Strings";
import ModalButton from "../ModalButton";
import {Spacer} from '../../Spacer';

const RegSuccessModal = (props) => {
    const [visible, setVisible] = useState(props.isVisible);

    const accept = () => {
        setVisible(false);
        props.accept();
    };
    return (
        <Modal
            transparent={true}
            visible={props.isVisible}
            onRequestClose={() => {
                console.log('hhh');
            }}>
            <View style={{
                flex: 1,
                justifyContent: 'center',
                backgroundColor: 'rgba(0, 0, 0, 0.5)'
            }}>
                <View
                    style={{paddingVertical: wp(85), width: wp(95), paddingLeft: wp(4)}}>
                    <View style={{backgroundColor: COLORS.white_color, borderWidth: 1, borderRadius: wp(1),borderColor:'transparent'}}>
                        <View style={{padding: wp(5)}}>
                            <Text style={{
                                textAlign: 'center',
                                alignSelf: 'center',
                                color: COLORS.black_color, fontFamily: FONT_FAMILY.PoppinsBold, fontSize: wp(4.8),
                            }}>Thanks for the Registration</Text>
                            <Spacer space={3}/>
                            <Text style={{
                                color: COLORS.off_black, fontSize: wp(3.2),
                                fontFamily: FONT_FAMILY.Poppins, alignSelf: 'center',
                                textAlign: 'center'
                            }}>
                                Your email account has been verified successfully</Text>
                        </View>
                        <Spacer space={3}/>
                        <ModalButton
                            onButtonPress={() => accept()}
                            label={"OK"}/>
                        <Spacer space={2}/>
                    </View>
                </View>

            </View>
        </Modal>
    );
};


export default RegSuccessModal;
