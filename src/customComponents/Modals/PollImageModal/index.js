// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import {View, Text, Modal, TouchableOpacity, Image} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import React, {useState} from 'react';
// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {Spacer} from '../../../customComponents/Spacer';
import COLORS from '../../../utils/Colors';
import {FONT_FAMILY} from '../../../utils/Font';
import STRINGS from '../../../utils/Strings';
// import ModalButton from "../../buttons/ModalButton";
import {MainContainer} from '../../../utils/BaseStyle';
import EntypoIcon from 'react-native-vector-icons/Entypo';


const PollImageModal = (props) => {
    const [visible, setVisible] = useState(props.isVisible);
    const [image] = useState(props.image);

    const crossClick = () => {
        setVisible(false);
        props.crossClick();
    };


    console.warn('Chkkkk', props);
    return (
        <Modal
            transparent={true}
            visible={props.isVisible}
            onRequestClose={() => {
                console.log('hhh');
            }}>
            <View style={{
                flex: 1,
                justifyContent: 'center',
                backgroundColor: 'rgba(0, 0, 0, 0.5)',
            }}>
                <View
                    style={{paddingVertical: wp(85), width: wp(95), paddingLeft: wp(4)}}>
                    <View style={{
                        backgroundColor: COLORS.white_color,
                        borderWidth: 1,
                        borderRadius: wp(1),
                        borderColor: 'transparent',
                    }}>
                        <View style={{}}>
                            <Spacer space={1}/>
                            <View style={{alignItems: 'flex-end'}}>
                                <EntypoIcon
                                    onPress={() => crossClick()}
                                    style={{alignItems: 'flex-end', width: wp(10)}}
                                    name={'cross'} size={wp(10)}/>
                            </View>
                            <Spacer space={2}/>
                            <View style={{backgroundColor: COLORS.new_pic_color, alignItems: 'center'}}>

                                <Image style={{height: wp(60), width: wp(60)}}
                                       source={{uri: props.image.option_image}} resizeMode={'contain'}/>
                            </View>
                            <Spacer space={2}/>
                            <Text style={{

                                width: wp(80),
                                textAlign: 'center',
                                alignSelf: 'center',
                                color: COLORS.black_color, fontFamily: FONT_FAMILY.PoppinsBold,
                                fontSize: wp(4.8),
                            }}
                                  numberOfLines={3}
                            >
                                {props.image.option_name} </Text>
                        </View>
                        {/*  <ModalButton
                            onButtonPress={() => accept()}
                            label={STRINGS.continue_text}/>*/}
                        <Spacer space={2}/>
                    </View>
                </View>

            </View>
        </Modal>
    );
};


export default PollImageModal;
