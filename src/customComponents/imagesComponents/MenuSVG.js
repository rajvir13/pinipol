// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

import React from 'react';
import Svg, {Rect, Path} from 'react-native-svg';
import COLORS from '../../../src/utils/Colors';

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------

function MenuSVG(props) {
    return (
        <Svg width="24" height="14" viewBox="0 0 24 14" fill="none" xmlns="http://www.w3.org/2000/svg">
            <Rect width="24" height="2" rx="1" fill="#92278F"/>
            <Rect y="6" width="24" height="2" rx="1" fill="#92278F"/>
            <Rect y="12" width="24" height="2" rx="1" fill="#92278F"/>
        </Svg>
    );
}

MenuSVG.defaultProps = {
    fillColor: COLORS.white_color,
    width: '24',
    height: '18',
};

export default MenuSVG;
