// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

import React from 'react';
import Svg, {Rect, Path} from 'react-native-svg';
import COLORS from '../../../src/utils/Colors';

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------

function PlusSVG(props) {
    return (
        <Svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
            <Rect x="18" width="4" height="40" fill="#92278F"/>
            <Rect x="40" y="18" width="4" height="40" transform="rotate(90 40 18)" fill="#92278F"/>
        </Svg>

    );
}

PlusSVG.defaultProps = {
    fillColor: COLORS.white_color,
    width: '24',
    height: '24',
};

export default PlusSVG;
