// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

import React from 'react';
import Svg, {Rect, Path} from 'react-native-svg';
import COLORS from '../../../src/utils/Colors';

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------

function CheckSVG(props) {
    return (
        <Svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <Rect x="0.5" y="0.5" width="19" height="19" rx="1.5" stroke="#92278F"/>
            <Path d="M14.8064 6.66675L8.54095 13.5328L5.3335 10.17" stroke="#92278F" stroke-width="1.5"
                  stroke-linecap="round" stroke-linejoin="round"/>
        </Svg>
    );
}

CheckSVG.defaultProps = {
    fillColor: COLORS.white_color,
    width: '24',
    height: '24',
};

export default CheckSVG;
