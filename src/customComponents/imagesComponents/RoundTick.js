// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

import React from "react"
import Svg, {Circle, Path} from "react-native-svg"
import COLORS from "../../../src/utils/Colors";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------

function RoundTickSVG(props) {
    return (
        <Svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
            <Circle cx="20" cy="20" r="19" stroke="white" stroke-width="2"/>
            <Path d="M9 20.2843L16.0711 27.3553L30.2132 13.2132" stroke="white" stroke-width="4"/>
        </Svg>
    )
}

RoundTickSVG.defaultProps = {
    fillColor: COLORS.white_color,
    width: "24",
    height: "24"
};

export default RoundTickSVG
