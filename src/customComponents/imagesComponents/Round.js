// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

import React from "react"
import Svg, {Circle} from "react-native-svg"
import COLORS from "../../../src/utils/Colors";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------

function RoundSVG(props) {
    return (
        <Svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
            <Circle cx="20" cy="20" r="19" stroke="#92278F" stroke-width="2"/>
        </Svg>
    )
}

RoundSVG.defaultProps = {
    fillColor: COLORS.white_color,
    width: "24",
    height: "24"
};

export default RoundSVG
