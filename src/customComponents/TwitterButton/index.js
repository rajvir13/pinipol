import React from 'react';
import {
    NativeModules,
    TouchableOpacity,
    Platform, View,
} from 'react-native';
import TwitterIconSVG from '../imagesComponents/TwitterIcon';

const {RNTwitterSignIn} = NativeModules;

const Constants = {
    //Dev Parse keys
    TWITTER_CONSUMER_KEY: 'YXSuZPRBlR7kSHqUKdAMdCazp',
    TWITTER_CONSUMER_SECRET: 'I7GwKYAMURndWMGDzctHBeRIQLYb5mH6sQXAY2Hg5OaywBlt8n',
};

class TwitterService {

    twitterLoginButton(callback) {
        return (
            <TouchableOpacity onPress={() => {
                RNTwitterSignIn.init(Constants.TWITTER_CONSUMER_KEY, Constants.TWITTER_CONSUMER_SECRET);
                RNTwitterSignIn.logIn()
                    .then(loginData => {
                        const {authToken, authTokenSecret} = loginData;
                        if (authToken && authTokenSecret) {
                            let profile = loginData;
                            profile.avatar = Platform.OS === 'ios' ? `https://twitter.com/${loginData.userName}/profile_image?size=original` : profile.avatar;
                            callback(profile);
                        }
                    })
                    .catch(error => {
                            console.warn(error);
                        },
                    );
            }}>
                <TwitterIconSVG/>
            </TouchableOpacity>
        );
    }
}

export const twitterService = new TwitterService();
