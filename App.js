/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import AppNavigator from './src/route/AppNavigation';
import {Provider} from "react-redux";
import Store from "./src/redux/store";

const App: () => React$Node = () => {
    return (
        <Provider store={Store}>
            <AppNavigator/>
        </Provider>
    );
};


export default App;
